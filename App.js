import React from 'react';
import {View, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Navigator from './src/screens/routes/Navigator';
import colors from './src/utils/colors/colors';
import {UserContext, UserProvider} from './src/context/user-context';
import {AlertNotificationRoot} from 'react-native-alert-notification';
const App = () => {
  return (
    <NavigationContainer>
      <AlertNotificationRoot>
        <UserProvider>
          <StatusBar backgroundColor={colors.primary} />
          <Navigator />
        </UserProvider>
      </AlertNotificationRoot>
    </NavigationContainer>
  );
};

export default App;
