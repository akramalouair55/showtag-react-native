const img = new Image();

document.addEventListener('shouldStart', e => {
  const myPlayer = document.getElementById('my-player');
  myPlayer.style.width = e.detail.width;
  myPlayer.style.height = e.detail.height;
  alert(e.detail.src);
  var Player = videojs('my-player', {controls: false, loadingSpinner: false});
  var playerId = Player.id_;
  Player.play();
  document.getElementById('my-player').appendChild(img);
  setTimeout(() => {
    Player.pause();
  }, 15000);
  const id_user = e.detail.userId;

  document.addEventListener('LoadImg', e => {
    img.src = 'data:image/png;base64,' + e.detail.src;
    img.style.width = '100%';
    img.style.height = '100%';
    img.style.position = 'absolute';
  });

  var config = {
    studio: true,
    designer: true,
    url: e.detail.src,
    videoId: e.detail.videoId,
    width: '960px',
    idUser: 'testuser',
  };

  var ProgramCode = config.videoId;

  Player.src({src: config.url, type: 'application/x-mpegURL'});
  //Player.setAttribute(c);
  // console.log(ProgramCode)
  // console.log(Player)

  var videoWidth;
  var videoHeight;

  // // Get the video element
  // var video = document.getElementById('ShowtagPlayer');
  // // Get the data attribute

  // console.log(programCode)

  // set global variables
  var studio = true;
  // var id_user = Math.random().toString(36).substr(2, 5); // generate a random user

  // const storedUser = localStorage.getItem("user");
  // if (storedUser) {
  //   const user = JSON.parse(storedUser);
  //   id_user = user.id;
  // } else {
  //   // Assuming userServiceInstance is available in this scope
  //   id_user = userServiceInstance.getUserId();
  // }

  console.log(id_user);

  var videoWidth;
  var videoHeight;
  var videoCustomWidth;

  var livelayer = false;

  var productTags = false;
  var celebrityTags = false;

  var classtype = 'web';
  var androidTV = false;
  var web = true;

  // if there is a user connected the system will overwerite the generate
  // user code for analytics and allow tracking on events and results
  // code = data-attribute("config")
  // if(config.idUser){
  //     id_user = config.idUser;
  // }

  // get the programcode
  // var ProgramCode = config.videoId;

  // get the storage reference
  var ClientCode = 'tag2339';

  // get the client reference
  var client_id = 2339;

  loadShowtag(Player, playerId, ProgramCode);

  // check if the video is published, if video is not published we do not continue
  // with loading the rest of the script
  function videoPublished(
    ClientCode,
    ProgramCode,
    Player,
    videoPlayerContainer,
    ProgramCode,
  ) {
    // if the studio setting is not set or set to false we load the plugin in published state
    // as one normal would load it, if a program is published we continue to load
    // else the plugin stops here.

    // we check first if the program exists and is published
    var xhrPublishedPrograms = new XMLHttpRequest();
    xhrPublishedPrograms.open(
      'GET',
      'https://' +
        ClientCode +
        '.blob.core.windows.net/api/programsactivated/' +
        client_id +
        '.json',
      true,
    );
    xhrPublishedPrograms.send();
    xhrPublishedPrograms.onreadystatechange = processRequestPublishedPrograms;

    function processRequestPublishedPrograms(e) {
      if (
        xhrPublishedPrograms.readyState == 4 &&
        xhrPublishedPrograms.status == 200
      ) {
        var PPData = JSON.parse(xhrPublishedPrograms.responseText);
        console.log(PPData);

        let publishedProgram = PPData.filter(
          program => program['ProgramCode'] === ProgramCode,
        );

        if (publishedProgram.length == 1) {
          // if program is detected then load
          loadShowtag(Player, videoPlayerContainer, ProgramCode);
        }
      }
    }
  }

  // load the specific player events: PAUSE | PLAY | SKIP | MOUSEOVER
  function playerEvents(Player, videoPlayerContainer, settings) {
    // by-pass to get the current time of the video
    var vid = document
      .getElementById(videoPlayerContainer)
      .getElementsByTagName('video')[0];

    // when the mouse is over the video we pause the video
    if (settings.mouseOverPause == 1) {
      var el = document.getElementById(videoPlayerContainer);

      el.addEventListener('mouseover', mouseOver);

      el.addEventListener('mouseleave', mouseOut);

      function mouseOver() {
        Player.pause();
      }

      function mouseOut() {
        Player.play();
      }
    }

    // PLAYING
    Player.on('playing', function (e) {
      // hide the showtag layer
      document.getElementById('showtagenabled').style.display = 'none';

      // hide the menu structure if minimize on tag is enabled
      if (settings.menuMinimizeTag == 1) {
        document.getElementById('ButtonContainer').style.display = 'none';
      }

      if (settings.ProgramInformationActive == 1) {
        document.getElementById('ProgramInformation').style.display = 'none';
      }

      // show the showtag logo in the bottom right corner for 3 seconds
      // document.getElementById('ShowtagLogo').style.display = 'block';
      // setTimeout(function () {
      //     document.getElementById('ShowtagLogo').style.display = 'none';
      // }, 3000);

      // hide elements when video continues play
      HideOnPlay(settings);

      // analytics

      // get the time of the pause and convert it into milisections and match with speed
      var currentTime =
        (Math.floor(vid.currentTime * (1000 / settings.FrameSpeed)) /
          (1000 / settings.FrameSpeed)) *
        1000;

      AnalyticsPlayClicked(currentTime, settings);
    });

    //PAUSE
    Player.on('pause', function (e) {
      // get the time of the pause and convert it into milisections and match with speed
      var currentTime =
        (Math.floor(Player.currentTime() * (1000 / settings.FrameSpeed)) /
          (1000 / settings.FrameSpeed)) *
        1000;

      console.log('currentTime :>>', currentTime);

      // duration of the video
      var duration = Player.duration();

      // reset the menu buttons to default state (remove the active effect added by JS)
      resetButtonHover();

      // hide all possible interactions on the screen
      HideAll(settings);

      // hide all possible active elements on the screen
      HideAllActive(settings);

      // hide the showtag logo
      // document.getElementById('ShowtagLogo').style.display = 'none';

      // only show the showtag layer if minimize is turned off (0 or null)
      if (settings.menuMinimize != 1) {
        // show the showtag layer
        document.getElementById('showtagenabled').style.display = 'block';
      } else {
        LoadShowtagEnabledMinimized(settings, currentTime);
      }

      // only show the button containerif minimize tag is turned off (0 or null) #EWALD FIX ERROR
      // if (settings.menuMinimizeTag != 1){

      //     // show the showtag layer
      //     document.getElementById('ButtonContainer').style.display = 'block';

      // } else {

      //     // if there are no product tags or celebrity tags
      //     if(celebrityTags == false && productTags == false){

      //         document.getElementById('ButtonContainer').style.display = 'block';

      //     }

      // }

      // turn the product detected label off (security if timeout does not run)
      if (settings.productTagNotification == 1) {
        document.getElementById('ProductDetected').style.display = 'none';
      }

      // if there are celebrity tags connected check if there is information for this time segment
      if (settings.ProductsInProgram != 0) {
        ProductTags(currentTime, settings);
      }

      // if there are product tags connected check if there is information for this time segment
      if (settings.ProductsInProgram != 0) {
        CelebrityTags(currentTime, settings);
      }

      // if there is music connected to the video check if there is music for the time segment
      if (settings.MusicInProgram != 0) {
        MusicInFrame(currentTime, settings);
      }

      // if there are forms present in the video check if a form exists for the time segment
      if (settings.FormInProgram != 0) {
        FormInFrame(currentTime, settings);
      }

      // if there are banner connected to the video, check if a banner exists for the time segment
      if (settings.BannersInProgram != 0) {
        BannersInFrame(currentTime, settings);
      }

      if (settings.ProgramInformationActive == 1) {
        ProgramInformation(settings, duration, currentTime);
      }

      // analytics
      AnalyticsPauseClicked(currentTime, settings);
    });

    // SEEKING
    Player.on('seeking', function (e) {
      // reset the menu buttons to default state (remove the active effect added by JS)
      resetButtonHover();

      // hide the showtag layer
      document.getElementById('showtagenabled').style.display = 'none';

      // show the showtag logo in the bottom right corner for 3 seconds
      // document.getElementById('ShowtagLogo').style.display = 'block';
      // setTimeout(function () {
      //     document.getElementById('ShowtagLogo').style.display = 'none';
      // }, 3000);
    });

    // FULL SCREEN DETECTION
    document.addEventListener('fullscreenchange', function () {
      checkFullscreen(vid);
    });
  }

  // detect if the player goes on fullscreen, then hide the showtag layer and resize it
  // according to screen size in ratio to the video size, bars will be place vertical
  // or horizontal next to the video
  function checkFullscreen(vid) {
    var container = document.querySelector('.video-container');

    // Listen for the fullscreen toggle request
    Player.on('fullscreenchange', function () {
      if (Player.isFullscreen()) {
        // If fullscreen mode is active, make the container go fullscreen
        if (container.requestFullscreen) {
          container.requestFullscreen();
        } else if (container.mozRequestFullScreen) {
          /* Firefox */
          container.mozRequestFullScreen();
        } else if (container.webkitRequestFullscreen) {
          /* Chrome, Safari and Opera */
          container.webkitRequestFullscreen();
        } else if (container.msRequestFullscreen) {
          /* IE/Edge */
          container.msRequestFullscreen();
        }
      } else {
        // If fullscreen mode is not active, exit
        if (document.exitFullscreen) {
          document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
          /* Firefox */
          document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
          /* Chrome, Safari and Opera */
          document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
          /* IE/Edge */
          document.msExitFullscreen();
        }
      }
    });

    // //TEST VIDEO: Width 1920
    // //TEST VIDEO: Height 1080

    // var original_video_width = vid.videoWidth; //get the original width

    // var original_video_height = vid.videoHeight; //get the original height

    // showtagenabled.style.display = "none";

    // // HideAll(settings);

    // var containerWidth = vid.clientWidth;

    // var containerHeight = vid.clientHeight;

    // var screen_width = screen.width;

    // var screen_height = screen.height;

    // var videoAspectRatio =  (original_video_width / original_video_height).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]; //16:9 = 1.77 movie: 2.4

    // var screenAspectRatio = (screen_width / screen_height).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]; //

    // if (document.fullscreenElement) {

    //     if(screenAspectRatio == videoAspectRatio){

    //         // screen
    //         // 1920 x 1080 = 1.77

    //         // video
    //         // 1920 x 1080 = 1.77 YES

    //         // screen and video will fill 100%

    //         document.getElementById("showtagenabled").style.width = screen_width;
    //         document.getElementById("showtagenabled").style.height = screen_height;

    //     } else if (screenAspectRatio >= videoAspectRatio) {

    //         // screenAspectRatio >= videoAspectRatio

    //         // screen
    //         // 3440 x 1440 = 2.38
    //         // 1920 x 1080 = 1.77
    //         // 1920 x 1200 = 1.6

    //         // video
    //         // 1920 x 1080 = 1.77 YES
    //         // 720 x 1280 = 0.56 (iphone video portait) YES

    //         // OFFSET LEFT AND RIGHT

    //         // GET THE ORIGINAL VIDEO WIDTH AND CALCULATE THE FACTOR WITH THE SCREEN RESOLUTION
    //         var factor = screen_height / original_video_height;

    //         // MULTIPLY THE HEIGHT OF THE ORIGINAL VIDEO WITH THE FACTOR
    //         var new_video_width = original_video_width * factor

    //         // SUBSTRACT THE NEW_VIDEO_HEIGHT FROM THE SCREENHEIGHT
    //         var offsetTotal = screen_width - new_video_width;

    //         // DIVIDE THE OFFSET TOTAL BY 2 TO GET THE BLACK BARS
    //         var offset_left = offsetTotal / 2;

    //         document.getElementById("showtagenabled").style.width = new_video_width;
    //         document.getElementById("showtagenabled").style.height = screen_height;
    //         document.getElementById("showtagenabled").style.left = offset_left + "px";

    //     } else if (screenAspectRatio <= videoAspectRatio) {

    //         // screen
    //         // 3440 x 1440 = 2.38
    //         // 1920 x 1080 = 1.77
    //         // 1920 x 1200 = 1.6

    //         // video
    //         // 1920 x 800 = 2.4

    //         // here the screen height is more as the video height

    //         // GET THE ORIGINAL VIDEO WIDTH AND CALCULATE THE FACTOR WITH THE SCREEN RESOLUTION
    //         var factor = screen_width / original_video_width;
    //         // 2.6875 = 3440 / 1280

    //         // MULTIPLY THE HEIGHT OF THE ORIGINAL VIDEO WITH THE FACTOR
    //         var new_video_height = original_video_height * factor
    //         // 3.225 = 1200 * 2.6875

    //         // SUBSTRACT THE NEW_VIDEO_HEIGHT FROM THE SCREENHEIGHT
    //         var offsetTotal = screen_height - new_video_height;

    //         // DIVIDE THE OFFSET TOTAL BY 2 TO GET THE BLACK BARS
    //         var offset_top = offsetTotal / 2;

    //         document.getElementById("showtagenabled").style.width = screen_width;
    //         document.getElementById("showtagenabled").style.height = new_video_height;
    //         document.getElementById("showtagenabled").style.top = offset_top + "px";

    //     } else {

    //     }

    // } else {

    //     // this would be the default load [WORKS FINE]

    //     document.getElementById("showtagenabled").style.width = containerWidth;
    //     document.getElementById("showtagenabled").style.height = containerHeight;
    //     document.getElementById("showtagenabled").style.top = "0px";
    //     document.getElementById("showtagenabled").style.left = "0px";

    // }
  }

  // adjust the video container is a custom width has been set, we adjust it to the videoWrapper
  function adjustSizevideoPlayerContainer(videoCustomWidth) {
    var videoWrapper = window.parent.document.getElementById('videoWrapper');

    // overrule the basic setting of dimensions of the video
    if (videoCustomWidth) {
      videoWidth = videoCustomWidth;

      videoHeight = video.videoHeight;

      if (videoCustomWidth == video.videoWidth) {
        // prevents divide by 0

        videoWidth = video.videoWidth + 'px';

        videoHeight = video.videoHeight + 'px';
      } else {
        videoWidth = videoCustomWidth;

        videoHeight =
          video.videoHeight / (video.videoWidth / videoCustomWidth) + 'px';
      }
    } else {
      videoWidth = video.videoWidth + 'px';

      videoHeight = video.videoHeight + 'px';
    }

    videoWrapper.style.width = videoWidth;
    videoWrapper.style.height = videoHeight;
    videoWrapper.setAttribute('scrolling', 'no');
    videoWrapper.setAttribute('frameborder', '0');
  }

  // SHOWTAG BELOW

  // SET GLOBAL VARIABLES FOR SHOWTAG
  var ProductData = [];
  var ProductDetailsData = [];
  var ProductDataCustom = [];
  var CelebrityData = [];
  var CelebrityDetailsData = [];
  var celebrityVideos = [];
  var MusicData = [];
  var currentTime;
  var lengthOfVideo;
  var PollData = [];
  var CelebrityVideoOverviewdata = [];
  var celebrityVideoArray = [];
  var ProductDetectedTimeOut;
  // let baseUrlStylesheet = "";

  // load showtag layer and check all the settings
  async function loadShowtag(Player, videoPlayerContainer, ProgramCode) {
    console.log('SHOWTAG ACTIVATED');

    const settings = await getProgramDetails(ProgramCode);

    console.log(settings);

    // console.log(settings)

    // we resize the iframe to the video's exact width and height
    // adjustSizevideoPlayerContainer(videoCustomWidth);

    // creation of the showtag interaction layer
    showtagLayer(videoPlayerContainer);

    // creation of the showtag logo in the bottom right of the screen
    showtagLogo(videoPlayerContainer);

    // creation of the showtag layer containers
    showtagContainers(videoPlayerContainer);

    // creation of the active elements on top of the video with the timer function
    showtagActiveElemets(settings, Player, videoPlayerContainer);

    // check if load on minimize is activated
    buttonContainerMinimize('onLoad', settings);

    // SWITCHES

    // VARIABLES
    var CustomShoppingList = 0,
      customTopButtonLinkText = '',
      customTopButtonLink = '',
      ProductsInProgram = '',
      CelebritiesInProgram = '',
      CustomCelebritiesInProgram = '',
      CustomProductsInProgram = '',
      LivePoll = '',
      MusicInProgram = '',
      activeMenuButton01 = '',
      activeMenuButton02 = '',
      activeMenuButton03 = '',
      activeMenuButton04 = '',
      activeMenuButton05 = '',
      celebrityVideoOverview = '',
      livelayer = false;
    var FavoritesActivated = false;
    var CustomShoppingList = false;
    var ProductsInProgram = '';
    var CelebritiesInProgram = '';
    var ProgramInformationActive = false;
    var livelayer = false;
    var buttonOverLive = false;
    var mouseOverPauseVideo = false;
    var mouseOverLiveVideo = false;
    var executedSkip = false;
    var executedForm = false;
    var executedPauseTo = false;
    var executedBanner = false;
    var executedCallToAction = false;

    //creation of the live button when the video is player to show the layer
    if (settings.buttonOverLive == 1) {
      buttonOverlive(videoPlayerContainer);
    }

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (settings.customTopButtonLinkVisible == 1) {
      CustomShoppingList = 1;

      customTopButtonLinkText = settings.customTopButtonLinkText;

      customTopButtonLink = settings.customTopButtonLink;
    }

    if (settings.ProductTopButtonText != 'disabled') {
      if (settings.ProductsInProgram != 0) {
        ProductsInProgram =
          '<div id="ButtonProduct" class="defaultMenuButton" style="pointer-events:initial; user-select: none;">' +
          settings.ProductTopButtonText +
          '<div id="ButtonProductLoader" class="defaultMenuLoader" style="display:none;"></div><div class="defaultMenuExtra"></div></div>';
      }
    }

    if (settings.CelebrityTopButtonText != 'disabled') {
      if (settings.CelebritiesInProgram != 0) {
        CelebritiesInProgram =
          '<div id="ButtonCelebrity" class="defaultMenuButton" style="pointer-events:initial; user-select: none;">' +
          settings.CelebrityTopButtonText +
          '<div id="ButtonCelebrityLoader" class="defaultMenuLoader" style="display:none;"></div><div class="defaultMenuExtra"></div></div>';
      }
    }

    if (settings.customCelebrityTopButton == 1) {
      if (settings.CelebritiesInProgram != 0) {
        CustomCelebritiesInProgram =
          '<div id="ButtonCustomCelebrity" class="defaultMenuButton" style="pointer-events:initial; user-select: none;">' +
          settings.customCelebrityTopButtonText +
          '<div id="ButtonCustomCelebrityLoader" class="defaultMenuLoader" style="display:none;"></div><div class="defaultMenuExtra"></div></div>';
      }
    }

    if (settings.celebrityVideoOverview == 1) {
      if (settings.CelebritiesInProgram != 0) {
        celebrityVideoOverview =
          '<div id="ButtonCelebrityVideoOverview" class="defaultMenuButton" style="pointer-events:initial; user-select: none;">' +
          settings.celebrityVideoOverviewText +
          '<div id="ButtonCelebrityVideoOverviewLoader" class="defaultMenuLoader" style="display:none;"></div><div class="defaultMenuExtra"></div></div>';
      }
    }

    if (settings.customTopButton == 1) {
      CustomProductsInProgram =
        '<div id="ButtonCustomProduct" class="defaultMenuButton" style="pointer-events:initial; user-select: none;">' +
        settings.customTopButtonText +
        '<div id="ButtonCustomProductLoader" class="defaultMenuLoader" style="display:none;"></div><div class="defaultMenuExtra"></div></div>';
    }

    if (settings.livePollsTopButton == 1) {
      LivePoll =
        '<div id="ButtonLivePoll" class="defaultMenuButton" style="pointer-events:initial; user-select: none;">' +
        settings.livePollsTopButtonText +
        '<div id="ButtonLivePollLoader" class="defaultMenuLoader" style="display:none;"></div> <div id="ButtonLivePollExtra"></div><div class="defaultMenuExtra"></div></div>';
    }

    if (settings.musicTopButtonText != 'disabled') {
      if (settings.MusicInProgram != 0) {
        MusicInProgram =
          '<div id="ButtonMusic" class="defaultMenuButton" style="pointer-events:initial; user-select: none;">' +
          settings.musicTopButtonText +
          '<div id="ButtonMusicLoader" class="defaultMenuLoader" style="display:none;"></div><div class="defaultMenuExtra"></div></div>';
      }
    }

    if (settings.activeMenuButton01 == 1) {
      activeMenuButton01 =
        '<div id="activeMenuButton01" style="pointer-events:initial" class="defaultMenuButton" onclick="window.open(&#39;' +
        settings.activeMenuButton01url +
        '&#39;)"><div class="activeButtonTitle">' +
        settings.activeMenuButton01text +
        '</div><div class="defaultMenuExtra"></div></div>';
    }

    if (settings.activeMenuButton02 == 1) {
      activeMenuButton02 =
        '<div id="activeMenuButton02" style="pointer-events:initial" class="defaultMenuButton" onclick="window.open(&#39;' +
        settings.activeMenuButton02url +
        '&#39;)">' +
        settings.activeMenuButton02text +
        '<div class="defaultMenuExtra"></div></div>';
    }

    if (settings.activeMenuButton03 == 1) {
      activeMenuButton03 =
        '<div id="activeMenuButton03" style="pointer-events:initial" class="defaultMenuButton" onclick="window.open(&#39;' +
        settings.activeMenuButton03url +
        '&#39;)">' +
        settings.activeMenuButton03text +
        '<div class="defaultMenuExtra"></div></div>';
    }

    if (settings.activeMenuButton04 == 1) {
      activeMenuButton04 =
        '<div id="activeMenuButton04" style="pointer-events:initial" class="defaultMenuButton" onclick="window.open(&#39;' +
        settings.activeMenuButton04url +
        '&#39;)">' +
        settings.activeMenuButton04text +
        '<div class="defaultMenuExtra"></div></div>';
    }

    if (settings.activeMenuButton05 == 1) {
      activeMenuButton05 =
        '<div id="activeMenuButton05" style="pointer-events:initial" class="defaultMenuButton" onclick="window.open(&#39;' +
        settings.activeMenuButton05url +
        '&#39;)">' +
        settings.activeMenuButton05text +
        '<div class="defaultMenuExtra"></div></div>';
    }

    // STYLESHEET SECTION

    if (settings.customStyleSheet == 1) {
      // LOAD THE STYLE SHEET (CUSTOM)
      var ss = document.createElement('link');
      ss.type = 'text/css';
      ss.rel = 'stylesheet';
      ss.href = settings.styleSheetUrl;
      document.getElementsByTagName('head')[0].appendChild(ss);

      let lastIndex = settings.styleSheetUrl.lastIndexOf('/');
      baseUrlStylesheet = settings.styleSheetUrl.substring(0, lastIndex);
    }

    if (settings.customStyleSheet == null || settings.customStyleSheet == 0) {
      // https://tag2081.blob.core.windows.net/stylesheet/showtagenabled.css

      // LOAD THE STYLE SHEET (DEFAULT)
      var ssd = document.createElement('link');
      ssd.type = 'text/css';
      ssd.rel = 'stylesheet';
      ssd.href =
        'https://' +
        ClientCode +
        '.blob.core.windows.net/stylesheet/showtagenabled.css';
      document.getElementsByTagName('head')[0].appendChild(ssd);
    }

    if (settings.mouseOverLive == 1) {
      var el = document.getElementById(videoPlayerContainer);

      el.addEventListener('mouseover', mouseOver);

      el.addEventListener('mouseleave', mouseOut);

      function mouseOver() {
        document.getElementById('showtagenabled').style.display = 'block';
      }

      function mouseOut() {
        document.getElementById('showtagenabled').style.display = 'none';
      }
    }

    // this gathers all the above switches to create the menu
    // and handles the clicks on the buttons
    if (settings.topBarVisible == 1) {
      // SHOW THE TOPBAR
      var TopBar = document.createElement('div');
      TopBar.id = 'ButtonContainer';
      TopBar.className = 'ButtonContainer';
      TopBar.style = 'display:none;';
      TopBar.innerHTML =
        '' +
        CelebritiesInProgram +
        CustomCelebritiesInProgram +
        ProductsInProgram +
        CustomProductsInProgram +
        MusicInProgram +
        LivePoll +
        celebrityVideoOverview +
        '<div id="activeButtonContainer">' +
        activeMenuButton01 +
        activeMenuButton02 +
        activeMenuButton03 +
        activeMenuButton04 +
        activeMenuButton05 +
        '</div>' +
        '</div>';

      document.getElementById('showtagenabled').appendChild(TopBar);

      if (settings.ProductTopButtonText != 'disabled') {
        if (settings.ProductsInProgram != 0) {
          document.getElementById('ButtonProduct').onclick = function () {
            // remove the class "Active" of any button in the menu
            resetButtonHover();

            GetAllProducts(settings, ProgramCode);

            // add class for button hover state
            addButtonHover(this.id);
          };
        }
      }

      if (settings.CelebrityTopButtonText != 'disabled') {
        if (settings.CelebritiesInProgram != 0) {
          if (livelayer == true) {
            var switchCMC = false;

            document.getElementById('ButtonCelebrity').onclick = function () {
              var id = document.getElementById('CelebrityMenuContainer');

              if (id != null) {
                if (switchCMC == false) {
                  id.style.display = 'none';

                  switchCMC = true;
                } else {
                  id.style.display = 'block';

                  switchCMC = false;
                }
              } else {
                // remove the class "Active" of any button in the menu
                resetButtonHover();

                GetAllCelebrities(0, settings, ProgramCode);

                // add class for button hover state
                addButtonHover(this.id);
              }
            };
          } else {
            document.getElementById('ButtonCelebrity').onclick = function () {
              // remove the class "Active" of any button in the menu
              resetButtonHover();

              GetAllCelebrities(0, settings, ProgramCode);

              // add class for button hover state
              addButtonHover(this.id);
            };
          }
        }
      }

      if (settings.customCelebrityTopButton == 1) {
        if (settings.CelebritiesInProgram != 0) {
          if (livelayer == true) {
            var switchCCMC = false;

            document.getElementById('ButtonCustomCelebrity').onclick =
              function () {
                var id = document.getElementById(
                  'CelebrityCustomMenuContainer',
                );

                if (id != null) {
                  if (switchCCMC == false) {
                    id.style.display = 'none';

                    switchCCMC = true;
                  } else {
                    id.style.display = 'block';

                    switchCCMC = false;
                  }
                } else {
                  // remove the class "Active" of any button in the menu
                  resetButtonHover();

                  GetAllCelebrities(1, settings, ProgramCode);

                  // add class for button hover state
                  addButtonHover(this.id);
                }
              };
          } else {
            document.getElementById('ButtonCustomCelebrity').onclick =
              function () {
                resetButtonHover();

                GetAllCelebrities(1, settings, ProgramCode);

                // add class for button hover state
                addButtonHover(this.id);
              };
          }
        }

        // SAVE CLICK TO ANALYTICS
        var productAnalyticsClickExternalLink = document.getElementsByClassName(
          'productexternallink',
        );

        for (var i = 0; i < productAnalyticsClickExternalLink.length; i++) {
          productAnalyticsClickExternalLink[i].onclick = function () {
            AnalyticsProductExternalLink(DivIdProduct, settings);
          };
        }
      }

      if (settings.customTopButton == 1) {
        document.getElementById('ButtonCustomProduct').onclick = function () {
          resetButtonHover();

          GetAllCustomProducts(settings, ProgramCode);

          AnalyticsMenuCustomProduct(settings);

          // add class for button hover state
          addButtonHover(this.id);
        };
      }

      if (settings.livePollsTopButton == 1) {
        document.getElementById('ButtonLivePoll').onclick = function () {
          resetButtonHover();

          GetLivePoll(settings, ProgramCode);

          // add class for button hover state
          addButtonHover(this.id);
        };
      }

      if (settings.musicTopButtonText != 'disabled') {
        document.getElementById('ButtonMusic').onclick = function () {
          resetButtonHover();

          GetAllMusic(settings, ProgramCode);

          // add class for button hover state
          addButtonHover(this.id);
        };
      }

      // // OLD VERSION COULD BE REMOVED
      // if (settings.favoriteButtonsActivated == 1) {

      //     document.getElementById("ButtonFavorites").onclick = function () { GetFavoriteCelebrities() };

      // }

      if (settings.celebrityVideoOverview == 1) {
        if (settings.CelebritiesInProgram != 0) {
          if (livelayer == true) {
            var switchCMC = false;

            document.getElementById('ButtonCelebrityVideoOverview').onclick =
              function () {
                var id = document.getElementById(
                  'CelebrityVideoOverviewContainer',
                );

                if (id != null) {
                  if (switchCMC == false) {
                    id.style.display = 'none';

                    switchCMC = true;
                  } else {
                    id.style.display = 'block';

                    switchCMC = false;

                    resetButtonHover();

                    GetCelebrityVideoOverview(settings, ProgramCode);

                    // add class for button hover state
                    addButtonHover(this.id);
                  }
                } else {
                  resetButtonHover();

                  GetCelebrityVideoOverview(settings, ProgramCode);

                  // add class for button hover state
                  addButtonHover(this.id);
                }
              };
          } else {
            document.getElementById('ButtonCelebrityVideoOverview').onclick =
              function () {
                resetButtonHover();

                GetCelebrityVideoOverview(settings, ProgramCode);

                // add class for button hover state
                addButtonHover(this.id);
              };
          }
        }
      }

      // analytics
      if (settings.activeMenuButton01 == 1) {
        document.getElementById('activeMenuButton01').onclick = function () {
          resetButtonHover();
          AnalyticsMenuActiveButton(settings.activeMenuButton01text, settings);
          // add class for button hover state
          addButtonHover(this.id);
        };
      }

      if (settings.activeMenuButton02 == 1) {
        document.getElementById('activeMenuButton02').onclick = function () {
          resetButtonHover();
          AnalyticsMenuActiveButton(settings.activeMenuButton02text, settings);
          // add class for button hover state
          addButtonHover(this.id);
        };
      }

      if (settings.activeMenuButton03 == 1) {
        document.getElementById('activeMenuButton03').onclick = function () {
          resetButtonHover();
          AnalyticsMenuActiveButton(settings.activeMenuButton03text, settings);
          // add class for button hover state
          addButtonHover(this.id);
        };
      }

      if (settings.activeMenuButton04 == 1) {
        document.getElementById('activeMenuButton04').onclick = function () {
          resetButtonHover();
          AnalyticsMenuActiveButton(settings.activeMenuButton04text, settings);
          // add class for button hover state
          addButtonHover(this.id);
        };
      }

      if (settings.activeMenuButton05 == 1) {
        document.getElementById('activeMenuButton05').onclick = function () {
          resetButtonHover();
          AnalyticsMenuActiveButton(settings.activeMenuButton05text, settings);
          // add class for button hover state
          addButtonHover(this.id);
        };
      }
    }

    playerEvents(Player, videoPlayerContainer, settings);
  }

  // menu button specific functions
  function addButtonHover(id) {
    // add the class to change the button
    document.getElementById(id).classList.add('defaultMenuButton-Active');

    document
      .getElementById(id)
      .getElementsByClassName('defaultMenuExtra')[0]
      .classList.add('defaultMenuExtra-Active');
  }

  function resetButtonHover() {
    // remove the class "Active" of any button in the menu
    var resetClass = document.getElementsByClassName(
      'defaultMenuButton-Active',
    );

    for (var i = 0; i < resetClass.length; i++) {
      resetClass[i].classList.remove('defaultMenuButton-Active');
    }

    // remove the class "Active" of any button in the menu
    var resetClass = document.getElementsByClassName('defaultMenuExtra-Active');

    for (var i = 0; i < resetClass.length; i++) {
      resetClass[i].classList.remove('defaultMenuExtra-Active');
    }
  }

  // get the video settings
  function getProgramDetails(ProgramCode) {
    return new Promise(resolve => {
      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/Program/' + ProgramCode;
      } else {
        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Program/' +
          ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype;
      }

      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.send();
      xhr.onreadystatechange = processRequest;

      function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
          resolve(JSON.parse(xhr.responseText));
        }
      }
    });
  }

  // creation of the showtag interaction layer
  function showtagLayer(videoPlayerContainer) {
    videoWidth = Player.videoWidth();
    videoHeight = Player.videoHeight();

    // Log the dimensions
    // console.log('Video Width:', videoWidth);
    // console.log('Video Height:', videoHeight);

    // Get the width and height of the browser window
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;

    // Log the dimensions
    // console.log('Window Width:', windowWidth);
    // console.log('Window Height:', windowHeight);

    var offsetTop = windowHeight - 1080;
    // console.log(offsetTop);
    // offset in percentage
    var offsetTopPercentage = (offsetTop / windowHeight) * 100;
    // console.log(offsetTopPercentage);

    // iframe
    var video_width = '100%';
    var video_height = 100;

    //CREATE THE SHOWTAG LAYER
    var showtagenabledLayer = document.createElement('div');
    showtagenabledLayer.setAttribute('id', 'showtagenabled');
    showtagenabledLayer.setAttribute(
      'style',
      'position:absolute;top:0px;pointer-events:none; overflow:hidden; display:none;',
    );
    // showtagenabledLayer.style.top = offsetTopPercentage +"%";
    showtagenabledLayer.style.width = video_width;
    showtagenabledLayer.style.height = video_height + '%';
    showtagenabledLayer.setAttribute('class', classtype);
    document
      .getElementById(videoPlayerContainer)
      .appendChild(showtagenabledLayer);
  }

  // creation of the showtag logo in the bottom right of the screen
  function showtagLogo(videoPlayerContainer) {}

  //create the containers in the showtag layer
  function showtagContainers(videoPlayerContainer) {
    // ACTIVE BANNERS
    var BannerActive = document.createElement('div');
    BannerActive.setAttribute('id', 'ShowtagActiveBanner');
    BannerActive.setAttribute('style', 'display: none');
    document.getElementById(videoPlayerContainer).appendChild(BannerActive);

    // PAUSE TO
    var PauseToActive = document.createElement('div');
    PauseToActive.setAttribute('id', 'ShowtagActivePauseTo');
    PauseToActive.setAttribute('style', 'display: none');
    document.getElementById(videoPlayerContainer).appendChild(PauseToActive);

    // ACTIVE FORMS
    var FormsActive = document.createElement('div');
    FormsActive.setAttribute('id', 'ShowtagActiveForms');
    FormsActive.setAttribute('style', 'display: none');
    document.getElementById(videoPlayerContainer).appendChild(FormsActive);

    // ACTIVE CALL-TO-ACTION
    var CalltoactionActive = document.createElement('div');
    CalltoactionActive.setAttribute('id', 'ShowtagActiveCalltoaction');
    CalltoactionActive.setAttribute('style', 'display: none');
    document
      .getElementById(videoPlayerContainer)
      .appendChild(CalltoactionActive);

    var CalltoActionDetailsDynamicActive = document.createElement('div');
    CalltoActionDetailsDynamicActive.setAttribute(
      'id',
      'ShowtagActiveCalltoactionDetails',
    );
    CalltoActionDetailsDynamicActive.setAttribute('style', 'display: none');
    document
      .getElementById(videoPlayerContainer)
      .appendChild(CalltoActionDetailsDynamicActive);

    // ACTIVE SKIP
    var SkipActive = document.createElement('div');
    SkipActive.setAttribute('id', 'ShowtagActiveSkip');
    SkipActive.setAttribute('style', 'display: none');
    document.getElementById(videoPlayerContainer).appendChild(SkipActive);

    // LOAD ON MINIMIZE
    var loadMinimize = document.createElement('div');
    loadMinimize.setAttribute('id', 'ShowtagActiveMinimize');
    loadMinimize.setAttribute('style', 'display: none');
    loadMinimize.setAttribute('class', classtype);
    document.getElementById(videoPlayerContainer).appendChild(loadMinimize);

    // PROGRAM INFORMATION
    var ProgramInformationDynamic = document.createElement('div');
    ProgramInformationDynamic.setAttribute('id', 'ProgramInformation');
    ProgramInformationDynamic.setAttribute('style', 'display: none');
    ProgramInformationDynamic.setAttribute('class', classtype);
    document
      .getElementById(videoPlayerContainer)
      .appendChild(ProgramInformationDynamic);

    // the following containers are added to the showtag enabled layer
    var showtagenabled = document.getElementById('showtagenabled');

    // CELEBRITY
    var CelebrityContainerDynamic = document.createElement('div');
    CelebrityContainerDynamic.id = 'Celebrity';
    showtagenabled.appendChild(CelebrityContainerDynamic);

    // CELEBRITY SECOND GROUP
    var CelebrityCustomContainerDynamic = document.createElement('div');
    CelebrityCustomContainerDynamic.id = 'CelebrityCustom';
    showtagenabled.appendChild(CelebrityCustomContainerDynamic);

    // CELEBRITY TAGS
    var CelebrityTagsDynamic = document.createElement('div');
    CelebrityTagsDynamic.id = 'CelebrityTag';
    showtagenabled.appendChild(CelebrityTagsDynamic);

    // CELEBRITY DETAILS
    var CelebrityDetailsDynamic = document.createElement('div');
    CelebrityDetailsDynamic.id = 'CelebrityDetails';
    showtagenabled.appendChild(CelebrityDetailsDynamic);

    // CELEBRITY VIDEO
    var CelebrityVideoDynamic = document.createElement('div');
    CelebrityVideoDynamic.id = 'CelebrityVideo';
    showtagenabled.appendChild(CelebrityVideoDynamic);

    // CELEBRITY VIDEO OVERVIEW
    var CelebrityVideoOverviewDynamic = document.createElement('div');
    CelebrityVideoOverviewDynamic.id = 'CelebrityVideoOverview';
    showtagenabled.appendChild(CelebrityVideoOverviewDynamic);

    // PRODUCT
    var ProductContainerDynamic = document.createElement('div');
    ProductContainerDynamic.id = 'Product';
    showtagenabled.appendChild(ProductContainerDynamic);

    // PRODUCT TAGS
    var ProductTagsDynamic = document.createElement('div');
    ProductTagsDynamic.id = 'ProductTag';
    showtagenabled.appendChild(ProductTagsDynamic);

    // PRODUCT DETAILS
    var ProductDetailsDynamic = document.createElement('div');
    ProductDetailsDynamic.id = 'ProductDetails';
    showtagenabled.appendChild(ProductDetailsDynamic);

    // CUSTOM PRODUCT DETAILS
    var CustomProductContainerDynamic = document.createElement('div');
    CustomProductContainerDynamic.id = 'ProductDetailsCustom';
    showtagenabled.appendChild(CustomProductContainerDynamic);

    // BANNER
    var BannerDynamic = document.createElement('div');
    BannerDynamic.id = 'Banner';
    showtagenabled.appendChild(BannerDynamic);

    // BANNER TRANSPAREN VIDEO
    var BannerTVideoDynamic = document.createElement('div');
    BannerTVideoDynamic.id = 'BannerTransparenVideo';
    showtagenabled.appendChild(BannerTVideoDynamic);

    // BANNER IMAGE
    var BannerImageDynamic = document.createElement('div');
    BannerImageDynamic.id = 'BannerImage';
    showtagenabled.appendChild(BannerImageDynamic);

    // FORM
    var FormsDynamic = document.createElement('div');
    FormsDynamic.id = 'Form';
    showtagenabled.appendChild(FormsDynamic);

    // POLL
    var PollDynamic = document.createElement('div');
    PollDynamic.id = 'LivePoll';
    showtagenabled.appendChild(PollDynamic);

    // MUSIC TAG
    var MusicDynamic = document.createElement('div');
    MusicDynamic.id = 'Music';
    showtagenabled.appendChild(MusicDynamic);

    // CELEBRITY
    var FavoriteContainerDynamic = document.createElement('div');
    FavoriteContainerDynamic.id = 'Favorite';
    showtagenabled.appendChild(FavoriteContainerDynamic);

    // EXIT
    var ExitButton = document.createElement('div');
    ExitButton.id = 'ExitButton';
    ExitButton.innerHTML = 'Exit';
    ExitButton.style.display = 'none';
    showtagenabled.appendChild(ExitButton);
  }

  //creation of the live button
  function buttonOverlive(videoPlayerContainer) {
    var LiveLayerActive = document.createElement('div');
    LiveLayerActive.setAttribute('id', 'ShowtagLiveLayer');
    LiveLayerActive.innerHTML =
      '<div class="skipContainer" style="position:absolute; user-select:none; pointer-events:initial;">showtag</div>';

    document.getElementById(videoPlayerContainer).appendChild(LiveLayerActive);

    document.getElementById('ShowtagLiveLayer').onclick = function () {
      myFunction();
    };

    function myFunction() {
      var x = document.getElementById('showtagenabled');
      if (x.style.display === 'none') {
        x.style.display = 'block';
      } else {
        x.style.display = 'none';
      }
    }
  }

  //active elements on top
  function showtagActiveElemets(settings, Player, videoPlayerContainer) {
    var executedSkip = false;
    var executedForm = false;
    var executedPauseTo = false;
    var executedBanner = false;
    var executedCallToAction = false;

    var livelayer = false;

    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (
      livelayer == false &&
      (settings.ActiveBannersInProgram == 1 ||
        settings.ActiveFormsInProgram == 1 ||
        settings.ActiveCalltoActionInProgram == 1 ||
        settings.ActivePauseToShop == 1 ||
        settings.ActiveSkipInto == 1 ||
        settings.productTagNotification == 1)
    ) {
      // SKIP INTRO

      if (settings.ActiveSkipInto == 1) {
        if (studio == true) {
          url =
            'https://tagapi.azurewebsites.net/api/Prepare/activeSkipIntro/' +
            settings.ProgramCode;
        } else {
          url =
            'https://' +
            ClientCode +
            '.azurewebsites.net/api/Active/skip/' +
            settings.ProgramCode;
        }

        var xhrActiveForms = new XMLHttpRequest();
        xhrActiveForms.open('GET', url, true);
        xhrActiveForms.send();
        xhrActiveForms.onreadystatechange = processRequestActiveForm;

        var ActiveSkip = [];

        function processRequestActiveForm(e) {
          if (xhrActiveForms.readyState == 4 && xhrActiveForms.status == 200) {
            ActiveSkip = JSON.parse(xhrActiveForms.responseText);
            SkipActiveContent = document.createElement('div');
          }
        }
      }

      function ActiveSkipTime(currentTime) {
        for (i = 0; i < ActiveSkip.length; i++) {
          if (
            currentTime >= ActiveSkip[i].TimeIn &&
            currentTime <= ActiveSkip[i].TimeOut
          ) {
            DrawSkip(ActiveSkip[i]);
          }

          if (currentTime == ActiveSkip[i].TimeOut) {
            document.getElementById('ShowtagActiveSkip').style.display = 'none';

            document.getElementById('ShowtagActiveSkip').innerHTML = '';

            executedSkip = false;
          }
        }
      }

      var DrawSkip = (function () {
        return function DrawSkip(item) {
          if (!executedSkip) {
            executedSkip = true;

            document.getElementById('ShowtagActiveSkip').style.display =
              'block';

            SkipActiveContent.innerHTML =
              '' +
              '<div id="skip_' +
              item.id_skip_intro +
              '" class="skipContainer" style="position:absolute; user-select:none; pointer-events:initial;">' +
              item.SkipText +
              '</div>';

            document
              .getElementById('ShowtagActiveSkip')
              .appendChild(SkipActiveContent);

            document.getElementById('skip_' + item.id_skip_intro).onclick =
              function () {
                var time = item.Duration / 1000;

                Player.currentTime(time); // EWALD CHECK THIS

                executedSkip = false;

                AnalyticsSkipClicked(item.id_skip_intro, settings);
              };
          }
        };
      })();

      // FORM

      if (settings.ActiveFormsInProgram == 1) {
        if (studio == true) {
          url =
            'https://tagapi.azurewebsites.net/api/Prepare/activeformsinprogram/' +
            settings.ProgramCode;
        } else {
          url =
            'https://' +
            ClientCode +
            '.azurewebsites.net/api/Active/Form/' +
            settings.ProgramCode;
        }

        var xhrActiveForms = new XMLHttpRequest();
        xhrActiveForms.open('GET', url, true);
        xhrActiveForms.send();
        xhrActiveForms.onreadystatechange = processRequestActiveForm;

        var ActiveForms = [];

        function processRequestActiveForm(e) {
          if (xhrActiveForms.readyState == 4 && xhrActiveForms.status == 200) {
            ActiveForms = JSON.parse(xhrActiveForms.responseText);
            FormsActiveContent = document.createElement('div');
          }
        }
      }

      function ActiveFormTime(currentTime) {
        for (i = 0; i < ActiveForms.length; i++) {
          if (
            currentTime >= ActiveForms[i].timeIn &&
            currentTime <= ActiveForms[i].timeOut
          ) {
            DrawForm(ActiveForms[i]);
          }

          if (currentTime == ActiveForms[i].timeOut) {
            document.getElementById('ShowtagActiveForms').style.display =
              'none';

            document.getElementById('ShowtagActiveForms').innerHTML = '';

            executedForm = false;
          }
        }
      }

      var DrawForm = (function () {
        return function DrawForm(item) {
          if (!executedForm) {
            executedForm = true;

            document.getElementById('ShowtagActiveForms').style.display =
              'block';

            FormsActiveContent.innerHTML =
              '' +
              '<iFrame class="FormIframe" src="' +
              item.url +
              '" style="width: ' +
              item.w +
              '%; height: ' +
              item.h +
              '%; top: ' +
              item.y +
              '%; left:' +
              item.x +
              '%; position: absolute;"></iFrame>' +
              '<img class="FormImage" src="https://' +
              ClientCode +
              '.blob.core.windows.net/images/form/' +
              item.image +
              '" style="width: ' +
              item.image_w +
              '%; height:auto; top: ' +
              item.image_y +
              '%; left:' +
              item.image_x +
              '%; position: absolute; pointer-events:none;" />';

            document
              .getElementById('ShowtagActiveForms')
              .appendChild(FormsActiveContent);
          }
        };
      })();

      // PAUSE TO

      if (settings.ActivePauseToShop == 1) {
        if (studio == true) {
          url =
            'https://tagapi.azurewebsites.net/api/Prepare/activepauseto/' +
            settings.ProgramCode;
        } else {
          url =
            'https://' +
            ClientCode +
            '.azurewebsites.net/api/Active/PauseTO/' +
            settings.ProgramCode;
        }

        var xhrActivePauseto = new XMLHttpRequest();
        xhrActivePauseto.open('GET', url, true);
        xhrActivePauseto.send();
        xhrActivePauseto.onreadystatechange = processRequestActivePauseTo;

        var ActivePauseTo = [];

        function processRequestActivePauseTo(e) {
          if (
            xhrActivePauseto.readyState == 4 &&
            xhrActivePauseto.status == 200
          ) {
            ActivePauseTo = JSON.parse(xhrActivePauseto.responseText);
            PauseToActiveContent = document.createElement('div');
          }
        }
      }

      function ActivePauseTime(currentTime) {
        for (i = 0; i < ActivePauseTo.length; i++) {
          if (
            currentTime >= ActivePauseTo[i].TimeIn &&
            currentTime <= ActivePauseTo[i].TimeOut
          ) {
            drawPauseTo(ActivePauseTo[i]);
          }

          if (currentTime == ActivePauseTo[i].TimeOut) {
            document.getElementById('ShowtagActivePauseTo').style.display =
              'none';

            document.getElementById('ShowtagActivePauseTo').innerHTML = '';

            executedPauseTo = false;
          }
        }
      }

      var drawPauseTo = (function () {
        return function drawPauseTo(item) {
          if (!executedPauseTo) {
            executedPauseTo = true;

            document.getElementById('ShowtagActivePauseTo').style.display =
              'block';

            PauseToActiveContent.innerHTML =
              '<div id="pauseto_' +
              item.id_program_pausetoshop +
              '" style="width: ' +
              item.Width +
              '%; height: auto; top: ' +
              item.Y +
              '%; left:' +
              item.X +
              '%; position: absolute;">' +
              '<img src="https://' +
              ClientCode +
              '.showtagenabled.com/images/pauseto/' +
              item.Image +
              '" style="width: 100%; height: auto" />' +
              '</div>';

            document
              .getElementById('ShowtagActivePauseTo')
              .appendChild(PauseToActiveContent);
          }
        };
      })();

      // BANNER

      if (settings.ActiveBannersInProgram == 1) {
        if (studio == true) {
          url =
            'https://tagapi.azurewebsites.net/api/Prepare/activeBannersInProgram/' +
            settings.ProgramCode;
        } else {
          url =
            'https://' +
            ClientCode +
            '.azurewebsites.net/api/Active/Banners/' +
            settings.ProgramCode;
        }

        var xhrActiveBanner = new XMLHttpRequest();
        xhrActiveBanner.open('GET', url, true);
        xhrActiveBanner.send();
        xhrActiveBanner.onreadystatechange = processRequestActiveBanner;

        var ActiveBanners = [];

        function processRequestActiveBanner(e) {
          if (
            xhrActiveBanner.readyState == 4 &&
            xhrActiveBanner.status == 200
          ) {
            ActiveBanners = JSON.parse(xhrActiveBanner.responseText);
            BannerActiveContent = document.createElement('div');
          }
        }
      }

      function ActiveBannersTime(currentTime) {
        for (i = 0; i < ActiveBanners.length; i++) {
          if (
            currentTime >= ActiveBanners[i].TimeIn &&
            currentTime <= ActiveBanners[i].TimeOut
          ) {
            drawbanner(ActiveBanners[i]);
          }

          if (currentTime == ActiveBanners[i].TimeOut) {
            document.getElementById('ShowtagActiveBanner').style.display =
              'none';

            document.getElementById('ShowtagActiveBanner').innerHTML = '';

            executedBanner = false;
          }
        }
      }

      var drawbanner = (function () {
        return function drawbanner(banner) {
          if (!executedBanner) {
            executedBanner = true;

            document.getElementById('ShowtagActiveBanner').style.display =
              'block';

            BannerActiveContent.innerHTML =
              '<div id="banner_' +
              banner.id_banner +
              '" style="width: ' +
              banner.Width +
              '%; height: auto; top: ' +
              banner.BannerY +
              '%; left:' +
              banner.BannerX +
              '%; position: absolute;">' +
              '<img src="https://' +
              ClientCode +
              '.showtagenabled.com/images/banner/' +
              banner.URL +
              '" style="width: 100%; height: auto;" />' +
              '<a style="width: ' +
              banner.LinkWidth +
              '%; height: ' +
              banner.LinkHeight +
              '%; top: ' +
              banner.LinkY +
              '%; left:' +
              banner.LinkX +
              '%; position: absolute; pointer-events: initial; cursor:pointer"' +
              'href="' +
              banner.LinkUrl +
              '" target="_blank"></a>' +
              '</div>';

            document
              .getElementById('ShowtagActiveBanner')
              .appendChild(BannerActiveContent);
          }
        };
      })();

      // CALL TO ACTION

      if (settings.ActiveCalltoActionInProgram == 1) {
        if (studio == true) {
          url =
            'https://tagapi.azurewebsites.net/api/Prepare/activecalltoactioninprogram/' +
            settings.ProgramCode;
        } else {
          url =
            'https://' +
            ClientCode +
            '.azurewebsites.net/api/Active/Calltoaction/' +
            settings.ProgramCode;
        }

        var xhrActiveCalltoAction = new XMLHttpRequest();
        xhrActiveCalltoAction.open('GET', url, true);
        xhrActiveCalltoAction.send();
        xhrActiveCalltoAction.onreadystatechange = processRequestActiveBanner;

        var ActiveCalltoAction = [];
        function processRequestActiveBanner(e) {
          if (
            xhrActiveCalltoAction.readyState == 4 &&
            xhrActiveCalltoAction.status == 200
          ) {
            ActiveCalltoAction = JSON.parse(xhrActiveCalltoAction.responseText);
            CalltoActionActiveContent = document.createElement('div');
          }
        }
      }

      function ActiveCalltoActionTime(currentTime) {
        for (i = 0; i < ActiveCalltoAction.length; i++) {
          if (
            currentTime >= ActiveCalltoAction[i].TimeIn &&
            currentTime <= ActiveCalltoAction[i].TimeOut
          ) {
            drawCallToAction(i);

            AnalyticsCallToActionView(settings, currentTime); // EWALD NEED TO ADD CALL TO ACTION ID
          }

          if (currentTime == ActiveCalltoAction[i].TimeOut) {
            document.getElementById('ShowtagActiveCalltoaction').style.display =
              'none';

            document.getElementById('ShowtagActiveCalltoaction').innerHTML = '';

            executedCallToAction = false;
          }
        }
      }

      var drawCallToAction = (function () {
        return function drawCallToAction(item) {
          if (!executedCallToAction) {
            executedCallToAction = true;

            document.getElementById('ShowtagActiveCalltoaction').style.display =
              'block';

            CalltoActionActiveContent.innerHTML =
              '' +
              '<img id="calltoaction_' +
              ActiveCalltoAction[item].id +
              '" class="CalltoActionImage" style="top: ' +
              ActiveCalltoAction[item].Y +
              '%; left:' +
              ActiveCalltoAction[item].X +
              '%; width: ' +
              ActiveCalltoAction[item].Width +
              '%; height:auto; position:absolute" src="https://' +
              ClientCode +
              '.showtagenabled.com/images/calltoaction/' +
              ActiveCalltoAction[item].image +
              '"/>';

            document
              .getElementById('ShowtagActiveCalltoaction')
              .appendChild(CalltoActionActiveContent);

            // click the call to action button
            document.getElementById(
              'calltoaction_' + ActiveCalltoAction[item].id,
            ).onclick = function () {
              var iFrame = document.getElementById(
                'calltoactionIframe_' + ActiveCalltoAction[item].id,
              );

              if (iFrame == null) {
                CallToActionDetails(item);
              } else {
                var Switch = iFrame.style.display != 'none';

                if (Switch == true) {
                  iFrame.style.display = 'none';
                } else {
                  iFrame.style.display = 'block';
                }
              }
            };

            function CallToActionDetails(item) {
              document.getElementById(
                'ShowtagActiveCalltoactionDetails',
              ).style.display = 'block';

              Player.pause(); // EWALD does this work?

              CalltoActionDetailsContent = document.createElement('div');

              CalltoActionDetailsContent.innerHTML =
                '' +
                '<iframe  id="calltoactionIframe_' +
                ActiveCalltoAction[item].id +
                '" class="CalltoActioniFrame" src="' +
                ActiveCalltoAction[item].MessageUrl +
                '" style="top: ' +
                ActiveCalltoAction[item].MessageY +
                '%; left:' +
                ActiveCalltoAction[item].MessageX +
                '%; width: ' +
                ActiveCalltoAction[item].MessageWidth +
                '%; height:' +
                ActiveCalltoAction[item].MessageHeight +
                '%; position:absolute;">' +
                '</iframe>';

              document
                .getElementById('ShowtagActiveCalltoactionDetails')
                .appendChild(CalltoActionDetailsContent);

              // ANALYTICS SAVE THE CLICK ON THE CALL TO ACTION BUTTON TO OPEN
              AnalyticsCallToActionLinkclick(
                ActiveCalltoAction[item].id,
                settings,
                currentTime,
              );
            }
          }
        };
      })();

      // NOTIFICATION SECTION
      if (settings.productTagNotification == 1) {
        // here we build the active element

        if (studio == true) {
          url =
            'https://tagapi.azurewebsites.net/api/Prepare/productsWithTime/' +
            settings.ProgramCode;
        } else {
          url =
            'https://' +
            ClientCode +
            '.azurewebsites.net/api/Menu/productsWithTime/' +
            settings.ProgramCode +
            '/' +
            id_user +
            '/' +
            client_id;
        }

        // PRODUCT DETECTED
        var ProductDetected = document.createElement('div');
        ProductDetected.setAttribute('id', 'ProductDetected');
        ProductDetected.setAttribute('style', 'display: none;');
        ProductDetected.setAttribute('class', classtype);
        ProductDetected.innerHTML =
          '<div class="ProductDetected" >' +
          '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="1.5vw" height="1.5vw" viewBox="0 0 496.304 496.303" style="enable-background:new 0 0 496.304 496.303;" xml:space="preserve">' +
          '<g><path d="M248.146,0C111.314,0,0,111.321,0,248.152c0,136.829,111.314,248.151,248.146,248.151   c136.835,0,248.158-111.322,248.158-248.151C496.304,111.321,384.98,0,248.146,0z M248.146,472.093   c-123.473,0-223.935-100.459-223.935-223.941c0-123.479,100.462-223.941,223.935-223.941   c123.488,0,223.947,100.462,223.947,223.941C472.093,371.634,371.634,472.093,248.146,472.093z M319.536,383.42v32.852   c0,1.383-1.123,2.494-2.482,2.494H196.45c-1.374,0-2.482-1.117-2.482-2.494V383.42c0-1.372,1.114-2.482,2.482-2.482h34.744V205.831   h-35.101c-1.375,0-2.468-1.111-2.468-2.474v-33.6c0-1.38,1.1-2.479,2.468-2.479h82.293c1.371,0,2.482,1.105,2.482,2.479v211.181   h36.186C318.413,380.938,319.536,382.048,319.536,383.42z M209.93,105.927c0-20.895,16.929-37.829,37.829-37.829   c20.886,0,37.826,16.935,37.826,37.829s-16.94,37.829-37.826,37.829C226.853,143.756,209.93,126.822,209.93,105.927z"/></g>' +
          '</svg>' +
          '<span id="ProductDetectedText">' +
          settings.productTagNotificationText +
          '</span></div>';
        document
          .getElementById(videoPlayerContainer)
          .appendChild(ProductDetected);

        // get all the products that are tagged with time
        var productListWithTime = [];

        var xhrpn = new XMLHttpRequest();
        xhrpn.open('GET', url, true);
        xhrpn.send();
        xhrpn.onreadystatechange = processRequest;

        function processRequest(e) {
          if (xhrpn.readyState == 4 && xhrpn.status == 200) {
            var data = JSON.parse(xhrpn.responseText);

            // we are going to loop over the recieved data and from each entry we extract
            // on time to stuff into our array
            data.forEach(element => {
              // adds an element to the array if it does not already exist using a comparer
              pushIfNew(element);
            });
          }
        }

        // we extract only an element if it not already exists in the array
        function pushIfNew(element) {
          for (var i = 0; i < productListWithTime.length; i++) {
            if (productListWithTime[i].Time === element.Time) {
              // we extract a list on time
              return;
            }
          }

          productListWithTime.push(element);
        }
      }

      function productTagnotification(currentTime) {
        // here we read the active element connected to the timeloop
        var multiplier = 3;

        // make a switch here
        if (settings.FrameSpeed < 1000) {
          multiplier = 6; // 6 * 500 = 4 sec
        }

        offset = settings.FrameSpeed * 4;

        newStartTime = currentTime - offset;

        // if a product time matches the time of the videoplayer we change the visibility and
        // hide it again after a certain time if the time is no re-invoked again
        productListWithTime.forEach(element => {
          // this one shows before the product is in the screen to compensate for user action speed
          if (element.Time == newStartTime) {
            // if a time was already before and the information still visible we remove the current timeout
            clearInterval(ProductDetectedTimeOut);

            // we keep the information on visible
            document.getElementById('ProductDetected').style.display = 'block';

            // we set a new timeout
            ProductDetectedTimeOut = setTimeout(function () {
              document.getElementById('ProductDetected').style.display = 'none';
              document.getElementById('ProductDetectedText').innerHTML =
                settings.productTagNotificationText;
            }, settings.FrameSpeed);
          }

          if (element.Time == currentTime) {
            // we keep the information on visible
            document.getElementById('ProductDetected').style.display = 'block';

            document.getElementById('ProductDetectedText').innerHTML =
              settings.productTagNotificationText;

            // document.getElementById("ProductDetectedText").innerHTML = "dette programmet er interaktivt - trykk på pause for å finne ut mer";
          }

          // else {

          //     document.getElementById("ProductDetectedText").innerHTML = settings.productTagNotificationText;

          // }
        });
      }

      // TIMER

      var vid = document
        .getElementById(videoPlayerContainer)
        .getElementsByTagName('video')[0];

      function TimeLoop() {
        var currentTime;

        vid.ontimeupdate = function () {
          currentTime =
            (Math.floor(vid.currentTime * (1000 / settings.FrameSpeed)) /
              (1000 / settings.FrameSpeed)) *
            1000;

          if (settings.ActiveBannersInProgram == 1) {
            ActiveBannersTime(currentTime);
          }

          if (settings.ActivePauseToShop == 1) {
            ActivePauseTime(currentTime);
          }

          if (settings.ActiveSkipInto == 1) {
            ActiveSkipTime(currentTime);
          }

          if (settings.ActiveCalltoActionInProgram == 1) {
            ActiveCalltoActionTime(currentTime);
          }

          if (settings.ActiveFormsInProgram == 1) {
            ActiveFormTime(currentTime);
          }

          if (settings.productTagNotification == 1) {
            productTagnotification(currentTime);
          }
        };
      }

      var intervalID;

      Player.onended = function (e) {
        clearInterval(intervalID);
      };

      Player.on('playing', function () {
        intervalID = setInterval(TimeLoop, 100);
      });

      Player.on('pause', function () {
        clearInterval(intervalID);
      });
    }
  }

  // minimize the menu if there are tags on the screen or as default
  function buttonContainerMinimize(source, settings) {
    if (settings.menuMinimize == 1) {
      document.getElementById('ShowtagActiveMinimize').innerHTML =
        '' +
        '<div id="minimizeButtonContainer" class="minimizeButtonContainer">' +
        '<div class="minimizeText" id="minimizeText">' +
        settings.menuMinimizeText +
        '</div>' +
        '<div class="minimizeExtra"></div>' +
        '<div id="minimizeProductNotification" class="minimizeProductNotification">3</div>' +
        '</div>';
    }

    if (settings.menuMinimizeTag == 1 && source == 'tag') {
      // minimize the menu of there are tags on the screen

      document.getElementById('ShowtagActiveMinimize').innerHTML =
        '' +
        '<div id="minimizeButtonContainer" class="minimizeButtonContainer">' +
        '<div class="minimizeText" id="minimizeText">' +
        settings.menuMinimizeText +
        '</div>' +
        '<div class="minimizeExtra"></div>' +
        '<div id="minimizeProductNotification" class="minimizeProductNotification">3</div>' +
        '</div>';

      document.getElementById('minimizeButtonContainer').style.display =
        'block';

      document.getElementById('minimizeButtonContainer').onclick = function () {
        document.getElementById('ButtonContainer').style.display = 'block';

        document.getElementById('ShowtagActiveMinimize').style.display = 'none';

        document.getElementById('CelebrityDetails').style.display = 'none';

        document.getElementById('ProductDetails').style.display = 'none';

        document.getElementById('ExitButton').style.display = 'block';
      };

      document.getElementById('ExitButton').onclick = function () {
        document.getElementById('ButtonContainer').style.display = 'none';

        document.getElementById('ShowtagActiveMinimize').style.display =
          'block';

        document.getElementById('ExitButton').style.display = 'none';
      };
    }
  }

  function LoadShowtagEnabledMinimized(settings, currentTime) {
    // we reset the notification if there are no products in the screen
    document.getElementById('minimizeProductNotification').style.display =
      'none';

    // we update the text in the button with the default text
    document.getElementById('minimizeText').innerHTML =
      settings.menuMinimizeText;

    document.getElementById('ShowtagActiveMinimize').style.display = 'block';

    document.getElementById('minimizeButtonContainer').onclick = function () {
      document.getElementById('showtagenabled').style.display = 'block';

      document.getElementById('ButtonContainer').style.display = 'block';

      document.getElementById('ProgramInformation').style.display = 'none';

      document.getElementById('ShowtagActiveMinimize').style.display = 'none';

      document.getElementById('ExitButton').style.display = 'block';
    };

    document.getElementById('ExitButton').onclick = function () {
      document.getElementById('showtagenabled').style.display = 'none';

      document.getElementById('ButtonContainer').style.display = 'none';

      document.getElementById('ProgramInformation').style.display = 'block';

      document.getElementById('ShowtagActiveMinimize').style.display = 'block';

      document.getElementById('ExitButton').style.display = 'none';
    };
  }

  // HIDE ON PLAY
  function HideOnPlay(settings) {
    // destroy the product tags
    document.getElementById('ProductTag').innerHTML = '';

    // destroy the celebrity tags
    document.getElementById('CelebrityTag').innerHTML = '';

    // destroy the celebrity details
    document.getElementById('CelebrityDetails').innerHTML = '';

    // hide the minimize button
    document.getElementById('ShowtagActiveMinimize').style.display = 'none';
  }

  // HIDE ALL LAYERS ETC. - FULL RESET
  function HideAll(settings) {
    document.getElementById('Product').style.display = 'none';

    document.getElementById('ProductDetails').style.display = 'none';

    document.getElementById('ProductDetailsCustom').style.display = 'none';

    // document.getElementById('ProductTag').style.display = "none";

    // document.getElementById('CelebrityTag').style.display = "none";

    document.getElementById('Celebrity').style.display = 'none';

    document.getElementById('CelebrityCustom').style.display = 'none';

    document.getElementById('CelebrityDetails').style.display = 'none';

    document.getElementById('CelebrityVideo').style.display = 'none';

    document.getElementById('CelebrityVideoOverview').style.display = 'none';

    document.getElementById('Banner').style.display = 'none';

    document.getElementById('BannerImage').style.display = 'none';

    document.getElementById('BannerTransparenVideo').style.display = 'none';

    document.getElementById('LivePoll').style.display = 'none';

    document.getElementById('Music').style.display = 'none';

    document.getElementById('Form').style.display = 'none';

    document.getElementById('Favorite').style.display = 'none';

    document.getElementById('ProgramInformation').style.display = 'none';

    if (settings.CelebrityVideoInProgram == 1) {
      document.getElementById('CelebrityVideo').innerHTML = '';
    }
  }

  function HideAllActive(settings) {
    document.getElementById('ShowtagActiveSkip').style.display = 'none';

    document.getElementById('ShowtagActiveSkip').innerHTML = '';

    document.getElementById('ShowtagActivePauseTo').style.display = 'none';

    document.getElementById('ShowtagActivePauseTo').innerHTML = '';

    document.getElementById('ShowtagActiveBanner').style.display = 'none';

    document.getElementById('ShowtagActiveBanner').innerHTML = '';

    document.getElementById('ShowtagActiveForms').style.display = 'none';

    document.getElementById('ShowtagActiveForms').innerHTML = '';

    document.getElementById('ShowtagActiveCalltoaction').style.display = 'none';

    document.getElementById('ShowtagActiveCalltoaction').innerHTML = '';

    document.getElementById('ShowtagActiveCalltoactionDetails').style.display =
      'none';

    document.getElementById('ShowtagActiveCalltoactionDetails').innerHTML = '';
  }

  // PROGRAM INFORMATION
  function ProgramInformation(settings, duration, currentTime) {
    var calculation = (currentTime / (duration * 1000)) * 100;

    var programInfo = '';

    if (videoWidth == 1920) {
      ProgramQuality = 'FULLHD';
    } else if (videoWidth == 3840) {
      ProgramQuality = '4K';
    } else if (videoWidth == 1280) {
      ProgramQuality = 'HD';
    } else if (videoWidth < 1280) {
      ProgramQuality = 'SD';
    } else {
      ProgramQuality = 'CUSTOM';
    }

    var ProgramDuration =
      Math.floor(duration / 60) +
      'm' +
      (duration / 60).toFixed(2).slice(-2) +
      'sec';

    var TimePassed =
      '' +
      '<div class="ProgramDurationContainer">' +
      '<div class="ProgramRunningTime" style="width:' +
      calculation +
      '%;"><div>' +
      '</div>';

    if (!settings.ProgramLogo == null || !settings.ProgramLogo == '') {
      ProgramLogo =
        '<img class="ProgramLogo" src="https://' +
        ClientCode +
        '.showtagenabled.com/images/' +
        settings.ProgramLogo +
        '" />';
      ProgramName = '';
    } else {
      ProgramLogo = '';
      ProgramName =
        '<div class="ProgramName">' + settings.ProgramName + '</div>';
    }

    if (!settings.ProgramYear == null || !settings.ProgramYear == '') {
      ProgramYear =
        '<div class="ProgramYear">' + settings.ProgramYear + '</div>';
    } else {
      ProgramYear = '';
    }

    if (!settings.ProgramAge == null || !settings.ProgramAge == '') {
      ProgramAge = '<div class="ProgramAge">' + settings.ProgramAge + '</div>';
    } else {
      ProgramAge = '';
    }

    if (!settings.ProgramEpisode == null || !settings.ProgramEpisode == '') {
      ProgramEpisode = settings.ProgramEpisode;
    } else {
      ProgramEpisode = '';
    }
    if (!settings.ProgramTitle == null || !settings.ProgramTitle == '') {
      ProgramTitle = settings.ProgramTitle;
    } else {
      ProgramTitle = '';
    }
    if (
      !settings.ProgramDescription == null ||
      !settings.ProgramDescription == ''
    ) {
      ProgramDescription = settings.ProgramDescription;
    } else {
      ProgramDescription = '';
    }

    programInfo =
      '' +
      '<div class="ProgramInformationContainer" style="position:absolute; user-select:none">' +
      ProgramLogo +
      ProgramName +
      '<div class="ProgramDataContainer">' +
      '' +
      ProgramYear +
      '' +
      ProgramAge +
      '<div class="ProgramDuration">' +
      ProgramDuration +
      '</div><div class="ProgramQuality"> ' +
      ProgramQuality +
      '</div><div class="ProgramDuration">' +
      ProgramEpisode +
      '</div></div> ' +
      '<div class="ProgramTitle">' +
      ProgramTitle +
      '</div>' +
      '<div class="ProgramDescription">' +
      ProgramDescription +
      '</div>' +
      TimePassed +
      '</div>';

    document.getElementById('ProgramInformation').innerHTML = programInfo;

    document.getElementById('ProgramInformation').style.display = 'block';
  }

  // CELEBRITY
  // when the viewer clicks the menu button we request all celebrities that are tagged in the video
  function GetAllCelebrities(group, settings, ProgramCode) {
    // console.log(group)

    // console.log(CelebrityDetailsData.length)

    var livelayer = false;

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (livelayer == false) {
      HideAll(settings);

      HideAllActive(settings);
    }

    // this prevents to download the data again
    if (CelebrityDetailsData.length == 0) {
      if (group == 1) {
        document.getElementById('ButtonCustomCelebrityLoader').style.display =
          'block';
      } else {
        document.getElementById('ButtonCelebrityLoader').style.display =
          'block';
      }

      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/celebritiesInProgram/' +
          settings.ProgramCode;
      } else {
        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Menu/CelebritiesInProgram/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id;
      }

      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.send();

      xhr.onreadystatechange = processRequest;

      function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
          CelebrityDetailsData = JSON.parse(xhr.responseText);

          CreateCelebrityDetails(group, settings);
        }
      }
    } else {
      CreateCelebrityDetails(group, settings);
    }
  }
  // create the celebrity details
  function CreateCelebrityDetails(group, settings) {
    // var celebrityVideoArray = [];

    if (settings.deactivateProgramDetails == 1) {
      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/CelebrityVideoAllinProgram/' +
          settings.ProgramCode;
      } else {
        // EWALD

        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Menu/CelebritiesInProgram/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id;
      }

      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.send();

      xhr.onreadystatechange = processRequest;

      function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
          celebrityVideoArray = JSON.parse(xhr.responseText);

          DrawCelebrityContent(group, celebrityVideoArray, settings);
        }
      }
    } else {
      var celebrityVideoArray = [];

      DrawCelebrityContent(group, celebrityVideoArray, settings);
    }
  }
  // show on the screen
  function DrawCelebrityContent(group, dataCvideo, settings) {
    var dynamic = '';

    var Description = '';

    var BirthDate = '';

    var BirthPlace = '';

    var CVideo = '';

    if (group == 0) {
      var celebrityGroup =
        '<div class="MenuContainer" id="CelebrityMenuContainer" style="pointer-events: initial; position:absolute">';
    }

    if (group == 1) {
      var celebrityGroup =
        '<div class="MenuContainer" id="CelebrityCustomMenuContainer" style="pointer-events: initial; position:absolute">';
    }

    for (i = 0; i < CelebrityDetailsData.length; i++) {
      if (
        CelebrityDetailsData[i].customGroup == group ||
        CelebrityDetailsData[i].customGroup == null
      ) {
        if (settings.deactivateProgramDetails == 1) {
          // string
          if (CelebrityDetailsData[i].Description != null) {
            Description =
              '<div class="MenuCelCelebrityDescription">' +
              CelebrityDetailsData[i].Description +
              '</div>';
          }

          // string
          if (CelebrityDetailsData[i].BirthDate != null) {
            BirthDate =
              '<div class="MenuCelCelebrityBirthDate">' +
              CelebrityDetailsData[i].BirthDate +
              '</div>';
          }

          // string
          if (CelebrityDetailsData[i].BirthPlace != null) {
            BirthPlace =
              '<div class="MenuCelCelebrityBirthplace">' +
              CelebrityDetailsData[i].BirthPlace +
              '</div>';
          }

          if (settings.CelebrityVideoInProgram == 1) {
            var CVDynamic = '';

            // search for the videos that match with the celebrity and add them into a list

            if (
              dataCvideo.find(
                x => x.id_celebrity == CelebrityDetailsData[i].id_celebrity,
              )
            ) {
              for (y = 0; y < dataCvideo.length; y++) {
                if (
                  dataCvideo[y].id_celebrity ==
                  CelebrityDetailsData[i].id_celebrity
                ) {
                  CVDynamic +=
                    '' +
                    '<div id="celebrityVideo_' +
                    dataCvideo[y].id_celebrityVideo +
                    '" class="CelebrityVideoThumb">' +
                    '<img id="celebrityIdVideo_' +
                    CelebrityDetailsData[i].id_celebrity +
                    '" class="CelebrityVideoThumbImage" src="https://' +
                    ClientCode +
                    '.showtagenabled.com/video/' +
                    dataCvideo[y].thumbUrl +
                    '" />' +
                    '</div>';
                }
              }

              CVideo =
                '<div class="CelebrityVideoContainer" style="position: relative; pointer-events:initial;">' +
                CVDynamic +
                '</div>';
            } else {
              CVideo = '';
            }
          }
        }

        dynamic +=
          '<div id="celebrityScroll_' +
          CelebrityDetailsData[i].id_celebrity +
          '" class="MenuCellContainerCelebrities">' +
          '<div class="MenuCellBackground">' +
          '<img src="https://' +
          ClientCode +
          '.showtagenabled.com/images/' +
          CelebrityDetailsData[i].CelebrityImage +
          '" />' +
          '<div class="MenuCellName" style="position:absolute; user-select:none;">' +
          CelebrityDetailsData[i].Name +
          '</div>' +
          Description +
          BirthDate +
          BirthPlace +
          CVideo +
          '</div>' +
          '</div>';
      }
    }

    var celebritymenuContainer =
      '' +
      celebrityGroup +
      '<div class="MenuTitle">' +
      settings.CelebrityTopButtonText +
      '</div>' +
      '<div class="MenuContainerScroll" style=" pointer-events: initial; position:relative">' +
      dynamic +
      '</div>' +
      '</div>';

    // celebrity main group
    if (group == 0) {
      document.getElementById('Celebrity').innerHTML = celebritymenuContainer;

      document.getElementById('Celebrity').style.display = 'block';

      document.getElementById('ButtonCelebrityLoader').style.display = 'none';

      // in case the celebrity as an aminiated png we reload the background each time
      let pnga_celebrity = getComputedStyle(
        document.documentElement,
      ).getPropertyValue('--scrollContainerBackgroundCelebrity');

      if (pnga_celebrity) {
        document.getElementById(
          'CelebrityMenuContainer',
        ).style.backgroundImage =
          'url("' +
          baseUrlStylesheet +
          '/' +
          pnga_celebrity +
          '?a=' +
          Math.random() +
          '")';
      }
    }

    // celebrity second group
    if (group == 1) {
      document.getElementById('CelebrityCustom').innerHTML =
        celebritymenuContainer;

      document.getElementById('CelebrityCustom').style.display = 'block';

      document.getElementById('ButtonCustomCelebrityLoader').style.display =
        'none';

      // in case the celebrity as an aminiated png we reload the background each time
      let pnga_celebrityCustom = getComputedStyle(
        document.documentElement,
      ).getPropertyValue('--scrollContainerBackgroundCustomCelebrity');

      if (pnga_celebrityCustom) {
        document.getElementById(
          'CelebrityCustomMenuContainer',
        ).style.backgroundImage =
          'url("' +
          baseUrlStylesheet +
          '/' +
          pnga_celebrityCustom +
          '?a=' +
          Math.random() +
          '")';
      }
    }

    var MenuContainerCelebrity = document.getElementsByClassName(
      'MenuCellContainerCelebrities',
    );

    if (settings.deactivateProgramDetails == 0) {
      for (var i = 0; i < MenuContainerCelebrity.length; i++) {
        MenuContainerCelebrity[i].onclick = function () {
          CelebrityClicked(this, 0, settings);
        };
      }
    }

    if (
      settings.customStyleSheet != 1 &&
      settings.ProgramInformationActive == 1 &&
      settings.topBarVisible == 1
    ) {
      document.getElementById('CelebrityMenuContainer').style.left = 'unset';

      document.getElementById('CelebrityMenuContainer').style.right = '1.5vw';
    }

    // CLICK THE CELEBRITY VIDEO
    if (settings.CelebrityVideoInProgram == 1) {
      // var CelebrityVideoClicked = document.getElementsByClassName("CelebrityVideoThumb");

      // for (var i = 0; i < CelebrityVideoClicked.length; i++) {

      //     CelebrityVideoClicked[i].onclick = function () { CelebrityVideo(this) }

      // };

      // CelebrityVideo(celebrityVideo, this, celebrityVideos, time, settings)

      // CLICK THE CELEBRITY VIDEO

      // in case the celebrity as an aminiated png we reload the background each time
      // let pnga_celebrity_video_scroll = getComputedStyle(document.documentElement).getPropertyValue('--celebrityDetailsVideoScroll');

      // if(pnga_celebrity_video_scroll){

      //     document.getElementById('CelebrityVideoContainer').style.backgroundImage = 'url("CSS/' + pnga_celebrity_video_scroll +'?a='+Math.random() +'")'

      // }

      var CelebrityVideoClicked = document.getElementsByClassName(
        'CelebrityVideoThumb',
      );

      for (var i = 0; i < CelebrityVideoClicked.length; i++) {
        CelebrityVideoClicked[i].onclick = function () {
          CelebrityVideo(
            this,
            this.getElementsByTagName('img')[0],
            dataCvideo,
            0,
            settings,
          );
        };

        // CelebrityVideoClicked[i].onclick = function () { CelebrityVideo(this, this, celebrityVideos, time, settings) }
      }
    }
  }
  // when the viewer clicks a celebrity tag or a celebrity item in the menu scroll section
  function CelebrityClicked(CelebrityClicked, time, settings) {
    var livelayer = false;

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (livelayer == false) {
      HideAll(settings);

      HideAllActive(settings);
    }

    // extract the id
    var DivIdCelebrity = CelebrityClicked.id.split('_', 2)[1]; //celebrity_

    if (CelebrityDetailsData.length == 0) {
      var CelebrityDetails = CelebrityTagData.find(
        x => x.id_celebrity == DivIdCelebrity,
      );
    } else {
      var CelebrityDetails = CelebrityDetailsData.find(
        x => x.id_celebrity == DivIdCelebrity,
      );
    }

    // SEND TO ANALYTICS
    AnalyticsCelebrityDetailsView(DivIdCelebrity, settings, time);

    var url;

    if (time == 0) {
      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/CelebrityVideoAll/' +
          settings.ProgramCode +
          '/' +
          DivIdCelebrity;

        // when the user has defined in the settings that the scroll menu should not be closed
        // we set the scroll menu back to visible again depending on the celebrity group
        if (settings.scrollSectionVisible == 1) {
          if (CelebrityDetails.customGroup == 0) {
            document.getElementById('Celebrity').style.display = 'block';
          } else {
            document.getElementById('CelebrityCustom').style.display = 'block';
          }
        }

        var CelebrityVP =
          '<div class="CelebrityDetails" id="CelebrityDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-index:100;">' +
          '<div id="CelebrityDetailsClose" class="CelebrityDetailsClose" style="pointer-events:initial;"></div>';
      } else {
        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Celebrity/Video/' +
          DivIdCelebrity +
          '/' +
          settings.ProgramCode +
          '/0/' +
          id_user;

        // when the user has defined in the settings that the scroll menu should not be closed
        // we set the scroll menu back to visible again depending on the celebrity group
        if (settings.scrollSectionVisible == 1) {
          if (CelebrityDetails.customGroup == 0) {
            document.getElementById('Celebrity').style.display = 'block';
          } else {
            document.getElementById('CelebrityCustom').style.display = 'block';
          }
        }

        var CelebrityVP =
          '<div class="CelebrityDetails" id="CelebrityDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-index:100;">' +
          '<div id="CelebrityDetailsClose" class="CelebrityDetailsClose" style="pointer-events:initial;"></div>';
      }
    } else {
      //click from tag

      // EWALD

      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/CelebrityVideoSpecific/' +
          settings.ProgramCode +
          '/' +
          time +
          '/' +
          DivIdCelebrity;

        if (CelebrityDetails.X < 50) {
          // tag on the left side of the screen

          var CelebrityVP =
            '<div class="CelebrityDetailsRight" id="CelebrityDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-index:100;">' +
            '<div id="CelebrityDetailsClose" class="CelebrityDetailsCloseRight" style="pointer-events:initial;"></div>';
        } else {
          // tag on the right side of the screen

          var CelebrityVP =
            '<div class="CelebrityDetailsLeft" id="CelebrityDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-index:100;">' +
            '<div id="CelebrityDetailsClose" class="CelebrityDetailsCloseLeft" style="pointer-events:initial;"></div>';
        }
      } else {
        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Celebrity/Video/' +
          DivIdCelebrity +
          '/' +
          settings.ProgramCode +
          '/' +
          time +
          '/' +
          id_user;

        if (CelebrityDetails.x < 50) {
          // tag on the left side of the screen

          var CelebrityVP =
            '<div class="CelebrityDetailsRight" id="CelebrityDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-index:100;">' +
            '<div id="CelebrityDetailsClose" class="CelebrityDetailsCloseRight" style="pointer-events:initial;"></div>';
        } else {
          // tag on the right side of the screen

          var CelebrityVP =
            '<div class="CelebrityDetailsLeft" id="CelebrityDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-index:100;">' +
            '<div id="CelebrityDetailsClose" class="CelebrityDetailsCloseLeft" style="pointer-events:initial;"></div>';
        }
      }
    }

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.send();

    xhr.onreadystatechange = processRequest;

    function processRequest(e) {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var celebrityVideos = JSON.parse(xhr.responseText);

        document.getElementById('CelebrityDetails').innerHTML = '';

        var CelebVideo = '';

        var CelebVideoDynamic = '';

        var FavoriteCelebrity = '';

        if (CelebrityDetails.BirthName != null) {
          var BirthName = CelebrityDetails.BirthName;
        } else {
          var BirthName = '';
        }
        if (CelebrityDetails.BirthDate != null) {
          var BirthDate = CelebrityDetails.BirthDate;
        } else {
          var BirthDate = '';
        }
        if (CelebrityDetails.BirthPlace != null) {
          var BirthPlace = CelebrityDetails.BirthPlace;
        } else {
          BirthPlace = '';
        }

        if (celebrityVideos.length > 0) {
          for (i = 0; i < celebrityVideos.length; i++) {
            CelebVideoDynamic +=
              '' +
              '<div id="celebrityVideo_' +
              celebrityVideos[i].id_celebrityVideo +
              '" class="CelebrityVideoThumb" style="pointer-events: initial">' +
              '<img class="CelebrityVideoThumbImage" src="https://' +
              ClientCode +
              '.showtagenabled.com/video/' +
              celebrityVideos[i].thumbUrl +
              '" />' +
              '<div class="CelebrityThumbPlaybutton"></div>' +
              '</div>';
          }

          CelebVideo =
            '<div class="CelebrityVideoContainer" id="CelebrityVideoContainer" style="position: absolute; pointer-events:none;"><div class="CelebrityVideoScroll" style="pointer-events:initial;">' +
            CelebVideoDynamic +
            '</div></div>';
        }

        if (settings.favoriteButtonsActivated == 1) {
          FavoriteCelebrity =
            '' +
            '<div class="CelebrityFavoriteIcon" id="Celebrity' +
            DivIdCelebrity +
            '" style="position: absolute;"></div>' +
            '<div class="CelebrityFavoriteIconRemove" id="Remove' +
            DivIdCelebrity +
            '" style="position: absolute; display:none;"></div>';
        } else {
          FavoriteCelebrity = '';
        }

        if (CelebrityDetails.Instragram != null) {
          instagram =
            '<div class="CelebrityInstagram" style="position: absolute; pointer-events: initial">' +
            '<div class="CelebrityInstagramImage"></div>' +
            '<a style="text-decoration: none;" href="https://www.instagram.com/' +
            CelebrityDetails.Instragram +
            '" target="_blank" class="CelebrityInstagramText">' +
            CelebrityDetails.Instragram +
            '</a>' +
            '</div>';
        } else {
          instagram = '';
        }

        if (CelebrityDetails.Description != null) {
          description = CelebrityDetails.Description;
        } else {
          description = '';
        }

        var CDDynamic = document.createElement('div');
        CDDynamic.id = 'celebrity_' + CelebrityDetails.id_celebrity;
        CDDynamic.innerHTML =
          '' +
          CelebrityVP +
          '<div class="CelebrityName" style="position: absolute;">' +
          CelebrityDetails.Name +
          '</div>' +
          '<img class="CelebrityImage" style="position: absolute;" src="https://' +
          ClientCode +
          '.showtagenabled.com/images/' +
          CelebrityDetails.CelebrityImage +
          '" />' +
          FavoriteCelebrity +
          '<div class="CelebrityExtra" style="position: absolute;"></div>' +
          '<div class="CelebrityPersonalia" style="position: absolute;">' +
          BirthName +
          '</br >' +
          BirthDate +
          '</br >' +
          BirthPlace +
          '</br >' +
          '</div >' +
          '<div class="CelebrityDescription" style="position:absolute; pointer-events: initial;">' +
          description +
          '</div>' +
          instagram +
          CelebVideo +
          '</div>';

        document.getElementById('CelebrityDetails').appendChild(CDDynamic);

        document.getElementById('CelebrityDetails').style.display = 'block';

        // in case the celebrity as an aminiated png we reload the background each time
        let pnga_celebrity_details = getComputedStyle(
          document.documentElement,
        ).getPropertyValue('--celebrityDetails');

        if (pnga_celebrity_details) {
          document.getElementById('CelebrityDetailsID').style.backgroundImage =
            'url("' +
            baseUrlStylesheet +
            '/' +
            pnga_celebrity_details +
            '?a=' +
            Math.random() +
            '")';
        }

        // CLICK THE CELEBRITY VIDEO
        if (celebrityVideos.length > 0) {
          // in case the celebrity as an aminiated png we reload the background each time
          let pnga_celebrity_video_scroll = getComputedStyle(
            document.documentElement,
          ).getPropertyValue('--celebrityDetailsVideoScroll');

          if (pnga_celebrity_video_scroll) {
            document.getElementById(
              'CelebrityVideoContainer',
            ).style.backgroundImage =
              'url("' +
              baseUrlStylesheet +
              '/' +
              pnga_celebrity_video_scroll +
              '?a=' +
              Math.random() +
              '")';
          }

          var CelebrityVideoClicked = document.getElementsByClassName(
            'CelebrityVideoThumb',
          );

          for (var i = 0; i < CelebrityVideoClicked.length; i++) {
            CelebrityVideoClicked[i].onclick = function () {
              CelebrityVideo(
                this,
                CelebrityClicked,
                celebrityVideos,
                time,
                settings,
              );
            };
          }
        }

        // CLICK THE FAVORITE ICON
        if (settings.favoriteButtonsActivated == 1) {
          // CLICK THE CELEBRITY FAVORITE ICON (ADD)

          var CelebrityFavoriteIcon = document.getElementsByClassName(
            'CelebrityFavoriteIcon',
          );

          for (var i = 0; i < CelebrityFavoriteIcon.length; i++) {
            CelebrityFavoriteIcon[i].onclick = function () {
              CelebrityFavoriteIconClicked(
                DivIdCelebrity,
                settings.ProgramCode,
              );
            };
          }

          // CLICK THE CELEBRITY FAVORITE ICON (REMOVE)

          var CelebrityFavoriteIconRemove = document.getElementsByClassName(
            'CelebrityFavoriteIconRemove',
          );

          for (var i = 0; i < CelebrityFavoriteIconRemove.length; i++) {
            CelebrityFavoriteIconRemove[i].onclick = function () {
              CelebrityFavoriteRemove(DivIdCelebrity, settings.ProgramCode);
            };
          }
        }

        // CLOSE THE CELEBRITY DETAILS
        document.getElementById('CelebrityDetailsClose').onclick =
          CloseCelebrityDetails;

        // close the celebrity video
        function CloseCelebrityDetails() {
          document.getElementById('CelebrityDetails').style.display = 'none';
        }
      }
    }
  }
  // when viewer clicks the favorite button we store this in the database
  function CelebrityFavoriteIconClicked(id_celebrity, ProgramCode) {
    var Celebrity = 'Celebrity' + id_celebrity;
    var removeCelebrity = 'Remove' + id_celebrity;

    url =
      'https://' +
      ClientCode +
      '.azurewebsites.net/api/User/Favorites/AddCelebrity/' +
      id_celebrity +
      '/' +
      ProgramCode +
      '/' +
      id_user;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', url);
    xmlhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xmlhttp.send(JSON.stringify());

    document.getElementById(Celebrity).style.display = 'none';
    document.getElementById(removeCelebrity).style.display = 'block';
  }
  // when the viewer clicks the favorite button we update this in the database
  function CelebrityFavoriteRemove(id_celebrity, ProgramCode) {
    var Celebrity = 'Celebrity' + id_celebrity;
    var removeCelebrity = 'Remove' + id_celebrity;

    url =
      'https://' +
      ClientCode +
      '.azurewebsites.net/api/User/Favorites/DeleteCelebrity/' +
      id_celebrity +
      '/' +
      id_user;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('DELETE', url);
    xmlhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xmlhttp.send(JSON.stringify());

    document.getElementById(removeCelebrity).style.display = 'none';
    document.getElementById(Celebrity).style.display = 'block';
  }
  // check if there is tag information for this time segment
  function CelebrityTags(currentTime, settings) {
    celebrityTags = false;

    if (studio == true) {
      url =
        'https://tagapi.azurewebsites.net/api/Prepare/celebrities/' +
        settings.ProgramCode +
        '/' +
        currentTime;
    } else {
      url =
        'https://' +
        ClientCode +
        '.azurewebsites.net/api/Celebrity/' +
        settings.ProgramCode +
        '/' +
        currentTime +
        '/' +
        id_user;
    }

    var xhrCelebrity = new XMLHttpRequest();
    xhrCelebrity.open('GET', url, true);
    xhrCelebrity.send();
    xhrCelebrity.onreadystatechange = processRequest;

    function processRequest(e) {
      if (xhrCelebrity.readyState == 4 && xhrCelebrity.status == 200) {
        Celebrities = JSON.parse(xhrCelebrity.responseText);

        if (Celebrities.length != 0) {
          ShowCelebrityTags(Celebrities, settings, currentTime);

          celebrityTags = true;
        }
      }
    }
  }
  // if tags exist for the time segment show them on the screen
  function ShowCelebrityTags(Celebrities, settings, currentTime) {
    CelebrityTagData = Celebrities;

    document.getElementById('CelebrityTag').style.display = 'block';

    document.getElementById('CelebrityTag').innerHTML = '';

    var dynamic = '';

    var CelebrityCard = '';

    var hoverLeft = '';

    var hoverRight = '';

    var buttonMore = '';

    // minimize the menu if there are tags on the screen
    if (settings.menuMinimizeTag == 1) {
      // hide the menu of there are tags on the screen

      buttonContainerMinimize('tag', settings);
    }

    for (i = 0; i < Celebrities.length; i++) {
      // load the default settings when hover over the tag the name becomes visible
      // click on tag for details
      if (
        settings.deactivateTagHoover != 1 &&
        settings.deactivateProgramDetails != 1
      ) {
        hoverLeft =
          '<div class="CelebrityTagTextLeft tagText" style="text-align:left; float: left; margin-left: 10px;">' +
          Celebrities[i].Name +
          '</div>';

        hoverRight =
          '<div class="CelebrityTagTextRight tagText" style="float: right; position:relative; text-align:right;">' +
          Celebrities[i].Name +
          '</div>';
      }

      // load where the name becomes directly visible, click on tag for details
      if (settings.deactivateTagHoover == 1) {
        hoverLeft =
          '<div class="CelebrityTagTextLeft tagText" style="text-align:left; float: left; margin-left: 10px; display:block">' +
          Celebrities[i].Name +
          '</div>';

        hoverRight =
          '<div class="CelebrityTagTextRight tagText" style="right: 10px; position:absolute; text-align:right; display:block">' +
          Celebrities[i].Name +
          '</div>';
      }

      // load all celebrity information next to the tag, click for details deactivated
      if (settings.deactivateProgramDetails == 1) {
        var CelebVideo = '';

        var CelebVideoDynamic = '';

        var FavoriteCelebrity = '';

        var instagram = '';

        var description = '';

        var BirthPlace = '';

        var BirthDate = '';

        var BirthName = '';

        if (Celebrities[i].BirthName != null) {
          var BirthName = Celebrities[i].BirthName;
        }

        if (Celebrities[i].BirthDate != null) {
          var BirthDate = Celebrities[i].BirthDate;
        }

        if (Celebrities[i].BirthPlace != null) {
          var BirthPlace = Celebrities[i].BirthPlace;
        }

        // if (celebrityVideos.length > 0) {

        //     for (i = 0; i < celebrityVideos.length; i++) {

        //         CelebVideoDynamic += ''

        //             + '<div id="celebrityVideo_' + celebrityVideos[i].id_celebrityVideo + '" class="CelebrityVideoThumb" style="pointer-events: initial">'
        //             + '<img class="CelebrityVideoThumbImage" src="https://' + ClientCode + '.showtagenabled.com/video/' + celebrityVideos[i].thumbUrl + '" />'
        //             + '<div class="CelebrityThumbPlaybutton"></div>'
        //             + '</div>'

        //     }

        //     CelebVideo = '<div class="CelebrityVideoContainer" id="CelebrityVideoContainer" style="position: absolute; pointer-events:none;"><div class="CelebrityVideoScroll" style="pointer-events:initial;">'
        //         + CelebVideoDynamic
        //         + '</div></div>'

        // };

        if (settings.favoriteButtonsActivated == 1) {
          FavoriteCelebrity =
            '' +
            '<div class="addToFavoriteButton" id="Celebrity' +
            Celebrities[i].id_celebrity +
            '"></div>' +
            '<div class="addToFavoriteButtonRemove" id="Remove' +
            Celebrities[i].id_celebrity +
            '" style="display:none;"></div>';
        }

        // if there is an external link is connected
        // to any social platform we show the QR code
        // this QR code will bring us to a webpage with
        // the link tree etc.
        if (
          Celebrities[i].Instragram != null ||
          Celebrities[i].facebook != null ||
          Celebrities[i].snapchat != null ||
          Celebrities[i].tiktok != null ||
          Celebrities[i].twitter != null ||
          Celebrities[i].youtube != null
        ) {
          // if the platform is Android we show a QR code to scan instead of the link
          if (androidTV == true) {
            // adds the qr code
            instagram =
              '<div class="CelebrityInstagram">' +
              '<div class="qrCode" id="QRCodeInstagram' +
              Celebrities[i].id_celebrity +
              '"></div>';
            +'</div>';

            buttonMore =
              '<button class="buttonMore" id="' +
              Celebrities[i].id_celebrity +
              '" style="pointer-events:initial;">More</button>';

            // if mobile or web we load the instagram with a link
          } else {
            instagram =
              '<div class="CelebrityInstagram" style="position: absolute; pointer-events: initial">' +
              '<div class="CelebrityInstagramImage"></div>' +
              '<a style="text-decoration: none;" href="https://www.instagram.com/' +
              Celebrities[i].Instragram +
              '" target="_blank" class="CelebrityInstagramText">' +
              Celebrities[i].Instragram +
              '</a>' +
              '</div>';
          }
        }

        if (Celebrities[i].Description != null) {
          description = Celebrities[i].Description;
        }

        if (Celebrities[i].x < 50) {
          cardPosition =
            '<div class="CelebrityCardLeft CelebrityCard" id="Cardcelebrity_' +
            Celebrities[i].id_celebrity +
            '" style="pointer-events: none; user-select: none; z-index:100;">';
        } else {
          cardPosition =
            '<div class="CelebrityCardRight CelebrityCard" id="Cardcelebrity_' +
            Celebrities[i].id_celebrity +
            '" style="pointer-events: none; user-select: none; z-index:100;">';
        }

        CelebrityCard =
          '' +
          cardPosition +
          '<img class="CelebrityImage" src="https://' +
          ClientCode +
          '.showtagenabled.com/images/' +
          Celebrities[i].CelebrityImage +
          '" />' +
          '<div class="CelebrityName">' +
          Celebrities[i].Name +
          '</div>' +
          '<div class="CelebrityPersonalia">' +
          BirthName +
          ' ' +
          BirthDate +
          ' ' +
          BirthPlace +
          ' ' +
          '</div >' +
          '<div class="CelebrityDescription" style="pointer-events: initial;" id="desc' +
          Celebrities[i].id_celebrity +
          '">' +
          description +
          '</div>' +
          '<div class="CelebrityOptionContainer" id="more' +
          Celebrities[i].id_celebrity +
          '">' +
          instagram +
          '</div>' +
          '</div>' +
          '<div class="CelebrityButtonContainer">' +
          FavoriteCelebrity +
          buttonMore +
          '</div>' +
          // + CelebVideo
          '</div>';
      }

      // divide the frame in two and show on left or right side of the screen
      if (Celebrities[i].x < 50) {
        // left

        dynamic +=
          '<div style="top:' +
          Celebrities[i].y +
          '%; left:' +
          Celebrities[i].x +
          '%; position:absolute; z-index:10;">' +
          '<div id="celebrity_' +
          Celebrities[i].id_celebrity +
          '" class="tag CelebrityTag" style="float: left; pointer-events: initial;">' +
          CelebrityCard +
          hoverLeft +
          '</div>' +
          '</div>';
      } else {
        // right

        dynamic +=
          '<div style="top:' +
          Celebrities[i].y +
          '%; left:' +
          Celebrities[i].x +
          '%; position:absolute; z-index: 10;">' +
          '<div id="celebrity_' +
          Celebrities[i].id_celebrity +
          '" class="tag CelebrityTag" style="position:absolute; pointer-events: initial;">' +
          CelebrityCard +
          hoverRight +
          '</div>' +
          '</div>';
      }
    }

    var CelebrityTagsContent = document.createElement('div');

    CelebrityTagsContent.id = 'CelebritiesTagContainer';

    CelebrityTagsContent.innerHTML = dynamic;

    document.getElementById('CelebrityTag').appendChild(CelebrityTagsContent);

    if (androidTV == true) {
      // this section only for TV app
      // we need to wait for the div's to be present in the dom before we can append
      for (i = 0; i < Celebrities.length; i++) {
        // if an URL is present we show the QR code, else we igonore it
        if (Celebrities[i].Instragram != null) {
          //https://development.showtagenabled.com?C=2081&c=59819&u=45897588&p=202007051209-2081

          // create the QR code and connect it to the unique id
          // EWALD: we need to change the address to: clientcode + showtagenabled.com/ProductDetailsData[i].id_product
          // where we need to capture the product id, find the URL and then redirect the user to the website
          new QRCode(
            document.getElementById(
              'QRCodeInstagram' + Celebrities[i].id_celebrity,
            ),
            'https://development.showtagenabled.com?C=2081&c=' +
              Celebrities[i].id_celebrity +
              '&u=45897588&p=202007051209-2081',
          );
        }
      }

      var CelebrityTag = document.getElementsByClassName('CelebrityTag');

      var CelebrityCardx = document.getElementsByClassName('CelebrityCard');

      var ProductTagContainer = document.getElementsByClassName(
        'ProductTagContainer',
      );

      for (var i = 0; i < CelebrityTag.length; i++) {
        // click on a tag and show
        CelebrityTag[i].onclick = function () {
          // hide all celebrity tag containers
          for (var i = 0; i < CelebrityTag.length; i++) {
            CelebrityCardx[i].style.display = 'none';
          }

          // hide all product tag containers
          for (var i = 0; i < ProductTagContainer.length; i++) {
            ProductTagContainer[i].style.display = 'none';
          }

          var CelebrityCardId = 'Card' + this.id;

          document.getElementById(CelebrityCardId).style.display = 'block';
        };
      }
    }

    // show linktree modal
    var clickme = document.getElementsByClassName('buttonMore');

    for (var i = 0; i < clickme.length; i++) {
      clickme[i].onclick = function () {
        var theID = 'more' + this.id;

        var theIDdesc = 'desc' + this.id;

        var x = document.getElementById(theID);

        if (x.style.display == 'none' || x.style.display == '') {
          x.style.display = 'block';

          document.getElementById(theIDdesc).style.display = 'none';
        } else {
          x.style.display = 'none';

          document.getElementById(theIDdesc).style.display = 'block';
        }
      };
    }

    // when program details are not disabled we activate the click on the tag to show details
    if (settings.deactivateProgramDetails != 1) {
      var CelebrityTag = document.getElementsByClassName('CelebrityTag');

      for (var i = 0; i < CelebrityTag.length; i++) {
        CelebrityTag[i].onclick = function () {
          CelebrityClicked(this, currentTime, settings);
        };
      }
    }
  }

  // CELEBRITY VIDEO

  // celebrity video loaded from celebrity details
  function CelebrityVideo(
    celebrityVideo,
    celeb,
    celebrityVideos,
    time,
    settings,
  ) {
    var DivIdCelebrity = celeb.id.split('_', 2)[1];

    // when time is 0 this means that the celebrity video is clicked from the celebrity scroll overview
    if (time == 0) {
      // we hide the celebrity scroll window
      document.getElementById('Celebrity').style.display = 'none';
    }

    document.getElementById('CelebrityVideo').innerHTML = '';

    document.getElementById('CelebrityDetails').style.display = 'none';

    var DivIdCelebrityVideo = celebrityVideo.id.split('_', 2)[1];

    var videoDetails = celebrityVideos.find(
      x => x.id_celebrityVideo == DivIdCelebrityVideo,
    );

    var CVDynamic = document.createElement('div');

    CVDynamic.innerHTML =
      '' +
      '<div id="CelebrityVideoPlayer" class="CelebrityVideoPlayer" style="pointer-events: none; position:absolute; z-index:100">' +
      '<video style="pointer-events: initial" id="VideoInVideo" controls controlsList="nofullscreen nodownload noremoteplayback" src="https://' +
      ClientCode +
      '.showtagenabled.com/video/' +
      videoDetails.videoUrl +
      '"></video> ' +
      '<div id="CelebrityVideoClose" style="pointer-events: initial"></div>' +
      '</div>';

    document.getElementById('CelebrityVideo').appendChild(CVDynamic);

    // delay video playback for 1 second

    var celebVideo = document.getElementById('VideoInVideo');

    setTimeout(function () {
      celebVideo.play();

      // analytics detect when the video starts playing
      AnalyticsVideoInVideoStart(celebrityVideo.id, settings, time);
    }, 1000);

    document.getElementById('CelebrityVideo').style.display = 'Block';

    document.getElementById('CelebrityVideoClose').onclick = CloseVideoInVideo;

    // in case the celebrity as an animated png we reload the background each time
    let pnga_celebrity_video_player = getComputedStyle(
      document.documentElement,
    ).getPropertyValue('--celebrityDetailsVideoPlayer');

    if (pnga_celebrity_video_player) {
      document.getElementById('CelebrityVideoPlayer').style.backgroundImage =
        'url("' +
        baseUrlStylesheet +
        '/' +
        pnga_celebrity_video_player +
        '?a=' +
        Math.random() +
        '")';
    }

    function CloseVideoInVideo() {
      // we hide the celebrity video modal
      document.getElementById('CelebrityVideoPlayer').style.display = 'none';

      // we destroy the contents of the celebrity video
      document.getElementById('CelebrityVideo').innerHTML = '';

      // when time is 0 this means that the celebrity video is clicked from the celebrity scroll overview
      if (time == 0) {
        // we show the celebrity scroll window
        document.getElementById('Celebrity').style.display = 'block';
      }

      // video has been clicked from celebrity details connected to a tag
      if (time != 0) {
        // we open the celebrity details again
        CelebrityClicked(celeb, time, settings);
      }

      // analytics
      AnalyticsVideoClosed(celebrityVideo.id, settings, time);
    }
  }
  // when the viewer clicks the menu button we show all connected videos
  function GetCelebrityVideoOverview(settings, ProgramCode) {
    var livelayer = false;

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    // if the livelayer is activated we clear the optional buffer of the videos
    // so that all videos are requested again, this allows the user to update
    // the videos in the programme
    if (livelayer == false) {
      HideAll(settings);

      HideAllActive(settings);
    } else {
      CelebrityVideoOverviewdata = []; // THIS WILL INVOKE A RELOAD ON THE DATA EACH TIME
    }

    // check if the videos have been loaded before, if that is the case we don't request the videos
    // again but simply make the overview visible on the screen, if not loaded before we request the
    // videos from the storage
    if (CelebrityVideoOverviewdata.length == 0) {
      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/CelebrityVideoAllinProgram/' +
          settings.ProgramCode;
      } else {
        // EWALD

        url =
          'https://tagapi.azurewebsites.net/api/Prepare/CelebrityVideoAllinProgram/' +
          settings.ProgramCode;

        //url = 'https://' + ClientCode + '.azurewebsites.net/api/Menu/CelebritiesInProgram/' + settings.ProgramCode + '/' + id_user + '/' + client_id
      }

      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.send();

      xhr.onreadystatechange = processRequest;

      function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
          CelebrityVideoOverviewdata = JSON.parse(xhr.responseText);

          var CVODynamic = '';

          document.getElementById('CelebrityVideoOverview').innerHTML = '';

          // loop over all the video elements connected to all the celebrities
          for (i = 0; i < CelebrityVideoOverviewdata.length; i++) {
            // non require section in the script we therefor need to capture the null
            // and make sure it is not displayed to the viewers
            var videoDescription = '';

            if (CelebrityVideoOverviewdata[i].videoDescription != null) {
              videoDescription = CelebrityVideoOverviewdata[i].videoDescription;
            }

            CVODynamic +=
              '' +
              '<div class="CelebrityVideoItemContainer" id="' +
              CelebrityVideoOverviewdata[i].id_celebrityVideo +
              '">' +
              '<img class="CelebrityVideoThumbImage" src="https://' +
              ClientCode +
              '.showtagenabled.com/video/' +
              CelebrityVideoOverviewdata[i].thumbUrl +
              '" />' +
              '<div class="CelebrityVideoThumbPlaybutton"></div>' +
              '<div class="CelebrityVideoName">' +
              CelebrityVideoOverviewdata[i].videoName +
              '</div>' +
              '<div class="CelebrityVideoDescription">' +
              videoDescription +
              '</div>' +
              '</div>';
          }

          document.getElementById('CelebrityVideoOverview').innerHTML =
            '' +
            '<div id="CelebrityVideoOverviewContainer" class="MenuContainer" style="position: absolute; pointer-events:none; z-index:100;">' +
            '<div class="MenuTitle">' +
            settings.celebrityVideoOverviewText +
            '</div>' +
            '<div class="MenuContainerScroll" style="pointer-events:initial">' +
            CVODynamic +
            '</div>' +
            '</div>';
        }

        document.getElementById('CelebrityVideoOverview').style.display =
          'block';

        // in case an aminiated png we reload the background each time
        let pnga_celebrity_video_overview = getComputedStyle(
          document.documentElement,
        ).getPropertyValue('--scrollContainerBackgroundCelebrityVideoOverview');

        if (pnga_celebrity_video_overview) {
          document.getElementById(
            'CelebrityVideoOverviewContainer',
          ).style.backgroundImage =
            'url("' +
            baseUrlStylesheet +
            '/' +
            pnga_celebrity_video_overview +
            '?a=' +
            Math.random() +
            '")';
        }

        // CLICK THE CELEBRITY VIDEO
        // the celebrity video is loaded over the videos that are in the scroll menu
        if (settings.CelebrityVideoInProgram == 1) {
          var CelebrityVideoClicked = document.getElementsByClassName(
            'CelebrityVideoItemContainer',
          );

          for (var i = 0; i < CelebrityVideoClicked.length; i++) {
            CelebrityVideoClicked[i].onclick = function () {
              CelebrityVideoOverviewPlayer(this, settings);
            };
          }
        }
      }
    } else {
      // load from buffer if video's already have been downloaded once

      document.getElementById('CelebrityVideoOverview').style.display = 'block';

      // in case an aminiated png we reload the background each time
      let pnga_celebrity_video_overview = getComputedStyle(
        document.documentElement,
      ).getPropertyValue('--scrollContainerBackgroundCelebrityVideoOverview');

      if (pnga_celebrity_video_overview) {
        document.getElementById(
          'CelebrityVideoOverviewContainer',
        ).style.backgroundImage =
          'url("' +
          baseUrlStylesheet +
          '/' +
          pnga_celebrity_video_overview +
          '?a=' +
          Math.random() +
          '")';
      }
    }
  }
  // play the video's from the celebrity video overview
  function CelebrityVideoOverviewPlayer(CelebrityVideo, settings) {
    document.getElementById('CelebrityVideo').innerHTML = '';

    var DivIdCelebrityVideo = CelebrityVideo.id;

    var videoDetails = CelebrityVideoOverviewdata.find(
      x => x.id_celebrityVideo == DivIdCelebrityVideo,
    );

    var CVDynamic = document.createElement('div');

    CVDynamic.innerHTML =
      '' +
      '<div id="CelebrityVideoPlayer" class="CelebrityVideoPlayer" style="pointer-events: initial; position:absolute; z-index:1000000000">' +
      '<video id="VideoInVideo" controls controlsList="nofullscreen nodownload noremoteplayback" src="https://' +
      ClientCode +
      '.showtagenabled.com/video/' +
      videoDetails.videoUrl +
      '"></video> ' +
      '<div id="CelebrityVideoClose"></div>' +
      '</div>';

    document.getElementById('CelebrityVideo').appendChild(CVDynamic);

    document.getElementById('CelebrityVideo').style.display = 'block';

    // in case an aminiated png we reload the background each time

    let pnga_celebrity_video_player = getComputedStyle(
      document.documentElement,
    ).getPropertyValue('--celebrityVideoOverviewPlayer');

    if (pnga_celebrity_video_player) {
      document.getElementById('CelebrityVideoPlayer').style.backgroundImage =
        'url("' +
        baseUrlStylesheet +
        '/' +
        pnga_celebrity_video_player +
        '?a=' +
        Math.random() +
        '")';
    }

    // delay video playback for 1 second

    var celebVideo = document.getElementById('VideoInVideo');

    setTimeout(function () {
      celebVideo.play();

      AnalyticsVideoInVideoStart(CelebrityVideo.id, settings, 0);
    }, 1000);

    document.getElementById('CelebrityVideoClose').onclick = CloseVideoInVideo;

    // close the celebrity video
    function CloseVideoInVideo() {
      document.getElementById('CelebrityVideoPlayer').style.display = 'none';

      document.getElementById('CelebrityVideo').innerHTML = '';

      AnalyticsVideoClosed(CelebrityVideo.id, settings, 0);
    }
  }

  // PRODUCTS
  // when the viewer clicks the product button in the menu we show all the products in a scroll
  function GetAllProducts(settings, ProgramCode) {
    var livelayer = false;

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (livelayer == false) {
      HideAll(settings);

      HideAllActive(settings);
    }

    if (ProductData.length == 0) {
      var ButtonCelebrityLoader = document.getElementById(
        'ButtonProductLoader',
      ).style;
      ButtonCelebrityLoader.opacity = 1;
      (function fade() {
        (ButtonCelebrityLoader.opacity += 0.1) > 0
          ? (ButtonCelebrityLoader.display = 'block')
          : setTimeout(fade, 40);
      })();

      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/productsInProgram/' +
          settings.ProgramCode;
      } else {
        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Menu/productsinprogram/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id;
      }

      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.send();

      xhr.onreadystatechange = processRequest;

      function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
          ProductDetailsData = JSON.parse(xhr.responseText);

          ProductListing(settings, ProductDetailsData);
        }
      }
    } else {
      ProductListing(settings, ProductDetailsData);
    }
  }
  // create the product scroll listing for the menu
  function ProductListing(settings, ProductDetailsData) {
    document.getElementById('Product').innerHTML = '';

    var dynamic = '';

    var FavoriteProduct = '';

    var qrCode = '';

    var externalLinkButton = '';

    for (i = 0; i < ProductDetailsData.length; i++) {
      var productPrice = '';

      if (ProductDetailsData[i].ProductCustomGroup == false) {
        // if the favorites are activated in the settings section we show the favorites button
        if (settings.favoriteButtonsActivated == 1) {
          FavoriteProduct =
            '' +
            '<div class="addToFavoriteButton" id="Product' +
            ProductDetailsData[i].id_product +
            '" style="position: absolute; pointer-events:initial;"></div>' +
            '<div class="addToFavoriteButtonRemove" id="RemoveProduct' +
            ProductDetailsData[i].id_product +
            '" style="position: absolute; display: none; pointer-events:initial;"></div>';
        }

        if (androidTV == true) {
          qrCode =
            '<div class="qrCode" id="qrCode' +
            ProductDetailsData[i].id_product +
            '"></div>';
        }

        // if there is a value in the price (float) we show the price section
        if (ProductDetailsData[i].Price != null) {
          productPrice =
            '<div class="ProductPrice">' +
            ProductDetailsData[i].Price +
            '</div>';
        }

        // if the product details are deactivated we show the buy now button the in the product listing
        if (settings.deactivateProgramDetails == 1) {
          externalLinkButton =
            '<a class="MenuCellButton" href="' +
            ProductDetailsData[i].Url +
            '">' +
            ProductDetailsData[i].ButtonText +
            '</a>';
        }

        dynamic +=
          '<div id="' +
          ProductDetailsData[i].id_product +
          '"class="MenuCellContainerProducts">' +
          '<div class="MenuCellBackground">' +
          '<img src="https://' +
          ClientCode +
          '.showtagenabled.com/images/' +
          ProductDetailsData[i].ProductImage +
          '" />' +
          '<div class="MenuCellName" style="position:absolute; user-select:none;"><span class="MenuCellBrandName">' +
          ProductDetailsData[i].BrandName +
          ' </span>' +
          ProductDetailsData[i].ProductName +
          '</div>' +
          productPrice +
          FavoriteProduct +
          qrCode +
          externalLinkButton +
          '</div>' +
          '</div>';
      }
    }

    var ProductContainerContent = document.createElement('div');
    ProductContainerContent.innerHTML =
      '' +
      '<div class="MenuContainer" id="ProductMenuContainer" style="pointer-events: initial; position:absolute">' +
      '<div class="MenuTitle">' +
      settings.ProductTopButtonText +
      '</div>' +
      '<div class="MenuContainerScroll" style=" pointer-events: initial; position:relative">' +
      dynamic +
      '</div>' +
      '</div>';

    document.getElementById('Product').appendChild(ProductContainerContent);

    if (androidTV == true) {
      // this section only for TV app
      // we need to wait for the div's to be present in the dom before we can append
      for (i = 0; i < ProductDetailsData.length - 1; i++) {
        // if an URL is present we show the QR code, else we igonore it
        if (ProductDetailsData[i].Url != null) {
          // create the QR code and connect it to the unique id
          // EWALD: we need to change the address to: clientcode + showtagenabled.com/ProductDetailsData[i].id_product
          // ?p=35274&u=59819&v=NetEiPPD
          // ?=id.product&u=id_user&v=programcode
          let str = 'example-string';
          let newPrC = ProgramCode.split('-')[0];

          // Encrypt

          function encryptNumber(num) {
            var encrypted = btoa(num);

            return encrypted;
          }

          var string =
            ProductDetailsData[i].id_product + '&u=' + id_user + '&v=' + newPrC;

          var encrypt = encryptNumber(string);

          // where we need to capture the product id, find the URL and then redirect the user to the website
          new QRCode(
            document.getElementById(
              'qrCode' + ProductDetailsData[i].id_product,
            ),
            'https://development.showtagenabled.com/?p=' + encrypt,
          );
        }
      }
    }

    document.getElementById('Product').style.display = 'block';

    document.getElementById('ButtonProductLoader').style.display = 'none';

    // if in the setting we deactivate the program details (we deactivate the details for celebrity and products and use direct cards)
    // we deactivate the click on the product
    if (settings.deactivateProgramDetails != 1) {
      var TopContainerCelProducts = document.getElementsByClassName(
        'MenuCellContainerProducts',
      );

      for (var i = 0; i < TopContainerCelProducts.length; i++) {
        TopContainerCelProducts[i].onclick = function () {
          ProductClicked(this, settings, 0);
        };
      }
    }
  }
  // when the viewer clicks a product tag or a product in the product scroll we show the details
  function ProductClicked(ClickedProduct, settings, currentTime) {
    HideAll(settings);

    HideAllActive(settings);

    var url;
    var ProductDetails;
    var DivIdProduct;
    var sourceTag = false;

    if (ProductDetailsData[0].ProductId) {
      // if the viewer clicked a product tag

      DivIdProduct = ClickedProduct.id;

      ProductDetails = ProductDetailsData.find(
        x => x.ProductId == DivIdProduct,
      );

      sourceTag = true;
    } else {
      // if the viewer clicked a product from the menu

      DivIdProduct = ClickedProduct.id;

      ProductDetails = ProductDetailsData.find(
        x => x.id_product == DivIdProduct,
      );
    }

    // SEND TO ANALYTICS
    AnalyticsProductDetailsView(DivIdProduct, settings, currentTime);

    document.getElementById('ProductDetails').innerHTML = '';

    // if the viewer clicked a product tag
    if (sourceTag == true) {
      if (ProductDetails.X < 50) {
        // tag on the left side of the screen

        var productVP =
          '<div class="ProductDetailsRight" id="ProductDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-indez:100;">' +
          '<div id="ProductDetailsClose" class="ProductDetailsCloseRight" style="pointer-events:initial;"></div>';
      } else {
        // tag on the right side of the screen

        var productVP =
          '<div class="ProductDetailsLeft" id="ProductDetailsID" style="position: absolute; pointer-events: none; user-select: none;z-indez:100;">' +
          '<div id="ProductDetailsClose" class="ProductDetailsCloseLeft" style="pointer-events:initial;"></div>';
      }
    } else {
      // user clicked a product in the menu

      var productVP =
        '<div class="ProductDetails" id="ProductDetailsID" style="position: absolute; pointer-events: none; user-select: none;z-indez:100;">' +
        '<div id="ProductDetailsClose" class="ProductDetailsClose" style="pointer-events:initial;"></div>';
    }

    if (settings.favoriteButtonsActivated == 1) {
      FavoriteProduct =
        '' +
        '<div class="productFavoriteIcon" id="Product' +
        DivIdProduct +
        '" style="position: absolute; style="pointer-events:initial;"></div>' +
        '<div class="productFavoriteIconRemove" id="RemoveProduct' +
        DivIdProduct +
        '" style="position: absolute; display: none; style="pointer-events:initial;"></div>';
    } else {
      FavoriteProduct = '';
    }

    if (ProductDetails.BrandName != null || ProductDetails.BrandName != '') {
      brandName = ProductDetails.BrandName;
    } else {
      brandName = '';
    }

    if (ProductDetails.UrlName != null && ProductDetails.ButtonText != null) {
      externallinkContainer =
        '' +
        '<div class="productExternalLinkContainer" style="position:absolute; pointer-events:initial;">' +
        '<div class="productexternallink"><a style="text-decoration: none;" href="' +
        ProductDetails.Url +
        '" target="_blank" class="" title="">' +
        ProductDetails.ButtonText +
        '<br /><span>' +
        ProductDetails.UrlName +
        '</span></a></div>' +
        '</div>';
    } else {
      externallinkContainer = '';
    }

    var PDDynamic = document.createElement('div');
    PDDynamic.innerHTML =
      '' +
      productVP +
      '<div class="ProductName" style="position: absolute;"><span id="brandName">' +
      brandName +
      '</span> ' +
      ProductDetails.ProductName +
      '</div>' +
      '<div class="ProductImageBackground" style="position: absolute;"><img class="ProductImage" src="https://' +
      ClientCode +
      '.showtagenabled.com/images/' +
      ProductDetails.ProductImage +
      '"/>' +
      FavoriteProduct +
      '</div > ' +
      '<div class="ProductExtra" style="position: absolute;"></div>' +
      '<div class="productdescription" style="position:absolute; style="pointer-events:initial;">' +
      ProductDetails.ProductDescription +
      '</div>' +
      externallinkContainer +
      '</div>';

    document.getElementById('ProductDetails').appendChild(PDDynamic);

    document.getElementById('ProductDetails').style.display = 'block';

    // close the product details
    var closeDiv = document.getElementById('ProductDetailsClose');
    closeDiv.onclick = function () {
      document.getElementById('ProductDetailsID').style.display = 'none';
    };

    // this loads the various options if the there would be an animated png background used
    if (sourceTag == true) {
      if (ProductDetails.X < 50) {
        // tag on the left side of the screen

        let pnga_product_details_right = getComputedStyle(
          document.documentElement,
        ).getPropertyValue('--productDetailsContainerRight');

        if (pnga_product_details_right) {
          document.getElementById('ProductDetailsID').style.backgroundImage =
            'url("' +
            baseUrlStylesheet +
            '/' +
            pnga_product_details_right +
            '?a=' +
            Math.random() +
            '")';
        }
      } else {
        // tag on the right side of the screen

        let pnga_product_details_left = getComputedStyle(
          document.documentElement,
        ).getPropertyValue('--productDetailsContainerLeft');

        if (pnga_product_details_left) {
          document.getElementById('ProductDetailsID').style.backgroundImage =
            'url("' +
            baseUrlStylesheet +
            '/' +
            pnga_product_details_left +
            '?a=' +
            Math.random() +
            '")';
        }
      }
    } else {
      // product detals loaded from the menu

      let pnga_product_details = getComputedStyle(
        document.documentElement,
      ).getPropertyValue('--productDetailsContainer');

      if (pnga_product_details) {
        document.getElementById('ProductDetailsID').style.backgroundImage =
          'url("' +
          baseUrlStylesheet +
          '/' +
          pnga_product_details +
          '?a=' +
          Math.random() +
          '")';
      }
    }

    if (
      settings.customStyleSheet != 1 &&
      settings.ProgramInformationActive == 1
    ) {
      document.getElementById('ProductDetailsID').style.left = 'unset';

      document.getElementById('ProductDetailsID').style.right = '1.5vw';
    }

    // SAVE CLICK TO ANALYTICS
    var productAnalyticsClickExternalLink = document.getElementsByClassName(
      'productexternallink',
    );

    for (var i = 0; i < productAnalyticsClickExternalLink.length; i++) {
      productAnalyticsClickExternalLink[i].onclick = function () {
        AnalyticsProductExternalLink(DivIdProduct, settings);
      };
    }

    if (settings.favoriteButtonsActivated == 1) {
      // CLICK THE PRODUCT FAVORITE ICON (ADD)

      var productFavoriteIcon = document.getElementsByClassName(
        'productFavoriteIcon',
      );

      for (var i = 0; i < productFavoriteIcon.length; i++) {
        productFavoriteIcon[i].onclick = function () {
          ProductFavoriteIconClicked(DivIdProduct, settings.ProgramCode);
        };
      }

      // CLICK THE PRODUCT FAVORITE ICON (REMOVE)

      var productFavoriteIconRemove = document.getElementsByClassName(
        'productFavoriteIconRemove',
      );

      for (var i = 0; i < productFavoriteIconRemove.length; i++) {
        productFavoriteIconRemove[i].onclick = function () {
          ProductFavoriteRemove(DivIdProduct, settings.ProgramCode);
        };
      }
    }
  }
  // when the user clicks the favorite icon we add this to the database (runs over API) SHARED WITH CUSTOM PRODUCTS
  function ProductFavoriteIconClicked(id_product, ProgramCode, productDetails) {
    console.log('favorite product clicks and preparing db entry');
    console.log(productDetails);

    const url =
      'https://tagweb2339.azurewebsites.net/auth/User/add-favorite-product';

    const body = {
      id_user: id_user, // Assuming id_user is globally available or passed to the function
      id_product: productDetails.ProductId,
      ProgramCode: productDetails.ProgramCode,
      id_client: 2339, // Assuming ClientCode is the id_client and is globally available
      ProductName: productDetails.ProductName,
      ProductDescription: productDetails.ProductDescription,
      ProductImage: productDetails.ProductImage,
      BrandId: 0, // Assuming BrandId might not always be there
      BrandName: productDetails.BrandName,
      BrandLogoImage: productDetails.BrandLogoImage,
      UrlName: productDetails.UrlName,
      Url: productDetails.Url,
      ButtonText: productDetails.ButtonText,
      Price: productDetails.Price,
    };

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to add favorite product');
        }
        return response.json();
      })
      .then(data => {
        console.log('Product added to favorites:', data);

        const Product = 'Product' + id_product;
        const removeProduct = 'RemoveProduct' + id_product;

        document.getElementById(Product).style.display = 'none';
        document.getElementById(removeProduct).style.display = 'block';
      })
      .catch(error => {
        console.error('Error adding product to favorites:', error);
      });
  }

  // when the user clicks the favorite icon in the product details (runs over API to update database) SHARED WITH CUSTOM PRODUCTS
  function ProductFavoriteRemove(id_product, ProgramCode) {
    const url =
      'https://tagweb2339.azurewebsites.net/auth/User/delete-favorite-product';

    const body = {
      id_user: id_user, // Assuming id_user is globally available or passed to the function
      id_product: id_product,
    };

    fetch(url, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to remove favorite product');
        }
        return response.json();
      })
      .then(data => {
        console.log('Product removed from favorites:', data);

        const Product = 'Product' + id_product;
        const removeProduct = 'RemoveProduct' + id_product;

        document.getElementById(removeProduct).style.display = 'none';
        document.getElementById(Product).style.display = 'block';
      })
      .catch(error => {
        console.error('Error removing product from favorites:', error);
      });
  }

  // check if product tags exist for this time segment
  function ProductTags(currentTime, settings) {
    console.log('product tags visible');

    productTags = false;

    if (studio == true) {
      url =
        'https://tagapi.azurewebsites.net/api/Prepare/Products/' +
        settings.ProgramCode +
        '/' +
        currentTime;
    } else {
      url =
        'https://' +
        ClientCode +
        '.azurewebsites.net/api/Products/' +
        settings.ProgramCode +
        '/' +
        currentTime +
        '/' +
        id_user;
    }

    console.log('url :>>', url);

    var xhrProducts = new XMLHttpRequest();
    xhrProducts.open('GET', url, true);
    xhrProducts.send();
    xhrProducts.onreadystatechange = processRequest;

    function processRequest(e) {
      if (xhrProducts.readyState == 4 && xhrProducts.status == 200) {
        var Products = JSON.parse(xhrProducts.responseText);

        console.log(Products);

        // if products are found we continue with the loading of the actual product tags on the screen

        if (Products.length != 0) {
          ShowProductTags(Products, settings, currentTime);

          productTags = true;
        }
      }
    }
  }
  // show the product tags on the screen if they exist
  function ShowProductTags(Products, settings, currentTime) {
    // if the minimize function is activated we show the number of tags available in the norification
    // area, the yellow circle
    if (settings.menuMinimize == 1) {
      document.getElementById('minimizeProductNotification').style.display =
        'block';

      // we show the number of tags that are present
      document.getElementById('minimizeProductNotification').innerHTML =
        Products.length;

      // we update the text in the button
      document.getElementById('minimizeText').innerHTML = 'Products available';
    }

    // // minimize the menu option
    // buttonContainerMinimize("tag", settings);

    ProductDetailsData = Products;

    // clear any old data that might be present
    document.getElementById('ProductTag').innerHTML = '';

    var dynamic = '';

    var tagImage = '';

    var FavoriteProduct = '';

    var qrCode = '';

    var productPrice = '';

    for (i = 0; i < Products.length; i++) {
      // if smart tv is activated we replace the button link option with a QR code
      if (androidTV == true) {
        // add the product image
        tagImage =
          '<img class="ProductTagImage" src="https://' +
          ClientCode +
          '.showtagenabled.com/images/' +
          Products[i].ProductImage +
          '">';

        // adds the qr code
        qrCode =
          '<div class="qrCode" id="QRCodeTag' +
          Products[i].ProductId +
          '"></div>';

        // removes the link
        tagButton = '';
      }

      // show the button that links to a website, this applied also to mobile
      if (web == true) {
        if (settings.deactivateProgramDetails == 1) {
          // add the product image
          tagImage =
            '<img class="ProductTagImage" src="https://' +
            ClientCode +
            '.showtagenabled.com/images/' +
            Products[i].ProductImage +
            '">';

          // add the button
          tagButton =
            '<div class="ProductTagButton"><a style="text-decoration: none; pointer-events:initial;" href="' +
            Products[i].Url +
            '" target="_blank" >' +
            Products[i].ButtonText +
            '<span> ' +
            Products[i].UrlName +
            '</span></a></div>';
        } else {
          tagButton = '';
        }
      }

      // if the favorites are activated in the settings section we show the favorites button
      if (settings.favoriteButtonsActivated == 1) {
        console.log('favorite products activated');

        FavoriteProduct =
          '' +
          '<div class="addToFavoriteButton" id="Product' +
          Products[i].ProductId +
          '" style="position: absolute; pointer-events:initial;"></div>' +
          '<div class="addToFavoriteButtonRemove" id="RemoveProduct' +
          Products[i].ProductId +
          '" style="position: absolute; display: none; pointer-events:initial;"></div>';
      }

      // if there is a value in the price (float) we show the price section
      if (Products[i].Price != null) {
        productPrice =
          '<div class="ProductPrice">' + Products[i].Price + '</div>';
      }

      // divide the screen in two, where 50% places the information of the tag on the left of right side

      if (Products[i].x < 50) {
        // check if there a brand connected for this tag
        if (Products[i].BrandName != 'NULL') {
          // check if the user want to show the logo image in the product tag
          if (settings.tagShowBrandLogo == 1) {
            // https://tag2081.blob.core.windows.net/images/brandlogo/LEGO_logo-700x700.png

            tagBrandLeft =
              '<img class="ProductTagLeftImage deactivateTagHover" src="https://tag2081.blob.core.windows.net/images/' +
              Products[i].BrandLogoImage +
              '">';
          } else {
            tagBrandLeft =
              '<div class="ProductBrandLeft deactivateTagHover" style="float: left; pointer-events: initial">' +
              Products[i].BrandName +
              '</div>';
          }
        } else {
          tagBrandLeft = '';
        }

        dynamic +=
          '' +
          '<div style = "top:' +
          Products[i].y +
          '%; left:' +
          Products[i].x +
          '%; position:absolute;">' +
          '<div class="ProductTagLeft ProductTag tag" style="pointer-events: initial;" id="' +
          Products[i].ProductId +
          '">' +
          '<div class="ProductTagContainer" id="ptc_' +
          Products[i].ProductId +
          '">' +
          tagImage +
          tagBrandLeft +
          '<div class="ProductTextLeft deactivateTagHover tagText">' +
          Products[i].ProductName +
          tagButton +
          '</div>' +
          productPrice +
          FavoriteProduct +
          qrCode +
          '</div>' +
          '</div>' +
          '</div>';
      } else {
        // product tag close to the right edge of the screen

        // check the brandname
        if (Products[i].BrandName != 'NULL') {
          // check if the user want to show the logo image in the product tag
          if (settings.tagShowBrandLogo == 1) {
            // https://tag2081.blob.core.windows.net/images/brandlogo/LEGO_logo-700x700.png

            tagBrandRight =
              '<img class="ProductTagRightImage deactivateTagHover" src="https://tag2081.blob.core.windows.net/images/' +
              Products[i].BrandLogoImage +
              '">';
          } else {
            tagBrandRight =
              '<div class="ProductBrandRight deactivateTagHover" style="pointer-events: initial; position: absolute">' +
              Products[i].BrandName +
              '</div>';
          }
        } else {
          tagBrandRight = '';
        }

        dynamic +=
          '' +
          '<div style = "top:' +
          Products[i].y +
          '%; left:' +
          Products[i].x +
          '%; position:absolute;">' +
          '<div class="ProductTagRight ProductTag tag" style="pointer-events: initial;" id="' +
          Products[i].ProductId +
          '">' +
          '<div class="ProductTagContainer" id="ptc_' +
          Products[i].ProductId +
          '">' +
          tagImage +
          tagBrandRight +
          '<div class="ProductTextRight deactivateTagHover tagText">' +
          Products[i].ProductName +
          tagButton +
          '</div>' +
          productPrice +
          FavoriteProduct +
          qrCode +
          '</div>' +
          '</div>' +
          '</div>';
      }
    }

    ProductTagsContent = document.createElement('div');

    ProductTagsContent.id = 'ProductsTagContainer';

    ProductTagsContent.innerHTML = dynamic;

    document.getElementById('ProductTag').appendChild(ProductTagsContent);

    if (androidTV == true) {
      // this section only for TV app
      // we need to wait for the div's to be present in the dom before we can append
      for (i = 0; i < Products.length; i++) {
        // if an URL is present we show the QR code, else we igonore it
        if (Products[i].Url != null) {
          // create the QR code and connect it to the unique id
          // EWALD: we need to change the address to: clientcode + showtagenabled.com/ProductDetailsData[i].id_product
          let str = 'example-string';
          let newPrC = ProgramCode.split('-')[0];

          // Encrypt

          function encryptNumber(num) {
            var encrypted = btoa(num);

            return encrypted;
          }

          var string =
            '?p' + Products[i].ProductId + '&u=' + id_user + '&v=' + newPrC;

          var encrypt = encryptNumber(string);

          // ewaldheretag
          // where we need to capture the product id, find the URL and then redirect the user to the website
          new QRCode(
            document.getElementById('QRCodeTag' + Products[i].ProductId),
            'http://development.showtagenabled.com/' + encrypt,
          );
        }
      }

      var ProductTag = document.getElementsByClassName('ProductTag');

      var ProductTagContainer = document.getElementsByClassName(
        'ProductTagContainer',
      );

      var CelebrityCard = document.getElementsByClassName('CelebrityCard');

      for (var i = 0; i < ProductTag.length; i++) {
        // click on a tag and show
        ProductTag[i].onclick = function () {
          // hide all celebrity tag containers
          for (var i = 0; i < CelebrityCard.length; i++) {
            CelebrityCard[i].style.display = 'none';
          }

          // hide all product tag containers
          for (var i = 0; i < ProductTag.length; i++) {
            ProductTagContainer[i].style.display = 'none';
          }

          var ProductTagContainerId = 'ptc_' + this.id;

          document.getElementById(ProductTagContainerId).style.display =
            'block';
        };
      }
    }

    if (web == true && settings.deactivateProgramDetails == 1) {
      var ProductTag = document.getElementsByClassName('ProductTag');

      var ProductTagContainer = document.getElementsByClassName(
        'ProductTagContainer',
      );

      var CelebrityCard = document.getElementsByClassName('CelebrityCard');

      for (var i = 0; i < ProductTag.length; i++) {
        // click on a tag and show
        ProductTag[i].onclick = function () {
          // hide all celebrity tag containers
          for (var i = 0; i < CelebrityCard.length; i++) {
            CelebrityCard[i].style.display = 'none';
          }

          // hide all product tag containers
          for (var i = 0; i < ProductTag.length; i++) {
            ProductTagContainer[i].style.display = 'none';
          }

          var ProductTagContainerId = 'ptc_' + this.id;

          document.getElementById(ProductTagContainerId).style.display =
            'block';
        };
      }
    }

    // show the product tag layer
    document.getElementById('ProductTag').style.display = 'block';

    // turn of the tag hover from code and make the brand name or image with
    // the product name directly visible on the screen
    if (
      settings.deactivateTagHoover == 1 ||
      settings.deactivateProgramDetails == 1
    ) {
      var DeactivateHover =
        document.getElementsByClassName('deactivateTagHover');

      for (var i = 0; i < DeactivateHover.length; i++) {
        DeactivateHover[i].style.display = 'block';
      }
    }

    // get the click on the tag but deactivate when ProgramDetails are deactivated
    if (
      settings.deactivateProgramDetails != 1 &&
      settings.deactivateProgramDetails != 1
    ) {
      var ProductTag = document.getElementsByClassName('ProductTag');

      for (var i = 0; i < ProductTag.length; i++) {
        ProductTag[i].onclick = function () {
          ProductClicked(this, settings, currentTime);
        };
      }
    }

    // when favorites are activated we also activate the click function on the button
    if (settings.favoriteButtonsActivated == 1) {
      console.log('favoriteClick button activated');

      const favoriteProducts = fetchFavoriteProducts(id_user);

      // CLICK THE PRODUCT FAVORITE ICON (ADD)
      var ProductFavoriteIcon = document.getElementsByClassName(
        'addToFavoriteButton',
      );
      var ProductFavoriteIconRemove = document.getElementsByClassName(
        'addToFavoriteButtonRemove',
      );

      if (settings.favoriteButtonsActivated == 1) {
        console.log('favoriteClick button activated');

        // Fetch the favorite products
        fetchFavoriteProducts(id_user)
          .then(favoriteProducts => {
            const ProductFavoriteIcon = document.getElementsByClassName(
              'addToFavoriteButton',
            );
            const ProductFavoriteIconRemove = document.getElementsByClassName(
              'addToFavoriteButtonRemove',
            );

            for (let i = 0; i < ProductFavoriteIcon.length; i++) {
              let productId = Products[i].ProductId; // Capture the current product's ID

              // Set initial state based on whether the product is a favorite
              if (isProductFavorite({ProductId: productId}, favoriteProducts)) {
                ProductFavoriteIcon[i].style.display = 'none';
                ProductFavoriteIconRemove[i].style.display = 'block';
              } else {
                ProductFavoriteIcon[i].style.display = 'block';
                ProductFavoriteIconRemove[i].style.display = 'none';
              }

              // Set up click handlers
              ProductFavoriteIcon[i].onclick = function () {
                ProductFavoriteIconClicked(productId, ProgramCode, Products[i]);
                this.style.display = 'none';
                ProductFavoriteIconRemove[i].style.display = 'block';
              };

              ProductFavoriteIconRemove[i].onclick = function () {
                ProductFavoriteRemove(productId, ProgramCode);
                this.style.display = 'none';
                ProductFavoriteIcon[i].style.display = 'block';
              };
            }
          })
          .catch(error => {
            console.error('Error fetching favorite products:', error);
          });
      }
    }
  }

  async function fetchFavoriteProducts(id_user) {
    const response = await fetch(
      'https://tagweb2339.azurewebsites.net/auth/User/get-favorite-product',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({id_user}),
      },
    );

    if (!response.ok) {
      throw new Error('Failed to fetch favorite products');
    }

    const data = await response.json();
    return data.products || []; // Return the products array or an empty array if it doesn't exist
  }

  function isProductFavorite(product, favoriteProducts) {
    return favoriteProducts.some(
      favProduct => favProduct.id_product === product.ProductId,
    );
  }

  // CUSTOM PRODUCTS
  // when the viewer clicks the custom product button (second group) we show all products in scroll
  function GetAllCustomProducts(settings, ProgramCode) {
    var livelayer = false;

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (livelayer == false) {
      HideAll(settings);

      HideAllActive(settings);
    }

    if (ProductDataCustom.length == 0) {
      var ButtonCustomProductLoader = document.getElementById(
        'ButtonCustomProductLoader',
      ).style;
      ButtonCustomProductLoader.opacity = 1;
      (function fade() {
        (ButtonCustomProductLoader.opacity += 0.1) > 0
          ? (ButtonCustomProductLoader.display = 'block')
          : setTimeout(fade, 40);
      })();

      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/customProductsInProgram/';
      } else {
        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Menu/CustomProductsInProgram/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id;
      }

      var xhr = new XMLHttpRequest();
      xhr.open('GET', url + ProgramCode, true); // PUBLISH VERSION ADD: ID_USER
      xhr.send();

      xhr.onreadystatechange = processRequest;

      function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
          ProductDataCustom = JSON.parse(xhr.responseText);

          CustomProductListing(settings, ProductDataCustom);
        }
      }
    } else {
      CustomProductListing(settings, ProductDataCustom);
    }
  }
  // create the scroll listing for the menu
  function CustomProductListing(settings, ProductDataCustom) {
    document.getElementById('Product').innerHTML = '';

    var dynamic = '';

    var shoppingList = '';

    var FavoriteProduct = '';

    var qrCode = '';

    var productPrice = '';

    var externalLinkButton = '';

    if (settings.CustomShoppingList == 1) {
      shoppingList =
        '<div class="MenuShoppingListButton" style="pointer-events: initial; position: absolute;user-select: none;"><a style="text-decoration: none;" href="' +
        customTopButtonLink +
        '" target="_blank">' +
        customTopButtonLinkText +
        '</a></div>';
    }

    for (i = 0; i < ProductDataCustom.length; i++) {
      // if the favorites are activated in the settings section we show the favorites button
      if (settings.favoriteButtonsActivated == 1) {
        FavoriteProduct =
          '' +
          '<div class="addToFavoriteButton" id="Product' +
          ProductDataCustom[i].id_product +
          '" style="position: absolute; pointer-events:initial;"></div>' +
          '<div class="addToFavoriteButtonRemove" id="RemoveProduct' +
          ProductDataCustom[i].id_product +
          '" style="position: absolute; display: none; pointer-events:initial;"></div>';
      }

      if (androidTV == true) {
        qrCode =
          '<div class="qrCode" id="qrCodeCustomProduct' +
          ProductDataCustom[i].id_product +
          '"></div>';
      }

      // if there is a value in the price (float) we show the price section
      if (ProductDataCustom[i].Price != null) {
        productPrice =
          '<div class="ProductPrice">' + ProductDataCustom[i].Price + '</div>';
      }

      // if the product details are deactivated we show the buy now button the in the product listing
      if (settings.deactivateProgramDetails == 1) {
        externalLinkButton =
          '<a class="MenuCellButton" href="' +
          ProductDataCustom[i].Url +
          '" target="_blank">' +
          ProductDataCustom[i].ButtonText +
          '</a>';
      }

      dynamic +=
        '<div id="customproduct_' +
        ProductDataCustom[i].id_product +
        '" class="MenuCellContainerCustomProducts">' +
        '<div class="MenuCellBackground">' +
        '<img src="https://' +
        ClientCode +
        '.showtagenabled.com/images/' +
        ProductDataCustom[i].ProductImage +
        '" />' +
        '<div class="MenuCellName" style="position:absolute; user-select:none;"><span class="MenuCellBrandName">' +
        ProductDataCustom[i].BrandName +
        ' </span>' +
        ProductDataCustom[i].ProductName +
        '</div>' +
        productPrice +
        FavoriteProduct +
        qrCode +
        externalLinkButton +
        '</div>' +
        '</div>';
    }

    var CustomProductContainer = document.createElement('div');
    CustomProductContainer.innerHTML =
      '' +
      '<div class="MenuContainer" id="CustomProductMenuContainerID" style="pointer-events: initial; position:absolute">' +
      '<div class="MenuTitle">' +
      settings.customTopButtonText +
      '</div>' +
      '<div class="MenuContainerScroll" style=" pointer-events: initial; position:relative">' +
      dynamic +
      '</div>' +
      shoppingList +
      '</div>';

    document.getElementById('Product').appendChild(CustomProductContainer);

    if (androidTV == true) {
      // this section only for TV app
      // we need to wait for the div's to be present in the dom before we can append
      for (i = 0; i < ProductDataCustom.length; i++) {
        // if an URL is present we show the QR code, else we igonore it
        if (ProductDataCustom[i].Url != null) {
          // create the QR code and connect it to the unique id
          // EWALD: we need to change the address to: clientcode + showtagenabled.com/ProductDetailsData[i].id_product
          // where we need to capture the product id, find the URL and then redirect the user to the website
          new QRCode(
            document.getElementById(
              'qrCodeCustomProduct' + ProductDataCustom[i].id_product,
            ),
            'http://showtagenabled.com/' + ProductDataCustom[i].id_product,
          );
        }
      }
    }

    // show the custom product listing on the screen
    document.getElementById('Product').style.display = 'block';

    // hide the loader
    document.getElementById('ButtonCustomProductLoader').style.display = 'none';

    if (
      settings.customStyleSheet != 1 &&
      settings.ProgramInformationActive == 1 &&
      settings.topBarVisible == 1
    ) {
      document.getElementById('CustomProductMenuContainerID').style.left =
        'unset';

      document.getElementById('CustomProductMenuContainerID').style.right =
        '1.5vw';
    }

    // if in the setting we deactivate the program details (we deactivate the details for celebrity and products and use direct cards)
    // we deactivate the click on the product
    if (settings.deactivateProgramDetails != 1) {
      var TopContainerCelCustomProducts = document.getElementsByClassName(
        'MenuCellContainerCustomProducts',
      );

      for (var i = 0; i < TopContainerCelCustomProducts.length; i++) {
        TopContainerCelCustomProducts[i].onclick = function () {
          CustomProductClicked(this, settings, 0, ProductDataCustom);
        };
      }
    }
  }
  // when the viewer clicks a product tag or a product in the custom product scroll (menu)
  function CustomProductClicked(
    ClickedCustomProduct,
    settings,
    currentTime,
    ProductDataCustom,
  ) {
    HideAll(settings);

    HideAllActive(settings);

    var url;
    var ProductDetails;
    var DivIdProduct = ClickedCustomProduct.id;

    DivIdProduct = ClickedCustomProduct.id.substring(
      ClickedCustomProduct.id.indexOf('_') + 1,
    );

    var sourceTag = false;

    var ProductDetails = ProductDataCustom.find(
      x => x.id_product == DivIdProduct,
    );

    // SEND TO ANALYTICS
    AnalyticsProductDetailsView(DivIdProduct, settings, currentTime);

    document.getElementById('ProductDetails').innerHTML = '';

    // if the viewer clicked a product tag
    if (sourceTag == true) {
      if (ProductDetails.X < 50) {
        // tag on the left side of the screen

        var productVP =
          '<div class="ProductDetailsRight" id="ProductDetailsID" style="position: absolute; pointer-events: none; user-select: none; z-indez:100;">' +
          '<div id="ProductDetailsClose" class="ProductDetailsCloseRight" style="pointer-events:initial;"></div>';
      } else {
        // tag on the right side of the screen

        var productVP =
          '<div class="ProductDetailsLeft" id="ProductDetailsID" style="position: absolute; pointer-events: none; user-select: none;z-indez:100;">' +
          '<div id="ProductDetailsClose" class="ProductDetailsCloseLeft" style="pointer-events:initial;"></div>';
      }
    } else {
      // user clicked a product in the menu

      var productVP =
        '<div class="ProductDetails" id="ProductDetailsID" style="position: absolute; pointer-events: none; user-select: none;z-indez:100;">' +
        '<div id="ProductDetailsClose" class="ProductDetailsClose" style="pointer-events:initial;"></div>';
    }

    if (settings.favoriteButtonsActivated == 1) {
      FavoriteProduct =
        '' +
        '<div class="productFavoriteIcon" id="Product' +
        DivIdProduct +
        '" style="position: absolute; style="pointer-events:initial;"></div>' +
        '<div class="productFavoriteIconRemove" id="RemoveProduct' +
        DivIdProduct +
        '" style="position: absolute; display: none; style="pointer-events:initial;"></div>';
    } else {
      FavoriteProduct = '';
    }

    if (ProductDetails.BrandName != null || ProductDetails.BrandName != '') {
      brandName = ProductDetails.BrandName;
    } else {
      brandName = '';
    }

    if (ProductDetails.UrlName != null && ProductDetails.ButtonText != null) {
      externallinkContainer =
        '' +
        '<div class="productExternalLinkContainer" style="position:absolute; pointer-events:initial;">' +
        '<div class="productexternallink"><a style="text-decoration: none;" href="' +
        ProductDetails.Url +
        '" target="_blank" class="" title="">' +
        ProductDetails.ButtonText +
        '<br /><span>' +
        ProductDetails.UrlName +
        '</span></a></div>' +
        '</div>';
    } else {
      externallinkContainer = '';
    }

    var PDDynamic = document.createElement('div');
    PDDynamic.innerHTML =
      '' +
      '<div id="CustomProductDetaildID" class="ProductDetails"  style="position: absolute; pointer-events: initial; user-select: none;">' +
      '<div class="ProductName" style="position: absolute;">' +
      brandName +
      ' ' +
      ProductDetails.ProductName +
      '</div>' +
      '<div class="ProductImageBackground" style="position: absolute;"><img class="ProductImage" src="https://' +
      ClientCode +
      '.showtagenabled.com/images/' +
      ProductDetails.ProductImage +
      '"/>' +
      FavoriteProduct +
      '</div > ' +
      '<div class="ProductExtra" style="position: absolute;"></div>' +
      '<div class="productdescription" style="position:absolute;">' +
      ProductDetails.ProductDescription +
      '</div>' +
      externallinkContainer +
      '</div>';

    document.getElementById('ProductDetails').appendChild(PDDynamic);

    document.getElementById('ProductDetails').style.display = 'block';

    // SAVE CLICK TO ANALYTICS
    var productAnalyticsClickExternalLink = document.getElementsByClassName(
      'productexternallink',
    );

    for (var i = 0; i < productAnalyticsClickExternalLink.length; i++) {
      productAnalyticsClickExternalLink[i].onclick = function () {
        AnalyticsProductExternalLink(DivIdProduct, settings);
      };
    }

    if (
      settings.customStyleSheet != 1 &&
      settings.ProgramInformationActive == 1
    ) {
      document.getElementById('CustomProductDetaildID').style.left = 'unset';

      document.getElementById('CustomProductDetaildID').style.right = '1.5vw';
    }

    if (settings.favoriteButtonsActivated == 1) {
      // CLICK THE PRODUCT FAVORITE ICON (ADD)

      var productFavoriteIcon = document.getElementsByClassName(
        'productFavoriteIcon',
      );

      for (var i = 0; i < productFavoriteIcon.length; i++) {
        productFavoriteIcon[i].onclick = function () {
          ProductFavoriteIconClicked(DivIdProduct, settings.ProgramCode);
        };
      }

      // CLICK THE PRODUCT FAVORITE ICON (REMOVE)

      var productFavoriteIconRemove = document.getElementsByClassName(
        'productFavoriteIconRemove',
      );

      for (var i = 0; i < productFavoriteIconRemove.length; i++) {
        productFavoriteIconRemove[i].onclick = function () {
          ProductFavoriteRemove(DivIdProduct, settings.ProgramCode);
        };
      }
    }
  }

  // MUSIC
  // when the viewer clicks the music button in the menu we show all music of the video in a scroll
  function GetAllMusic(settings, ProgramCode) {
    var livelayer = false;

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (livelayer == false) {
      HideAll(settings);

      HideAllActive(settings);
    }

    if (MusicData.length == 0) {
      document.getElementById('ButtonMusicLoader').style.display = 'block';

      if (studio == true) {
        url =
          'https://tagapi.azurewebsites.net/api/Prepare/musicinprogram/' +
          ProgramCode;
      } else {
        url =
          'https://' +
          ClientCode +
          '.azurewebsites.net/api/Menu/musicinprogram/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id;
      }

      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true); // PUBLISH VERSION ADD: ID_USER
      xhr.send();

      xhr.onreadystatechange = processRequest;

      function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
          var MusicData = JSON.parse(xhr.responseText);

          document.getElementById('Music').innerHTML = '';

          var dynamic = '';

          var qrCode = '';

          var FavoriteMusic = '';

          for (i = 0; i < MusicData.length; i++) {
            if (androidTV == true) {
              qrCode =
                '<div class="qrCode" id="qrCodeMusic' +
                MusicData[i].id +
                '"></div>';
            } else {
              qrCode =
                '' +
                '<div class="MenuCellMusicUrlContainer">' +
                '<a href="' +
                MusicData[i].url +
                '" target="_blank" style="text-decoration: none;" class="analyticsMusicClick" id="' +
                MusicData[i].id +
                '">' +
                '<div class="MenuCellMusicUrlName">' +
                MusicData[i].url_text +
                '</div>' +
                '<div class="MenuCellMusicUrl">' +
                MusicData[i].url_name +
                '</div>' +
                '</div>' +
                '</a>' +
                '</div>';
            }

            // if the favorites are activated in the settings section we show the favorites button
            if (settings.favoriteButtonsActivated == 1) {
              FavoriteMusic =
                '' +
                '<div class="addToFavoriteButton" id="Music_' +
                MusicData[i].id +
                '" style="position: absolute; pointer-events:initial;"></div>' +
                '<div class="addToFavoriteButtonRemove" id="RemoveProduct' +
                MusicData[i].id +
                '" style="position: absolute; display: none; pointer-events:initial;"></div>';
            }

            dynamic +=
              '<div id="Music_' +
              MusicData[i].id +
              '" class="MenuCellContainerMusic">' +
              '<div class="MenuCellBackground">' +
              '<img src="https://' +
              ClientCode +
              '.showtagenabled.com/images/music/' +
              MusicData[i].image +
              '" />' +
              '<div class="MenuCellName" style="position:absolute; user-select:none;">' +
              MusicData[i].artist +
              '<div class="MenuCellSong">' +
              MusicData[i].song +
              '</div>' +
              '</div>' +
              FavoriteMusic +
              qrCode +
              '</div>' +
              '</div>';
          }

          var MDynamic = document.createElement('div');
          MDynamic.innerHTML =
            '' +
            '<div class="MenuContainer" id="MusicMenuContainer" style="pointer-events: initial; position:absolute">' +
            '<div class="MenuTitle">' +
            settings.musicTopButtonText +
            '</div>' +
            '<div class="MenuContainerScroll" style=" pointer-events: initial; position:relative" id="MusicContainerScroll">' +
            dynamic +
            '<div id="player_id"></div>' +
            '</div>' +
            '</div>';

          document.getElementById('Music').appendChild(MDynamic);

          if (androidTV == true) {
            // this section only for TV app
            // we need to wait for the div's to be present in the dom before we can append
            for (i = 0; i < MusicData.length; i++) {
              // if an URL is present we show the QR code, else we igonore it
              if (MusicData[i].url != null) {
                // create the QR code and connect it to the unique id
                // EWALD: we need to change the address to: clientcode + showtagenabled.com/ProductDetailsData[i].id_product
                // where we need to capture the product id, find the URL and then redirect the user to the website
                new QRCode(
                  document.getElementById('qrCodeMusic' + MusicData[i].id),
                  'https://showtagenabled.com/Music/' + MusicData[i].id,
                );
              }
            }
          }

          document.getElementById('Music').style.display = 'block';

          document.getElementById('ButtonMusicLoader').style.display = 'none';

          // ANALYTICS

          var TopContainerCelMusic = document.getElementsByClassName(
            'analyticsMusicClick',
          );

          for (var i = 0; i < TopContainerCelMusic.length; i++) {
            TopContainerCelMusic[i].onclick = function () {
              AnalyticsMusicExternalLink(this.id, settings, 'menu');
            };
          }
        }
      }
    } else {
      document.getElementById('Music').style.display = 'block';
    }
  }
  // when user clicks the favorite music icon we store in the database (runs over API) [DEACTIVATED]
  function MusicFavoriteIconClicked(id_music, ProgramCode) {
    var Music = 'Music' + id_music;
    var removeMusic = 'RemoveMusic' + id_music;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open(
      'POST',
      'https://' +
        ClientCode +
        '.azurewebsites.net/api/User/Favorites/AddMusic/' +
        id_music +
        '/' +
        ProgramCode +
        '/' +
        id_user,
    );
    xmlhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xmlhttp.send(JSON.stringify());

    document.getElementById(Music).style.display = 'none';
    document.getElementById(removeMusic).style.display = 'block';
  }
  // when user clicks favorite button we update the setting in database [DEACTIVATED]
  function MusicFavoriteRemove(id_music, ProgramCode) {
    var Music = 'Music' + id_music;
    var removeMusic = 'RemoveMusic' + id_music;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open(
      'DELETE',
      'https://' +
        ClientCode +
        '.azurewebsites.net/api/User/Favorites/DeleteMusic/' +
        id_music +
        '/' +
        id_user,
    );
    xmlhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xmlhttp.send(JSON.stringify());

    document.getElementById(removeMusic).style.display = 'none';
    document.getElementById(Music).style.display = 'block';
  }
  // check if music exist for the time segment
  function MusicInFrame(currentTime, settings) {
    if (studio == true) {
      url =
        'https://tagapi.azurewebsites.net/api/Prepare/Music/' +
        settings.ProgramCode +
        '/' +
        currentTime;
    } else {
      url =
        'https://' +
        ClientCode +
        '.azurewebsites.net/api/Music/' +
        settings.ProgramCode +
        '/' +
        currentTime +
        '/' +
        id_user;
    }

    var xhrMusic = new XMLHttpRequest();
    xhrMusic.open('GET', url, true);
    xhrMusic.send();
    xhrMusic.onreadystatechange = processRequest;

    function processRequest(e) {
      if (xhrMusic.readyState == 4 && xhrMusic.status == 200) {
        Music = JSON.parse(xhrMusic.responseText);

        if (Music.length != 0) {
          ShowMusic(Music, settings, currentTime);
        }
      }
    }
  }
  // show music connected to time segment
  function ShowMusic(Music, settings, currentTime) {
    document.getElementById('Music').innerHTML = '';

    document.getElementById('Music').style.display = 'block';

    MusicContent = document.createElement('div');

    MusicContent.id = 'music_' + Music[0].id;
    MusicContent.className = 'MusicContainer';
    MusicContent.style.top = Music[0].y + '%';
    MusicContent.style.left = Music[0].x + '%';
    MusicContent.style.width = Music[0].w + '%';
    MusicContent.style.height = 'auto';
    MusicContent.style.position = 'absolute';

    MusicContent.innerHTML =
      '' +
      '<img class="musicImage" src="https://' +
      ClientCode +
      '.showtagenabled.com/images/music/' +
      Music[0].image +
      '"/>' +
      '<div class="musicUrlButton" style="pointer-events: initial; user-select: none;" id="click_' +
      Music[0].id +
      '"><a style="text-decoration: none;" href="' +
      Music[0].url +
      '" target="_blank">' +
      '<div class="musicUrlText">' +
      Music[0].song +
      '</div>' +
      '<div class="musicUrlName">' +
      Music[0].artist +
      '</div>' +
      '</a></div>';

    document.getElementById('Music').appendChild(MusicContent);

    // ANALYTICS SECTION

    // SEEN ON SCREEN
    AnalyticsMusicView(settings, currentTime, Music[0].id);

    // CLICK EXTERNAL LINK

    document.getElementById('click_' + Music[0].id).onclick = function () {
      AnalyticsMusicExternalLink(Music[0].id, settings, 'frame');
    };
  }

  // LIVEPOLL
  // when the viewer clicks the livepoll button we show the livepoll on the screen
  function GetLivePoll(settings, ProgramCode) {
    var livelayer = false;

    // set live layer to true if any of these are true
    if (settings.buttonOverLive == 1 || settings.mouseOverLive == 1) {
      livelayer = true;
    }

    if (livelayer == false) {
      HideAll(settings);

      HideAllActive(settings);
    }

    if (studio == true) {
      url =
        'https://tagapi.azurewebsites.net/api/Prepare/livepollinprogram/' +
        settings.ProgramCode;
    } else {
      url =
        'https://' +
        ClientCode +
        '.azurewebsites.net/api/Menu/LivePoll/' +
        settings.ProgramCode +
        '/' +
        id_user +
        '/' +
        client_id;
    }

    var dataresult = [];

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.send();
    xhr.onreadystatechange = processRequest;
    function processRequest(e) {
      if (xhr.readyState == 4 && xhr.status == 200) {
        PollData = JSON.parse(xhr.responseText);
      }

      if (studio != true) {
        loadLivePollResults();
      } else {
        createLivePoll();
      }
    }

    function loadLivePollResults() {
      var xhr2 = new XMLHttpRequest();
      xhr2.open(
        'GET',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api/interactions/LivePoll/result/' +
          settings.ProgramCode,
        true,
      );
      xhr2.send();
      xhr2.onreadystatechange = processRequest;
      function processRequest(e) {
        if (xhr2.readyState == 4 && xhr.status == 200) {
          dataresult = JSON.parse(xhr2.responseText);

          createLivePoll();
        }
      }
    }

    function createLivePoll() {
      document.getElementById('LivePoll').innerHTML = '';

      var dynamic = '';

      if (studio != true) {
        var TotalPollVotes = 0;

        // calculate the total number of votes the set the max
        for (i = 0; i < dataresult.length; i++) {
          TotalPollVotes += dataresult[i].Votes;
        }

        // create each poll item
        for (i = 0; i < PollData.length; i++) {
          var ThisVote = dataresult.find(
            x => x.LivePollItemCode == PollData[i].LivePollItemCode,
          );

          if (!ThisVote) {
            var calculation = 0;
          } else {
            var calculation = (ThisVote.Votes / TotalPollVotes) * 100;

            calculation = calculation.toFixed(0);
          }

          dynamic +=
            '<div id="id-LPitem_' +
            PollData[i].LivePollItemCode +
            '" class="PollItem" style="pointer-events: none;">' +
            '<div class="PollItem-name" style="position:absolute">' +
            PollData[i].LivePollItemName +
            '</div>' +
            '<div class="PollItem-Imagebackground">' +
            '<img class="PollItem-Image" src="https://' +
            ClientCode +
            '.showtagenabled.com/images/' +
            PollData[i].LivePollItemImage +
            '" />' +
            '</div>' +
            '<div class="PollItemBar">' +
            '<div class="PollItemBarResult" style="width:' +
            calculation +
            '%">' +
            '<div class="PollItemBarResultVoteNumber">' +
            calculation +
            ' votes</div>' +
            '<div class="PollItemBarResultnumber">' +
            calculation +
            '%</div>' +
            '</div>' +
            '</div>' +
            '<div class="PollItem-Votebutton" style="position:absolute; pointer-events: initial;">' +
            PollData[i].VoteButtonText +
            '</div>' +
            '</div>';
        }
      } else {
        var TotalPollVotes = 75;

        for (i = 0; i < PollData.length; i++) {
          // REMOVE FOR PUBLISH VERSION

          if (i == 0) {
            number = Math.floor(Math.random() * 100) + 1;

            totalProcessed = number;

            calculation = number;
          }

          if (i > 0 && i < PollData.length - 1) {
            if (PollData.length > 2) {
              calculate = 100 - totalProcessed;

              number = Math.floor(Math.random() * calculate) + 1;

              totalProcessed = totalProcessed + number;

              calculation = number;
            }
          }

          if (i == PollData.length - 1) {
            var lastOne = 100 - totalProcessed;

            calculation = lastOne;
          }

          dynamic +=
            '<div id="id-LPitem_' +
            PollData[i].LivePollItemCode +
            '" class="PollItem" style="pointer-events: none;">' +
            '<div class="PollItem-name" style="position:absolute">' +
            PollData[i].LivePollItemName +
            '</div>' +
            '<div class="PollItem-Imagebackground">' +
            '<img class="PollItem-Image" src="https://' +
            ClientCode +
            '.showtagenabled.com/images/' +
            PollData[i].LivePollItemImage +
            '" />' +
            '</div>' +
            '<div class="PollItemBar">' +
            '<div class="PollItemBarResult" style="width:' +
            calculation +
            '%">' +
            '<div class="PollItemBarResultVoteNumber">' +
            calculation +
            ' votes</div>' +
            '<div class="PollItemBarResultnumber">' +
            calculation +
            '%</div>' +
            '</div>' +
            '</div>' +
            '<div class="PollItem-Votebutton" style="position:absolute; pointer-events: initial;">' +
            PollData[i].VoteButtonText +
            '</div>' +
            '</div>';
        }
      }

      var LPDynamic = document.createElement('div');

      LPDynamic.innerHTML =
        '<div class="PollContainer" style="pointer-events:none; position: absolute; top:0; bottom: 0; left: 0; right:0; overflow:hidden;">' +
        '<div class="Poll-title" style="position:absolute">' +
        PollData[0].LivePollTitle +
        '</div>' +
        '<div class="Poll-LivePollDescription" style="position:absolute">' +
        PollData[0].LivePollDescription +
        '</div>' +
        '<div class="PollItem-Container" style="position:absolute;">' +
        dynamic +
        '</div>' +
        '<div class="PollItemVoteConfirmation" id="VoteConfirm">' +
        '<div class="PollItemVoteConfirmationText">' +
        PollData[0].VoteConfirmText +
        '</div>' +
        '</div>' +
        '</div>';

      document.getElementById('LivePoll').appendChild(LPDynamic);

      document.getElementById('LivePoll').style.display = 'block';

      var PollItemsClick = document.getElementsByClassName('PollItem');

      for (var i = 0; i < PollItemsClick.length; i++) {
        PollItemsClick[i].onclick = function () {
          PollItemClicked(this, settings, PollData);
        };
      }
    }
  }
  // GET THE RESULTS OF THE LIVEPOLL
  function PollItemClicked(ClickedPollItem, settings, PollData) {
    document.getElementById('VoteConfirm').style.display = 'block';

    DivIdProduct = ClickedPollItem.id.split('_').pop();

    var SelectedPollItem = PollData.find(
      x => x.LivePollItemCode == DivIdProduct,
    );

    var allPollItems = document.getElementsByClassName('PollItem');

    for (var i = 0; i < allPollItems.length; i++) {
      allPollItems[i].classList.remove('PollItem-votedcolor');
    }

    document
      .getElementById('id-LPitem_' + SelectedPollItem.LivePollItemCode)
      .classList.add('PollItem-votedcolor');

    if (studio != true) {
      // post the result of the vote the the client database
      // each viewer can only vote once for a livepoll item (possible to vote for another item)
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api/interactions/LivePoll/vote/' +
          SelectedPollItem.LivePollItemCode +
          '/' +
          id_user +
          '/' +
          settings.ProgramCode,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  //FORM
  // check if there is a form for the time segment
  function FormInFrame(currentTime, settings) {
    if (studio == true) {
      url =
        'https://tagapi.azurewebsites.net/api/Prepare/Forms/' +
        settings.ProgramCode +
        '/' +
        currentTime;
    } else {
      url =
        'https://' +
        ClientCode +
        '.azurewebsites.net/api/Form/' +
        settings.ProgramCode +
        '/' +
        currentTime +
        '/' +
        id_user;
    }

    var xhrForm = new XMLHttpRequest();
    xhrForm.open('GET', url, true);
    xhrForm.send();
    xhrForm.onreadystatechange = processRequest;

    function processRequest(e) {
      if (xhrForm.readyState == 4 && xhrForm.status == 200) {
        Forms = JSON.parse(xhrForm.responseText);

        if (Forms.length != 0) {
          ShowForm(Forms, settings, currentTime);
        }
      }
    }
  }
  // if a form exist for the time segment show it on the screen
  function ShowForm(Forms, settings, currentTime) {
    document.getElementById('Form').innerHTML = '';

    document.getElementById('Form').style.display = 'block';

    FormsContent = document.createElement('div');

    FormsContent.id = 'form_' + Forms[0].id;

    // send to analytics
    AnalyticsFormView(settings, currentTime);

    if (Forms[0].image != null) {
      var FormImage =
        '<img class="FormImage" src="https://' +
        ClientCode +
        '.showtagenabled.com/images/form/' +
        Forms[0].image +
        '" style="width: ' +
        Forms[0].image_w +
        '%; height:auto; top: ' +
        Forms[0].image_y +
        '%; left:' +
        Forms[0].image_x +
        '%; position: absolute; pointer-events:none;" />';
    } else {
      var FormImage = '<div></div>';
    }

    FormsContent.innerHTML =
      '<iFrame class="FormIframe" src="' +
      Forms[0].url +
      '" style="pointer-events:initial; width: ' +
      Forms[0].w +
      '%; height: ' +
      Forms[0].h +
      '%; top: ' +
      Forms[0].y +
      '%; left:' +
      Forms[0].x +
      '%; position: absolute;">' +
      '</iFrame>' +
      FormImage;

    document.getElementById('Form').appendChild(FormsContent);
  }

  //BANNER
  // check if there are banners for the time segment
  function BannersInFrame(currentTime, settings) {
    document.getElementById('Banner').style.display = 'block';

    if (studio == true) {
      url =
        'https://tagapi.azurewebsites.net/api/Prepare/Banners/' +
        settings.ProgramCode +
        '/' +
        currentTime;
    } else {
      url =
        'https://' +
        ClientCode +
        '.azurewebsites.net/api/Banners/' +
        settings.ProgramCode +
        '/' +
        currentTime +
        '/' +
        id_user;
    }

    var xhrBanner = new XMLHttpRequest();
    xhrBanner.open('GET', url, true);
    xhrBanner.send();

    xhrBanner.onreadystatechange = processRequest;

    function processRequest(e) {
      if (xhrBanner.readyState == 4 && xhrBanner.status == 200) {
        var Banner = JSON.parse(xhrBanner.responseText);

        if (Banner.length != 0) {
          BannerImage(Banner, currentTime, settings);

          BannerVideoTransparent(Banner, currentTime, settings);

          BannerVideo(Banner, currentTime, settings);
        }
      }
    }
  }
  // show banner image on the screen
  function BannerImage(data, currentTime, settings) {
    document.getElementById('BannerImage').innerHTML = '';

    // loop over all the banners
    for (i = 0; i < data.length; i++) {
      // check if the banner is an image
      if (data[i].BannerType == 0) {
        AnalyticsBannerView(data[i].id_banner, settings, currentTime);

        BIDynamic = document.createElement('div');

        BIDynamic = document.createElement('div');

        BIDynamic.innerHTML =
          '<div id="banner_' +
          data[i].id_banner +
          '" style="width: ' +
          data[i].Width +
          '%; height:auto; top: ' +
          data[i].BannerY +
          '%; left:' +
          data[i].BannerX +
          '%; position: absolute; pointer-events: initial; cursor:default;">' +
          '<img src="https://' +
          ClientCode +
          '.showtagenabled.com/images/banner/' +
          data[i].URL +
          '" style="width: 100%; height: auto;" style="pointer-events: initial; cursor:default"/>' +
          '<a class="analyticsBannerClick" id="' +
          data[i].id_banner +
          '" style="width: ' +
          data[i].LinkWidth +
          '%; height: ' +
          data[i].LinkHeight +
          '%; top: ' +
          data[i].LinkY +
          '%; left:' +
          data[i].LinkX +
          '%; position: absolute; pointer-events: initial; cursor:pointer"' +
          'href="' +
          data[i].LinkUrl +
          '" target="_blank"></a>' +
          '</div>';

        document.getElementById('BannerImage').appendChild(BIDynamic);

        document.getElementById('BannerImage').style.display = 'block';

        // ANALYTICS

        var TopContainerCelBanner = document.getElementsByClassName(
          'analyticsBannerClick',
        );

        for (var i = 0; i < TopContainerCelBanner.length; i++) {
          TopContainerCelBanner[i].onclick = function () {
            AnalyticsBannerExternalLink(this.id, settings);
          };
        }
      }
    }
  }
  // show if the banner is a video banner
  function BannerVideo(data, currentTime, settings) {
    document.getElementById('Banner').innerHTML = '';

    for (i = 0; i < data.length; i++) {
      if (data[i].BannerType == 1 && data[i].Transparent == 0) {
        AnalyticsBannerView(data[i].id_banner, settings, currentTime);

        BDynamic = document.createElement('div');
        BDynamic.id = 'banner_' + data[i].id_banner;
        BDynamic.innerHTML =
          '' +
          '<div style = "width: ' +
          data[i].Width +
          '%; top: ' +
          data[i].BannerY +
          '%; left:' +
          data[i].BannerX +
          '%; position: absolute; pointer-events: initial; cursor:default;" > ' +
          '<video autoplay loop crossorigin="anonymous" src="https://' +
          ClientCode +
          '.showtagenabled.com/video/banner/' +
          data[i].URL +
          '" style="width: 100%;" style="pointer-events: initial; cursor:default"></video>' +
          '<a class="analyticsBannerClick" id="' +
          data[i].id_banner +
          '" style="width: ' +
          data[i].LinkWidth +
          '%; height: ' +
          data[i].LinkHeight +
          '%; top: ' +
          data[i].LinkY +
          '%; left:' +
          data[i].LinkX +
          '%; position: absolute; pointer-events: initial; cursor:pointer" href="' +
          data[i].LinkUrl +
          '" target="_blank"></a>' +
          '</div>';

        document.getElementById('Banner').appendChild(BDynamic);

        document.getElementById('Banner').style.display = 'block';

        // ANALYTICS

        var TopContainerCelBanner = document.getElementsByClassName(
          'analyticsBannerClick',
        );

        for (var i = 0; i < TopContainerCelBanner.length; i++) {
          TopContainerCelBanner[i].onclick = function () {
            AnalyticsBannerExternalLink(this.id, settings);
          };
        }
      }
    }
  }
  // show if the banner is a transparent video banner
  function BannerVideoTransparent(data, currentTime, settings) {
    // loop over the banners
    for (i = 0; i < data.length; i++) {
      // check if a banner is a transparent video banner
      if (data[i].BannerType == 1 && data[i].Transparent == 1) {
        AnalyticsBannerView(data[i].id_banner, settings, currentTime);

        drawTransparentBanner(data[i]);
      }
    }

    function drawTransparentBanner(data) {
      document.getElementById('BannerTransparenVideo').innerHTML = '';

      var source = '';

      var loop = '';

      function getVideoDimensionsOf(url) {
        return new Promise(function (resolve) {
          let video = document.createElement('video');

          video.addEventListener(
            'loadedmetadata',
            function () {
              // retrieve dimensions
              let height = this.videoHeight;
              let width = this.videoWidth;

              // send back result
              resolve({
                height: height,
                width: width,
                url: video.src,
              });
            },
            false,
          );

          video.src = url;
        });
      }

      getVideoDimensionsOf(
        'https://' +
          ClientCode +
          '.showtagenabled.com/video/banner/' +
          data.URL,
      ).then(({width, height, url}) => {
        Canvas_height = height / 2;

        buildTransparenVideo(width, Canvas_height, height, url);
      });

      if (data.Repeat == 0) {
        loop = 'loop';
      } else {
        loop = '';
      }

      function buildTransparenVideo(
        Canvas_width,
        Canvas_height,
        Canvas_height_alfa,
        url,
      ) {
        BannerTVideoDynamic = document.createElement('div');

        BannerTVideoDynamic.innerHTML =
          '' +
          '<div style="width: ' +
          data.Width +
          '%; margin-left:' +
          data.BannerX +
          '%; top:' +
          data.BannerY +
          '%; position:absolute; pointer-events:none; z-index:10000">' +
          '<a class="analyticsBannerClick" id="' +
          data.id +
          '" style="width: ' +
          data.LinkWidth +
          '%; height: ' +
          data.LinkHeight +
          '%; top: ' +
          data.LinkY +
          '%; left:' +
          data.LinkX +
          '%; position: absolute; pointer-events: initial; cursor:pointer"' +
          'href="' +
          data.LinkUrl +
          '" target="_blank"></a>' +
          '<video id="Transparentvideo' +
          data.id +
          '" style="display:none;" autoplay ' +
          loop +
          ' crossorigin="anonymous"></video>' +
          '<canvas width="' +
          Canvas_width +
          '" height="' +
          Canvas_height_alfa +
          '" id="buffer' +
          data.id +
          '" style="display: none;"></canvas>' +
          '<canvas width="' +
          Canvas_width +
          '" height="' +
          Canvas_height +
          '" id="output' +
          data.id +
          '" style="display: inline-block; width:100%"></canvas>' +
          '</div>';

        document
          .getElementById('BannerTransparenVideo')
          .appendChild(BannerTVideoDynamic);

        document.getElementById('BannerTransparenVideo').style.display =
          'block';

        source = document.createElement('source');

        source.setAttribute('type', 'video/mp4');

        source.setAttribute('src', url);

        var outputCanvas = document.getElementById('output' + data.id);

        var output = outputCanvas.getContext('2d');

        var bufferCanvas = document.getElementById('buffer' + data.id);

        var buffer = bufferCanvas.getContext('2d');

        var Bannervideo = document.getElementById('Transparentvideo' + data.id);

        Bannervideo.style.width = '100%';

        var width = outputCanvas.width;

        var height = outputCanvas.height;

        var interval;

        function processFrame() {
          buffer.drawImage(Bannervideo, 0, 0);

          var image = buffer.getImageData(0, 0, width, height),
            imageData = image.data,
            alphaData = buffer.getImageData(0, height, width, height).data;

          for (var i = 3, len = imageData.length; i < len; i = i + 4) {
            imageData[i] = alphaData[i - 1];
          }

          output.putImageData(image, 0, 0, 0, 0, width, height);
        }

        Bannervideo.addEventListener(
          'play',
          function () {
            clearInterval(interval);

            interval = setInterval(processFrame, 0);
          },
          false,
        );

        Bannervideo.addEventListener(
          'ended',
          function () {
            if (data.Repeat == 1) {
              // VIDEO LOOP IS OFF

              document.getElementById('BannerTransparenVideo').innerHTML = '';
            }
          },
          false,
        );

        var theID = document.getElementById('Transparentvideo' + data.id);

        theID.appendChild(source);

        // ANALYTICS

        var TopContainerCelBanner = document.getElementsByClassName(
          'analyticsBannerClick',
        );

        for (var i = 0; i < TopContainerCelBanner.length; i++) {
          TopContainerCelBanner[i].onclick = function () {
            AnalyticsBannerExternalLink(this.id, settings);
          };
        }
      }
    }
  }

  // ANALYTICS

  // ANALYTICS CLICK ON ELEMENTS

  // [OK] SAVE THE CLICK ON THE PRODUCT EXTERNAL LINK
  // "api/Analytics/productExternalLink/{ProgramCode}/{id_product}/{id_user}/{id_client}"
  function AnalyticsProductExternalLink(id_product, settings) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/productExternalLink/' +
          settings.ProgramCode +
          '/' +
          id_product +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK} SAVE THE CLICK ON THE BANNER EXTERNAL LINK
  // "api/Analytics/bannerExternalLink/{ProgramCode}/{id_banner}/{id_user}/{id_client}"
  function AnalyticsBannerExternalLink(id_banner, settings) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/bannerExternalLink/' +
          settings.ProgramCode +
          '/' +
          id_banner +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK] SAVE THE CLICK ON THE MUSIC EXTERNAL LINK
  // "api/Analytics/musicExternalLink/{ProgramCode}/{id_music}/{id_user}/{id_client}/{origin}"
  function AnalyticsMusicExternalLink(id_music, settings, origin) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/musicExternalLink/' +
          settings.ProgramCode +
          '/' +
          id_music +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          origin +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK] SAVE THE CLICK ON THE CALL-TO-ACTION BUTTON
  // "api/Analytics/Calltoactionclick/{id_calltoaction}/{ProgramCode}/{Time}/{id_user}/{id_client}"
  function AnalyticsCallToActionLinkclick(
    id_calltoaction,
    settings,
    currentTime,
  ) {
    if (studio != true) {
      // Calltoactionclick/{id_calltoaction}/{ProgramCode}/{Time}/{id_user}/{id_client}/{platform}

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/Calltoactionclick/' +
          id_calltoaction +
          '/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // ANALYTICS VIEW

  // [OK] SAVE THE VIEW FOR CELEBRITY DETAILS (SEEN ON SCREEN)
  // "api/Analytics/celebrityDetails/{id_celebrity}/{ProgramCode}/{id_user}/{id_client}/{time}"
  function AnalyticsCelebrityDetailsView(id_celebrity, settings, currentTime) {
    if (studio != true) {
      //

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/celebrityDetails/' +
          id_celebrity +
          '/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          currentTime +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK} SAVE THE VIEW FOR PRODUCT DETAILS (SEEN ON SCREEN)
  // "api/Analytics/productDetails/{id_product}/{ProgramCode}/{id_user}/{id_client}/{Time}"
  function AnalyticsProductDetailsView(id_product, settings, currentTime) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/productDetails/' +
          id_product +
          '/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          currentTime +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK] SAVE THE VIEW OF A FORM (SEEN ON SCREEN)
  // "api/Analytics/form/{ProgramCode}/{time}/{id_user}/{id_client}"
  function AnalyticsFormView(settings, currentTime) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/form/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK]SAVE THE VIEW OF BANNER (SEEN ON SCREEN)
  // "api/Analytics/banner/{id_banner}/{ProgramCode}/{time}/{id_user}/{id_client}"
  function AnalyticsBannerView(id_banner, settings, currentTime) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/banner/' +
          id_banner +
          '/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK] SAVE THE VIEW OF CALL TO ACTION (SEEN ON SCREEN)
  // "api/Analytics/calltoaction/{ProgramCode}/{time}/{id_user}/{id_client}"
  function AnalyticsCallToActionView(settings, currentTime) {
    if (studio != true) {
      // calltoaction/{ProgramCode}/{time}/{id_user}/{id_client}/{platform}

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/calltoaction/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK] SAVE THE VIEW OF MUSIC (SEEN ON SCREEN) ON TIME SEGMENT
  // "api/Analytics/music/{ProgramCode}/{time}/{id_user}/{id_client}/{id_music}"
  function AnalyticsMusicView(settings, currentTime, id_music) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/music/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          id_music +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // ANALYTICS MENU

  // [OK] SAVE THE CLICK MENU DIRECT ACTION BUTTON
  // "api/Analytics/menuactivebutton/{id_button}/{ProgramCode}/{id_user}/{id_client}"
  function AnalyticsMenuActiveButton(id_button, settings) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/menuactivebutton/' +
          id_button +
          '/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK] SAVE THE CLICK MENU CUSTOM PRODUCT
  // "api/Analytics/customproductlink/{ProgramCode}/{id_user}/{id_client}"
  function AnalyticsMenuCustomProduct(settings) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/customproductlink/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // ANALYTICS VIDEO IN VIDEO INTERACTIONS

  // SAVE IF THE CELEBRITY VIDEO IS WATCHED FOR A TIME SEGMENT
  // "api/Analytics/celebrityVideoWatch/{id_CelebrityVideo}/{ProgramCode}/{Time}/{id_user}"
  function AnalyticsVideoInVideoStart(
    id_CelebrityVideo,
    settings,
    currentTime,
  ) {
    if (studio != true) {
      // celebrityVideoWatch/{id_CelebrityVideo}/{ProgramCode}/{Time}/{id_user}/{platform}/{id_client}

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/celebrityVideoWatch/' +
          id_CelebrityVideo +
          '/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          classtype +
          '/' +
          client_id,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // SAVE IF THE CELEBRITY VIDEO IS CLOSED FOR A TIME SEGMENT
  // "api/Analytics/celebrityVideoClose/{id_CelebrityVideo}/{ProgramCode}/{Time}/{id_user}"
  function AnalyticsVideoClosed(id_CelebrityVideo, settings, currentTime) {
    if (studio != true) {
      // celebrityVideoClose/{id_CelebrityVideo}/{ProgramCode}/{Time}/{id_user}/{platform}/{id_client}

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/celebrityVideoClose/' +
          id_CelebrityVideo +
          '/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          classtype +
          '/' +
          client_id,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // ANALYTICS PLAYER INTERACTIONS

  // [OK] SAVE THE CLICK ON THE PLAY BUTTON
  // "api/Analytics/play/{ProgramCode}/{Time}/{id_user}/{id_client}"
  function AnalyticsPlayClicked(currentTime, settings) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/play/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // [OK] SAVE THE CLICK ON THE PAUSE BUTTON
  // "api/Analytics/pause/{ProgramCode}/{Time}/{id_user}/{id_client}"
  function AnalyticsPauseClicked(currentTime, settings) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/pause/' +
          settings.ProgramCode +
          '/' +
          currentTime +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }

  // SAVE THE CLICK ON THE SKIP BUTTON
  // "api/Analytics/skip/{id_skip}/{ProgramCode}/{id_user}/{id_client}"
  function AnalyticsSkipClicked(id_skip, settings) {
    if (studio != true) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open(
        'POST',
        'https://' +
          ClientCode +
          '.azurewebsites.net/api_v3/Analytics/skip/' +
          id_skip +
          '/' +
          settings.ProgramCode +
          '/' +
          id_user +
          '/' +
          client_id +
          '/' +
          classtype,
      );
      xmlhttp.setRequestHeader(
        'Content-Type',
        'application/json;charset=UTF-8',
      );
      xmlhttp.send(JSON.stringify());
    }
  }
});
