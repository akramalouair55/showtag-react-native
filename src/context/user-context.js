import * as React from 'react';
import {Toast, Dialog} from 'react-native-alert-notification';

export const UserContext = React.createContext();

export const UserProvider = ({children}) => {
  const [avatar, setAvatar] = React.useState(1);
  const [id, setId] = React.useState('70804969-687b-4208-84a3-d68a121f2326');
  const [route, setRoute] = React.useState('Details');
  const [name, setName] = React.useState('');
  const [token, setToken] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [notification, setNotification] = React.useState({});
  const [dialog, setDialog] = React.useState({});

  const updateDialog = dialog => {
    setDialog(dialog);
  };

  const updateNotification = notification => {
    setNotification(notification);
  };

  const updateName = name => {
    setName(name);
  };

  const updateEmail = email => {
    setEmail(email);
  };

  const updateRoute = route => {
    setRoute(route);
  };

  const updateAvatar = avatar => {
    setAvatar(avatar);
  };
  const updateUserId = id => {
    setId(id);
  };

  const updateToken = token => {
    setToken(token);
  };

  React.useEffect(() => {
    if (Object.keys(notification).length > 0) {
      console.log(notification);
      Toast.show({...notification});
      updateNotification({});
    }
  }, [notification]);

  React.useEffect(() => {
    if (Object.keys(dialog).length > 0) {
      console.log(dialog);
      Dialog.show({...dialog});
      updateDialog({});
    }
  }, [dialog]);

  return (
    <UserContext.Provider
      value={{
        avatar,
        updateAvatar,
        id,
        updateUserId,
        route,
        updateRoute,
        name,
        updateName,
        token,
        updateToken,
        email,
        updateEmail,
        notification,
        updateNotification,
        dialog,
        updateDialog,
      }}>
      {children}
    </UserContext.Provider>
  );
};
