import CollectionScreen from './CollectionScreen';
import SeriesScreen from './SeriesScreen';
import TrendingScreen from './TrendingScreen';
import MoviesScreen from './MoviesScreen';

export {CollectionScreen, SeriesScreen, TrendingScreen, MoviesScreen};
