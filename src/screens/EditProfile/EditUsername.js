import React from 'react';
import {View, StyleSheet, ScrollView, Text} from 'react-native';
import colors from '../../utils/colors/colors';
import Header from '../../components/Header/Header';
import {SignupInput} from '../../components/Input';
import {WhiteButton} from '../../components/Buttons';
import {UserContext} from '../../context/user-context';
import {ALERT_TYPE} from 'react-native-alert-notification';
import {API} from '../../utils/colors/consts';

import axios from 'react-native-axios';
import {useNavigation} from '@react-navigation/native';

const EditUsername = () => {
  const userContext = React.useContext(UserContext);

  const [email, setEmail] = React.useState('');
  const [errors, setErrors] = React.useState({});

  const navigation = useNavigation();

  const validateForm = () => {
    const newErrors = {};

    if (!email) newErrors.email = 'ny e-post er nødvendig';
    if (userContext.email == email)
      newErrors.email = 'ny e-post må være annerledes enn den eldre';

    setErrors(newErrors);

    return Object.keys(newErrors).length === 0; // No errors
  };

  const onPressSave = () => {
    const _data = {
      email: email,
    };

    const config = {
      method: 'post',
      url: `${API}auth/change_email_request`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userContext.token}`,
      },

      data: _data,
    };

    axios(config)
      .then(response => {
        if (response.data.status == 200) {
          userContext.updateEmail(email);
          navigation.goBack();
          userContext.updateNotification({
            type: ALERT_TYPE.SUCCESS,
            title: 'Success',
            textBody: response.data.message,
            autoClose: true,
          });
        }
      })
      .catch(err => {
        console.log(err.response);
        userContext.updateNotification({
          type: ALERT_TYPE.DANGER,
          title: 'Error',
          textBody: err.response.data.message,
          autoClose: true,
        });
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: colors.primary}}>
      <Header title="Edit email" />

      <ScrollView contentContainerStyle={styles.container}>
        <Text
          style={{
            fontFamily: 'SFProDisplay-Bold',
            color: colors.white,
            fontSize: 24,
          }}>
          Velg ny e-post
        </Text>

        <SignupInput
          value={email}
          setValue={setEmail}
          floatingText="Ny epost"
          placeholder="Ny epost"
          error={errors.email}
        />
      </ScrollView>
      <WhiteButton title="Lagre" onPress={onPressSave} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingTop: 20,
    flex: 1,
  },
  text: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: colors.white,
    marginTop: 17,
  },
});

export default EditUsername;
