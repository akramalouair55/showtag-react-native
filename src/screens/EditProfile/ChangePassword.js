import React from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import colors from '../../utils/colors/colors';
import Header from '../../components/Header/Header';
import {SignupInput} from '../../components/Input';
import {WhiteButton} from '../../components/Buttons';
import {API} from '../../utils/colors/consts';
import axios from 'react-native-axios';
import {UserContext} from '../../context/user-context';
import {useNavigation} from '@react-navigation/native';
import {saveToken} from '../../utils/colors/utils';
import {ALERT_TYPE, Dialog, Toast} from 'react-native-alert-notification';

const ChangePassword = () => {
  const [oldPassword, setOldPassword] = React.useState('');
  const [newPassword, setNewPassword] = React.useState('');
  const [errors, setErrors] = React.useState({});
  const navigation = useNavigation();

  const userContext = React.useContext(UserContext);

  const validateForm = () => {
    const newErrors = {};

    if (!oldPassword) newErrors.oldPassword = 'Gammelt passord kreves';
    if (!newPassword) newErrors.newPassword = 'Nytt passord kreves';
    if (oldPassword == newPassword)
      newErrors.newPassword = 'Passord må ikke samsvare';

    setErrors(newErrors);

    return Object.keys(newErrors).length === 0; // No errors
  };

  const onPressSave = () => {
    if (validateForm()) {
      const _data = {
        old_password: oldPassword,
        new_password: newPassword,
      };

      const config = {
        method: 'post',
        url: `${API}auth/change_password`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userContext.token}`,
        },
        data: _data,
      };

      axios(config)
        .then(response => {
          if (response.data.status == 200) {
            navigation.goBack();
            userContext.updateNotification({
              type: ALERT_TYPE.SUCCESS,
              title: 'Success',
              textBody: response.data.message,
              autoClose: true,
            });
          }
        })
        .catch(err => {
          userContext.updateNotification({
            type: ALERT_TYPE.DANGER,
            title: 'Error',
            textBody: err.response.data.message,
            autoClose: true,
          });
        });
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: colors.primary}}>
      <Header title="Rediger passord" />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.container}>
        <Text
          style={{
            fontFamily: 'SFProDisplay-Bold',
            color: colors.white,
            fontSize: 24,
          }}>
          Choose new password
        </Text>
        <Text style={styles.text}>
          Velg et sterkt passord, symboler og mellomrom er tillatt
        </Text>
        <SignupInput
          value={oldPassword}
          setValue={setOldPassword}
          floatingText="Gammelt passord"
          placeholder="
          Skriv inn ditt gamle passord"
          error={errors.oldPassword}
          Password
        />
        <SignupInput
          value={newPassword}
          setValue={setNewPassword}
          floatingText="Nytt passord"
          placeholder="Skriv inn ditt nye passord"
          error={errors.newPassword}
          Password
        />
      </ScrollView>
      <WhiteButton title="Lagre" onPress={onPressSave} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingTop: 20,
    flex: 1,
  },
  text: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: colors.white,
    marginTop: 17,
  },
});

export default ChangePassword;
