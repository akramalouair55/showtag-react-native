import React, {useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import colors from '../../utils/colors/colors';
import SignupProgress from '../../components/SignupProgress/SignupProgress';
import {CodeInput} from '../../components/Input';
import {WhiteButton} from '../../components/Buttons';
import {useNavigation} from '@react-navigation/native';
import {API} from '../../utils/colors/consts';
import axios from 'react-native-axios';
import {saveToken} from '../../utils/colors/utils';
import {UserContext} from '../../context/user-context';
import {ALERT_TYPE} from 'react-native-alert-notification';

const ConfirmScreen = ({route}) => {
  const firstRef = useRef(null);
  const secondRef = useRef(null);
  const thirdRef = useRef(null);
  const fourthRef = useRef(null);

  const userContext = React.useContext(UserContext);

  const [firstValue, setFirstValue] = React.useState('');
  const [secondValue, setSecondValue] = React.useState('');
  const [thirdValue, setThirdValue] = React.useState('');
  const [fourthValue, setFourthValue] = React.useState('');

  const navigation = useNavigation();

  const onPressResend = () => {
    const _data = {
      email: route.params.data.email,
    };

    const config = {
      method: 'post',
      url: `${API}auth/resend_confirmation`,
      headers: {'Content-Type': 'application/json'},
      data: _data,
    };

    axios(config)
      .then(response => {
        if (response.data.status == 200) {
          userContext.updateNotification({
            type: ALERT_TYPE.SUCCESS,
            title: 'Success',
            textBody: 'Code has been sent to your email',
            autoClose: true,
          });
        } else {
          userContext.updateNotification({
            type: ALERT_TYPE.DANGER,
            title: 'Error',
            textBody: response.data.message,
            autoClose: true,
          });
        }
      })
      .catch(err => {
        userContext.updateNotification({
          type: ALERT_TYPE.DANGER,
          title: 'Error',
          textBody: err.response.data.message,
          autoClose: true,
        });
      });
  };

  const onPressNext = () => {
    const fullCode = `${firstValue}${secondValue}${thirdValue}${fourthValue}`;

    const _data = {
      email: route.params.data.email,
      code: fullCode,
    };

    const config = {
      method: 'post',
      url: `${API}auth/confirm_code`,
      headers: {'Content-Type': 'application/json'},
      data: _data,
    };

    axios(config)
      .then(response => {
        if (response.data.status == 200) {
          saveToken(response.data.user.access_token);
          userContext.updateToken(response.data.user.access_token);
          userContext.updateName(response.data.user.fullname);
          userContext.updateAvatar(response.data.user.avatar);
          userContext.updateUserId(response.data.user.id);
          userContext.updateEmail(response.data.user.email);
          navigation.navigate('DrawerNavigator', {data: response.data.user});
        }
      })
      .catch(err => {
        console.log(err.response);
        userContext.updateNotification({
          type: ALERT_TYPE.DANGER,
          title: 'Error',
          textBody: err.response.data.message,
          autoClose: true,
        });
      });
  };

  return (
    <View style={styles.main}>
      <View style={{flex: 1}}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          automaticallyAdjustKeyboardInsets>
          <View style={{flex: 0.8}}>
            <View style={{alignItems: 'center', marginBottom: 34}}>
              <SignupProgress progress={3} />
              <Text style={styles.title}>Epostbekreftelse</Text>
              <Text style={styles.text}>
                vi trenger at du bekrefter e-postadressen din. Vi har sendt en
                e-post til e-postadressen din inneholder en 4-sifret kode, som
                utløper om 15 minutter. Vennligst skriv inn koden nedenfor.
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <CodeInput
                value={firstValue}
                setValue={setFirstValue}
                isRef={firstRef}
                nextRef={secondRef}
              />
              <CodeInput
                isRef={secondRef}
                nextRef={thirdRef}
                value={secondValue}
                setValue={setSecondValue}
                previousRef={firstRef}
              />
              <CodeInput
                nextRef={fourthRef}
                isRef={thirdRef}
                value={thirdValue}
                setValue={setThirdValue}
                previousRef={secondRef}
              />
              <CodeInput
                previousRef={thirdRef}
                isRef={fourthRef}
                value={fourthValue}
                setValue={setFourthValue}
              />
            </View>
          </View>
        </ScrollView>
        <View style={{marginBottom: 20, flex: 0.2}}>
          <View style={styles.resendView}>
            <Text style={styles.forget}>Fikk du ikke koden?</Text>
            <TouchableOpacity onPress={onPressResend}>
              <Text style={styles.resend}>Send den på nytt.</Text>
            </TouchableOpacity>
          </View>
          <WhiteButton
            onPress={onPressNext}
            marginHorizontal={0}
            title="Continue"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.primary,
    paddingTop: 40,
    paddingHorizontal: 24,
  },
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 24,
    color: colors.white,
    marginTop: 24,
  },
  text: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    marginTop: 8,
    textAlign: 'center',
    color: colors.white,
  },
  forget: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 13,
    color: colors.white,
  },
  resend: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 13,
    color: 'rgba(66, 178, 155, 1)',
  },
  resendView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 32,
    bottom: 0,
  },
});

export default ConfirmScreen;
