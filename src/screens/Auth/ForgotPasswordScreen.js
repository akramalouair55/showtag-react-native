import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import colors from '../../utils/colors/colors';
import Header from '../../components/Header/Header';
import {SignupInput} from '../../components/Input';
import {WhiteButton} from '../../components/Buttons';
import axios from 'react-native-axios';
import {API} from '../../utils/colors/consts';
import {UserContext} from '../../context/user-context';
import {ALERT_TYPE} from 'react-native-alert-notification';
import {useNavigation} from '@react-navigation/native';

const ForgotPassword = () => {
  const [email, setEmail] = React.useState('');
  const [errors, setErrors] = React.useState({});

  const navigation = useNavigation();

  const userContext = React.useContext(UserContext);

  const isValidEmail = email => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailRegex.test(email);
  };

  const validateForm = () => {
    const newErrors = {};

    if (!email) newErrors.email = 'E-post er påkrevd';
    if (!isValidEmail(email)) newErrors.email = 'Ugyldig e-postformat';

    setErrors(newErrors);

    return Object.keys(newErrors).length === 0; // No errors
  };

  const onPressNext = () => {
    if (validateForm()) {
      const _data = {
        email: email,
      };

      const config = {
        method: 'post',
        url: `${API}auth/reset_password`,
        headers: {'Content-Type': 'application/json'},
        data: _data,
      };

      axios(config)
        .then(response => {
          if (response.data.status == 200) {
            userContext.updateNotification({
              type: ALERT_TYPE.SUCCESS,
              title: 'Success',
              textBody: response.data.message,
              autoClose: true,
            });
            navigation.goBack();
          } else {
            userContext.updateNotification({
              type: ALERT_TYPE.DANGER,
              title: 'Error',
              textBody: response.data.message,
              autoClose: true,
            });
          }
        })
        .catch(err => {
          userContext.updateNotification({
            type: ALERT_TYPE.DANGER,
            title: 'Error',
            textBody: err.response.data.message,
            autoClose: true,
          });
        });
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: colors.primary}}>
      <Header title="Glemt passord" />
      <ScrollView
        contentContainerStyle={{
          flex: 1,
          paddingTop: 17,
          paddingHorizontal: 24,
        }}>
        <SignupInput
          floatingText="E-post"
          placeholder="Skriv inn din email"
          value={email}
          setValue={setEmail}
          error={errors.email}
        />
      </ScrollView>
      <WhiteButton title="Sende" onPress={onPressNext} />
    </View>
  );
};

const styles = StyleSheet.create({});

export default ForgotPassword;
