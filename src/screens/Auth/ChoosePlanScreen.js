import React from 'react';

import {View, Text, Image, StyleSheet} from 'react-native';
import colors from '../../utils/colors/colors';
import SignupProgress from '../../components/SignupProgress/SignupProgress';
import Planduration from '../../components/Planduration/Planduration';
import PlanCard from '../../components/PlanCard/PlanCard';
import {WhiteButton} from '../../components/Buttons';
import {useNavigation} from '@react-navigation/native';

const ChoosePlanScreen = () => {
  const [selection, setSelection] = React.useState(true);
  const [isSelected, setIsSelected] = React.useState(true);

  const navigation = useNavigation();

  const onPressNext = () => {
    navigation.navigate('Signup', {isSelected, selection});
  };

  return (
    <View style={styles.main}>
      <View style={{flex: 0.98}}>
        <View style={{alignItems: 'center', marginBottom: 34}}>
          <SignupProgress />
          <Text style={styles.title}>Choose your plan</Text>
          <Text style={styles.text}>
            omnis iste natus error sit voluptatem accusantium doloremque
            laudantium, totam rem aperiam, eaque ipsa
          </Text>
        </View>
        <Planduration selection={selection} setSelection={setSelection} />
        <PlanCard isSelected={isSelected} setIsSelected={setIsSelected} />
      </View>
      <WhiteButton onPress={onPressNext} title="Next" marginHorizontal={0} />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.primary,
    paddingTop: 40,
    paddingHorizontal: 24,
  },
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 24,
    color: colors.white,
    marginTop: 24,
  },
  text: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    marginTop: 8,
    textAlign: 'center',
    color: colors.white,
  },
});

export default ChoosePlanScreen;
