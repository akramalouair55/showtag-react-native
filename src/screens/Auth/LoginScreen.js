import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import colors from '../../utils/colors/colors';
import Header from '../../components/Header/Header';
import Input from '../../components/Input/Input';
import CheckBox from '../../components/Checkbox/Checkbox';
import {WhiteButton, ThirdPartyButton} from '../../components/Buttons';
import Spacer from '../../components/Spacer/Spacer';
import {useNavigation} from '@react-navigation/native';
import {saveToken} from '../../utils/colors/utils';
import {UserContext} from '../../context/user-context';
import {API} from '../../utils/colors/consts';
import {ALERT_TYPE} from 'react-native-alert-notification';

import axios from 'react-native-axios';

const LoginScreen = () => {
  const [isChecked, setIsChecked] = React.useState(false);
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  const userContext = React.useContext(UserContext);

  const navigation = useNavigation();

  const onPressForgotPassword = () => {
    navigation.navigate('ForgotPassword');
  };

  const onLogin = () => {
    navigation.navigate('DrawerNavigator');
    // const _data = {
    //   email: email,
    //   password: password,
    //   remember_me: isChecked,
    // };

    // const config = {
    //   method: 'post',
    //   url: `${API}auth/login`,
    //   headers: {'Content-Type': 'application/json'},
    //   data: _data,
    // };

    // axios(config)
    //   .then(response => {
    //     if (response.data.status == 200) {
    //       if (isChecked) {
    //         saveToken(response.data.user.access_token);
    //       }
    //       userContext.updateToken(response.data.user.access_token);
    //       userContext.updateName(response.data.user.fullname);
    //       userContext.updateAvatar(response.data.user.avatar);
    //       userContext.updateUserId(response.data.user.id);
    //       userContext.updateEmail(response.data.user.email);
    //       navigation.navigate('DrawerNavigator');
    //     } else if (response.data.status == 201) {
    //       navigation.navigate('Confirm', {data: {email: email}});
    //     }
    //   })
    //   .catch(err => {
    //     console.log(err.response);
    //     userContext.updateNotification({
    //       type: ALERT_TYPE.DANGER,
    //       title: 'Check your credentials',
    //       textBody: err.response.data.message,
    //       autoClose: true,
    //     });
    //   });
  };

  return (
    <View style={styles.main}>
      <Header />
      <View style={{paddingHorizontal: 17}}>
        <Text style={styles.title}>Velkommen til Heim</Text>
        <Input
          placeholder="email@example.com"
          floatingText="
          Fyll inn epostadressen din"
          value={email}
          setValue={setEmail}
        />
        <Input
          value={password}
          setValue={setPassword}
          placeholder="Password"
          floatingText="Skriv inn passordet ditt"
          isPassword
        />

        <View style={{flexDirection: 'row', marginTop: 24}}>
          <Text style={styles.text1}>Glemt passord ? ingen sak</Text>
          <TouchableOpacity onPress={onPressForgotPassword}>
            <Text style={styles.text2}>Kom deg nå</Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <CheckBox isChecked={isChecked} setIsChecked={setIsChecked} />
          <Text style={styles.text3}>hold meg pålogget</Text>
        </View>
      </View>
      <View style={{paddingTop: '10%'}}>
        <WhiteButton title="Logg inn" onPress={onLogin} />
        <Spacer Space={'20%'} />
        {/* <ThirdPartyButton title="Continue with Apple" />
        <ThirdPartyButton title="Continue with Google" isApple={false} /> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  title: {
    fontFamily: 'SFProDisplay-Semibold',
    color: colors.white,
    fontSize: 24,
    marginTop: 30,
  },
  text1: {
    color: colors.white,
    fontSize: 16,
    fontFamily: 'SFProDisplay-Regular',
  },
  text2: {
    color: colors.white,
    marginLeft: 5,
    textDecorationLine: 'underline',
    fontSize: 16,
    fontFamily: 'SFProDisplay-Semibold',
  },
  text3: {
    color: colors.white,
    fontSize: 16,
    fontFamily: 'SFProDisplay-Regular',
    marginTop: 12,
    marginLeft: 10,
  },
});

export default LoginScreen;
