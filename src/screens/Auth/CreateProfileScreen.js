import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import colors from '../../utils/colors/colors';
import ImageSelector from '../../components/ImageSelector/ImageSelector';
import {SignupInput} from '../../components/Input';
import {WhiteButton} from '../../components/Buttons';
import Spacer from '../../components/Spacer/Spacer';
import {useNavigation} from '@react-navigation/native';
import {API} from '../../utils/colors/consts';
import axios from 'react-native-axios';
import {UserContext} from '../../context/user-context';
import Header from '../../components/Header/Header';

const CreateProfileScreen = ({route}) => {
  const [selectedImage, setSelectedImage] = React.useState(
    require('../../assets/img/avatar6.png'),
  );

  const userContext = React.useContext(UserContext);

  const [index, setIndex] = React.useState(1);
  const [data, setData] = React.useState(null);
  const [from, setFrom] = React.useState(null);
  const navigation = useNavigation();

  const onPressNext = () => {
    if (from) {
      const _data = {
        avatar: index,
      };

      const config = {
        method: 'post',
        url: `${API}auth/change_avatar`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userContext.token}`,
        },
        data: _data,
      };

      axios(config)
        .then(response => {
          if (response.data.status == 200) {
            userContext.updateAvatar(index);
            navigation.goBack();
          }
        })
        .catch(err => {
          console.log(err.response);
        });
    } else {
      const _data = {
        fullname: data.fullname,
        email: data.email,
        password: data.password,
        avatar: index,
      };

      const config = {
        method: 'post',
        url: `${API}auth/register`,
        headers: {'Content-Type': 'application/json'},
        data: _data,
      };

      axios(config)
        .then(response => {
          if (response.data.status == 200) {
            navigation.navigate('Confirm', {data: {email: data.email}});
          }
        })
        .catch(err => {
          console.log(err.response);
        });
    }
  };

  React.useEffect(() => {
    console.log(route);
    if (route.params?.data) {
      setData(route.params.data);
    } else if (route.params?.from) {
      setFrom(route.params.from);
    }
  }, [route]);

  return (
    <View style={{flex: 1, backgroundColor: colors.primary}}>
      {from ? <Header title="Edit avatar" /> : <></>}
      <View style={{flex: 1, paddingHorizontal: 24}}>
        <ScrollView showsVerticalScrollIndicator={false} style={{flex: 0.98}}>
          <View style={{alignItems: 'center', marginBottom: 28}}>
            <Text style={styles.title}>Opprett profil</Text>
            <Image
              source={selectedImage}
              style={{marginTop: 56, marginBottom: 41}}
            />
            <ImageSelector
              setIndex={setIndex}
              setSelectedImage={setSelectedImage}
            />
          </View>
          {/* <SignupInput placeholder="Username" floatingText="Username" /> */}
          <Spacer Space={20} />
        </ScrollView>
        <WhiteButton
          onPress={onPressNext}
          marginHorizontal={0}
          title="Lagre profil"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 24,
    color: colors.white,
    marginTop: 24,
  },
});

export default CreateProfileScreen;
