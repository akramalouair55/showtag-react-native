import React from 'react';
import {View, StyleSheet, Image, Text, ScrollView} from 'react-native';
import colors from '../../utils/colors/colors';
import SignupProgress from '../../components/SignupProgress/SignupProgress';
import {SignupInput} from '../../components/Input';
import {WhiteButton} from '../../components/Buttons';
import Spacer from '../../components/Spacer/Spacer';
import {useNavigation} from '@react-navigation/native';

const SignupScreen = ({route}) => {
  const [planType, setPlanType] = React.useState('Basic Plan');
  const [price, setPrice] = React.useState('Free');
  const navigation = useNavigation();

  const [errors, setErrors] = React.useState({});

  const [fullname, setFullname] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');

  const isValidEmail = email => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailRegex.test(email);
  };

  const validateForm = () => {
    const newErrors = {};

    if (!fullname) newErrors.fullname = 'Fullt navn kreves';
    if (!email) newErrors.email = 'E-post er påkrevd';
    if (!isValidEmail(email)) newErrors.email = 'Ugyldig e-postformat';
    if (!password) newErrors.password = 'Passord er påkrevd';
    if (!confirmPassword)
      newErrors.confirmPassword = 'Bekreftelsespassord kreves';
    if (password !== confirmPassword)
      newErrors.confirmPassword = 'passordene er ikke like';

    setErrors(newErrors);

    return Object.keys(newErrors).length === 0; // No errors
  };

  //add on error

  const onPressNext = () => {
    if (validateForm()) {
      navigation.navigate('CreateProfile', {
        data: {fullname, email, password},
      });
    }
  };

  React.useEffect(() => {
    if (route.params.isSelected) {
      setPrice('Free');
      setPlanType('Basic Plan');
    } else {
      setPrice('9.99');
      setPlanType('Ad Free Plan');
    }
  }, [route]);

  return (
    <View style={styles.main}>
      <ScrollView
        automaticallyAdjustKeyboardInsets
        showsVerticalScrollIndicator={false}>
        <View style={{alignItems: 'center', marginBottom: 34}}>
          <SignupProgress progress={2} />
          <Text style={styles.title}>lag din kontot</Text>
          <Text style={styles.text}>
            Vær den første til å strømme de siste episodene rett etter at de er
            sendt.
          </Text>
        </View>
        <Text style={{...styles.text, fontSize: 16, textAlign: 'left'}}>
          Selected plan
        </Text>
        <View style={styles.planView}>
          <Text style={styles.planTypeText}>{planType}</Text>
          <View style={styles.freeView}>
            <Text style={styles.free}>{price}</Text>
            <Image
              source={require('../../assets/img/tick.png')}
              style={{width: 24, height: 24}}
            />
          </View>
        </View>
        <Text style={styles.txt}>Kontoinformasjon</Text>
        <SignupInput
          value={fullname}
          setValue={setFullname}
          floatingText={'Fullt navn'}
          placeholder={'Fullt navn'}
          error={errors.fullname}
        />
        <SignupInput
          value={email}
          setValue={setEmail}
          floatingText={'E-post'}
          placeholder={'E-post'}
          error={errors.email}
        />
        <SignupInput
          value={password}
          setValue={setPassword}
          floatingText={'Passord'}
          placeholder={'Passord'}
          Password
          error={errors.password}
        />
        <SignupInput
          value={confirmPassword}
          setValue={setConfirmPassword}
          Password
          floatingText={'Bekreft passord'}
          placeholder={'Bekreft passord'}
          error={errors.confirmPassword}
        />
        <Spacer Space={50} />
      </ScrollView>
      <WhiteButton onPress={onPressNext} title={'Neste'} marginHorizontal={0} />
      <View style={{marginBottom: 12}} />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.primary,
    paddingTop: 40,
    paddingHorizontal: 24,
  },
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 24,
    color: colors.white,
    marginTop: 24,
  },
  text: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    marginTop: 8,
    textAlign: 'center',
    color: colors.white,
  },
  free: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 15,
    color: colors.white,
  },
  freeView: {
    flexDirection: 'row',
  },
  planView: {
    flexDirection: 'row',
    height: 53,
    borderRadius: 8,
    backgroundColor: 'rgba(66, 178, 155, 0.25)',
    marginTop: 16,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  planTypeText: {
    fontFamily: 'SFProDisplay-Medium',
    fontSize: 16,
    color: colors.white,
  },
  txt: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: colors.white,
    marginTop: 30,
  },
});

export default SignupScreen;
