import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import colors from '../../utils/colors/colors';
import Header from '../../components/Header/Header';
import {useNavigation} from '@react-navigation/native';
import {WhiteButton, TransparentButton} from '../../components/Buttons';
import Slider from '../../components/Carousel/Slider';

const OnboardingScreen = () => {
  const navigation = useNavigation();

  const onPressLogin = () => {
    navigation.navigate('Login');
  };

  const onPressSignup = () => {
    navigation.navigate('Signup', {isSelected: true, selection: true});
  };

  return (
    <View style={styles.main}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image
          source={require('../../assets/img/heimlogo.png')}
          style={{height: 67}}
          resizeMode="contain"
        />
      </View>
      <View style={styles.contentView}>
        <Text style={styles.title}>Velkommen til Heim</Text>
        <Text style={styles.description}>
          Oppdag filmer, TV-serier og eksklusivt innhold—når som helst, hvor som
          helst
        </Text>
      </View>
      <WhiteButton title="Logg inn" onPress={onPressLogin} />
      <TransparentButton title="Opprett konto" onPress={onPressSignup} />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.primary,
    paddingTop: 12,
  },
  onboardingImg: {height: 222, marginTop: 34},
  progress: {width: 110, height: 6},
  progressView: {alignItems: 'center', paddingTop: 30},
  description: {
    marginTop: 10,
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    color: colors.gray,
    textAlign: 'center',
  },
  title: {
    color: colors.white,
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
  },
  contentView: {
    alignItems: 'center',
    paddingTop: 30,
    paddingHorizontal: 17,
    marginBottom: 30,
  },
  description: {
    marginTop: 10,
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    color: colors.gray,
    textAlign: 'center',
  },
  title: {
    color: colors.white,
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
  },
});

export default OnboardingScreen;
