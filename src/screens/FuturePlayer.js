import React, {useRef} from 'react';
import {View, Dimensions, PixelRatio} from 'react-native';
import WebView from 'react-native-webview';

const FuturePlayer = () => {
  const Ref = useRef(null);

  console.log(
    PixelRatio.getPixelSizeForLayoutSize(Dimensions.get('window').width),
  );
  console.log(
    PixelRatio.getPixelSizeForLayoutSize(Dimensions.get('window').height),
  );

  const TESTEr = `var event = new CustomEvent('showtag',{'detail':{'time':'4000','image':'https://upload.wikimedia.org/wikipedia/commons/4/40/Image_test.png'}}); document.dispatchEvent(event);`;

  const testr2 = `var event = new Event('start'); document.dispatchEvent(event);`;

  const onMessage = payload => {
    let dataPayload;
    try {
      dataPayload = JSON.parse(payload.nativeEvent.data);
    } catch (e) {
      console.log(e);
    }

    if (dataPayload) {
      if (dataPayload.type === 'Console') {
        console.info(`[Console] ${JSON.stringify(dataPayload.data)}`);
      } else {
        console.log(dataPayload);
      }
    }
  };

  const injectedJavaScript = `const startEvent = new CustomEvent('test',{'detail':{'studio':true,'url':'https://tag2339.showtagenabled.com/hls/brs1e2-2339/brs1e2-2339_master.m3u8','videoId':'brs1e2','width':'1500px','idUser':'testuser'}} ); document.dispatchEvent(startEvent);`;

  return (
    <View style={{flex: 1}}>
      <WebView
        ref={Ref}
        javaScriptEnabled
        cacheEnabled
        onLoadEnd={() => {
          setTimeout(() => {
            Ref.current.injectJavaScript(injectedJavaScript);
          }, 5000);
          setTimeout(() => {
            Ref.current.injectJavaScript(TESTEr);
          }, 10000);
          setTimeout(() => {
            Ref.current.injectJavaScript(testr2);
          }, 11000);
        }}
        thirdPartyCookiesEnabled
        domStorageEnabled
        webviewDebuggingEnabled
        originWhitelist={['*']}
        source={{uri: 'file:///android_asset/akrem/player.html'}}
        style={{flex: 1}}
      />
    </View>
  );
};

export default FuturePlayer;
