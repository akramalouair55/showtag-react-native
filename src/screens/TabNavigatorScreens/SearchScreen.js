import React from 'react';
import {View, Text, ScrollView, TextInput, Image} from 'react-native';
import colors from '../../utils/colors/colors';
import HeaderTab from '../../components/Header/HeaderTab';
import Search from '../../components/Search/Search';
import HomeCard from '../../components/Cards/HomeCard';
import axios from 'react-native-axios';

const SearchScreen = () => {
  const [searchQuery, setSearchQuery] = React.useState('');
  const [data, setData] = React.useState([]);
  const [filteredData, setFilteredData] = React.useState([]);

  const onLoad = () => {
    const config = {
      method: 'get',
      url: `https://tag2339.blob.core.windows.net/api/series/seriesOverview.json`,
    };

    axios(config)
      .then(response => {
        setData(response.data);
        setFilteredData(response.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  React.useEffect(() => {
    onLoad();
  }, []);

  React.useEffect(() => {
    if (searchQuery.length > 0) {
      const value = searchPrograms(data, searchQuery);
      if (value) {
        setFilteredData(value);
      }
    }
  }, [searchQuery]);

  const searchPrograms = (programs, searchTerm) => {
    return programs.filter(program => {
      return Object.keys(program).some(key => {
        const value = program[key];
        if (typeof value === 'string' || typeof value === 'number') {
          return value
            .toString()
            .toLowerCase()
            .includes(searchTerm.toLowerCase());
        }
        return false;
      });
    });
  };

  return (
    <View style={{flex: 1, backgroundColor: colors.primary}}>
      <HeaderTab isBlur={false} />
      <View style={{height: 10}} />
      <Search value={searchQuery} setValue={setSearchQuery} />
      <View style={{height: 10}} />
      <ScrollView
        style={{paddingTop: 16, paddingHorizontal: 8}}
        showsVerticalScrollIndicator={false}>
        {filteredData.map((item, index) => {
          return <HomeCard key={index} item={item} marginTop={15} />;
        })}
      </ScrollView>
    </View>
  );
};

export default SearchScreen;
