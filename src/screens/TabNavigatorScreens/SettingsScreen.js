import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import colors from '../../utils/colors/colors';
import EditInfo from '../../components/Profile/EditInfo';
import {useNavigation} from '@react-navigation/native';
import {avatars} from '../../utils/colors/consts';
import {WhiteButton} from '../../components/Buttons';
import {UserContext} from '../../context/user-context';
import RNRestart from 'react-native-restart';
import {removeItemValue} from '../../utils/colors/utils';
import {API} from '../../utils/colors/consts';
import axios from 'react-native-axios';
import {ALERT_TYPE} from 'react-native-alert-notification';

const SettingsScreen = () => {
  const navigation = useNavigation();

  const userContext = React.useContext(UserContext);

  const onPressUsername = () => {
    navigation.navigate('EditUsername');
  };

  const onPressChangePassword = () => {
    navigation.navigate('ChangePassword');
  };

  const onPressChangeAvatar = () => {
    navigation.navigate('CreateProfile', {from: 'Settings'});
  };

  const onPressLogout = async () => {
    userContext.updateToken('');
    userContext.updateEmail('');
    userContext.updateName('');
    userContext.updateAvatar(1);
    userContext.updateNotification({});
    await removeItemValue('userToken');
    RNRestart.restart();
  };

  const onConfirmDeletion = () => {
    const config = {
      method: 'DELETE',
      url: `${API}auth/delete_account`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userContext.token}`,
      },
    };

    axios(config)
      .then(response => {
        console.log(response.data);
        if (response.data.status == 200) {
          userContext.updateToken('');
          userContext.updateEmail('');
          userContext.updateName('');
          userContext.updateAvatar(1);
          userContext.updateNotification({
            type: ALERT_TYPE.SUCCESS,
            title: 'Success',
            textBody: response.data.message,
            autoClose: true,
          });
          removeItemValue('userToken');
          RNRestart.restart();
        }
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  const onPressDeleteAccount = () => {
    userContext.updateDialog({
      type: ALERT_TYPE.DANGER,
      title: 'Sletting av konto',
      textBody:
        'Er du sikker på at du vil slette kontoen din?! trykk utenfor for å avbryte',
      button: 'Ja, slett den',
      autoClose: 5000,
      closeOnOverlayTap: true,
      onPressButton: onConfirmDeletion,
    });
  };

  return (
    <View style={styles.main}>
      <ScrollView>
        <Text style={styles.accountText}>Regnskap</Text>
        {/* <View style={styles.avatarView}>
          <Image
            source={avatars[userContext.avatar - 1]}
            style={styles.avatarImg}
          />
          <TouchableOpacity onPress={onPressChangeAvatar}>
            <Text style={styles.avatarText}>Bytt avatar</Text>
          </TouchableOpacity>
        </View> */}
        <View style={{paddingTop: 10}}>
          <EditInfo
            onPress={onPressUsername}
            title="Epostadresse"
            content={userContext.email}
          />
          <EditInfo
            onPress={onPressChangePassword}
            title="Passord"
            isPassword={true}
          />
          <EditInfo disabled title="Brukernavn" content={userContext.name} />
          <View style={styles.horizontalLine} />
          <WhiteButton
            onPress={onPressLogout}
            title="Logg ut"
            marginHorizontal={0}
          />
          <WhiteButton
            onPress={onPressDeleteAccount}
            title="Slett kontoen din"
            marginHorizontal={0}
            backgroundColor={colors.secondWhite}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: colors.primary,
    flex: 1,
    paddingTop: 40,
    paddingHorizontal: 24,
  },
  accountText: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 32,
    color: colors.white,
  },
  avatarView: {flexDirection: 'row', marginTop: 31, alignItems: 'center'},
  avatarImg: {height: 61, width: 61, marginRight: 9},
  avatarText: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: '#42B29B',
  },
  horizontalLine: {
    height: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.16)',
    marginVertical: 16,
    marginBottom: 20,
  },
});

export default SettingsScreen;
