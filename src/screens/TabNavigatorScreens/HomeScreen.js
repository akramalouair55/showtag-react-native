import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import colors from '../../utils/colors/colors';
import HeaderTab from '../../components/Header/HeaderTab';
import HomeCard from '../../components/Cards/HomeCard';
import Carousel from '../../components/HorizontalCarousel/Carousel';
import axios from 'react-native-axios';
import {API} from '../../utils/colors/consts';
import {UserContext} from '../../context/user-context';

const HomeScreen = () => {
  const [isBlur, setIsBlur] = React.useState(false);

  const userContext = React.useContext(UserContext);

  const [continueWatching, setContinueWatching] = React.useState([]);
  const [firstSeries, setFirstSeries] = React.useState([]);
  const [secondSeries, setSecondSeries] = React.useState([]);

  const arraytest = [0, 1, 2, 3, 4, 5];

  const ContinueWatching = () => {
    const config = {
      method: 'get',
      url: `https://tag2339flask.azurewebsites.net/api/v1/plugin/user/continueWatching?id_user=${userContext.id}`,
    };

    axios(config)
      .then(response => {
        console.log('response.data :>>', response.data);
        setContinueWatching(response.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const onLoad = () => {
    const config = {
      method: 'get',
      url: `https://tag2339.blob.core.windows.net/api/series/seriesOverview.json`,
    };

    axios(config)
      .then(response => {
        const array1 = response.data.filter(
          obj => obj.id_program_serie === 2150,
        );
        const array2 = response.data.filter(
          obj => obj.id_program_serie === 2156,
        );
        setFirstSeries(array1);
        setSecondSeries(array2);
      })
      .catch(err => {
        console.log(err);
      });
  };

  React.useEffect(() => {
    onLoad();
    ContinueWatching();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: colors.primary}}>
      <View
        style={{
          zIndex: 5,
          height: Platform.OS == 'ios' ? 120 : 60,
          position: 'absolute',
          width: '100%',
        }}>
        <HeaderTab isBlur={isBlur} />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        scrollEventThrottle={16}
        onScroll={event => {
          if (Platform.OS == 'android') {
            if (event.nativeEvent.contentOffset.y > 30) {
              setIsBlur(true);
            } else {
              setIsBlur(false);
            }
          } else {
            if (event.nativeEvent.contentOffset.y > 0) {
              setIsBlur(true);
            } else {
              setIsBlur(false);
            }
          }
        }}>
        {secondSeries.length > 0 ? (
          <HomeCard data={secondSeries} item={secondSeries[0]} />
        ) : (
          <></>
        )}
        <Carousel title={'Fortsett å se'} isContinue data={continueWatching} />
        <Carousel title={'Rom for suksess'} data={firstSeries} />
        <Carousel title={'Boligradets'} data={secondSeries} />
        <View style={{marginBottom: 50}} />
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
