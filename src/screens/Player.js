import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  PixelRatio,
} from 'react-native';
import {Video} from 'expo-av';
import Slider from '@react-native-community/slider';
import colors from '../utils/colors/colors';
import * as Animatable from 'react-native-animatable';
import Loader from '../components/Loader/Loader';
import {useNavigation} from '@react-navigation/native';
import {parseVMAP, parseVAST} from '../utils/colors/utils';
import WebView from 'react-native-webview';
import ViewShot from 'react-native-view-shot';
import RNFS from 'react-native-fs';
import {getProducts} from '../utils/colors/Showtag';
import ShowtagButton from '../components/Showtag/ShowtagButton';

const Player = ({route}) => {
  const navigation = useNavigation();

  const [progress, setProgress] = React.useState(0);
  const [maximumValue, setMaximumValue] = React.useState(0);
  const [isPaused, setIsPaused] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [visibleController, setVisibleController] = React.useState(false);
  const [lastTimeoutID, setLastTimeoutID] = React.useState(null);
  const [videoUrl, setVideoUrl] = React.useState('');
  const [isMuted, setIsMuted] = React.useState(false);
  const [isAdPlaying, setIsAdPlaying] = React.useState(false);
  const [mediaUrl, setMediaUrl] = React.useState([]);
  const [products, setProducts] = React.useState([]);

  const videoRef = React.useRef(null);
  const ContainerRef = React.useRef(null);
  const PlayerRef = React.useRef(null);
  const screenShotRef = React.useRef(null);

  const WIDTH = PixelRatio.getPixelSizeForLayoutSize(
    Dimensions.get('window').width,
  );

  const HEIGHT = PixelRatio.getPixelSizeForLayoutSize(
    Dimensions.get('window').height,
  );

  const fetcher = (url, parseType) => {
    fetch(url)
      .then(response => response.text())
      .then(data => {
        if (parseType == 'vast') {
          let arr = parseVAST(data);
          let currentMedia = mediaUrl;
          currentMedia.push(...arr);
          setMediaUrl([...currentMedia]);
        } else {
          const arr = parseVMAP(data);
          arr.map(item => {
            fetcher(item, 'vast');
          });
        }
      })
      .catch(error => {
        console.error('Error fetching VMAP:', error);
      });
  };

  navigation.addListener('blur', () => {
    if (lastTimeoutID != null) {
      clearTimeout(lastTimeoutID);
    }
  });

  //function that convert seconds to hours and minutes and seconds
  const convertSeconds = millis => {
    var seconds = millis / 1000;
    var hours = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - hours * 3600) / 60);
    var seconds = seconds - hours * 3600 - minutes * 60;
    var time = '';

    if (hours != 0) {
      time = hours + ':';
    }
    if (minutes != 0 || time !== '') {
      minutes = minutes < 10 && time !== '' ? '0' + minutes : String(minutes);
      time += minutes + ':';
    }
    if (time === '') {
      time = parseInt(seconds) + 's';
    } else {
      time +=
        seconds < 10 ? '0' + parseInt(seconds) : String(parseInt(seconds));
    }
    return time;
  };

  const ControllerHandler = () => {
    if (lastTimeoutID != null) {
      clearTimeout(lastTimeoutID);
    }
    setVisibleController(true);
    const timeoutID = setTimeout(() => {
      try {
        PlayerRef.current.slideOutDown(500);
        ContainerRef.current.fadeOut(500);
      } catch (error) {
        console.log(error);
      }
      setTimeout(() => {
        setVisibleController(false);
        setLastTimeoutID(null);
      }, 500);
    }, 5000);
    setLastTimeoutID(timeoutID);
  };

  const seekForward = async () => {
    ControllerHandler();
    if (videoRef.current) {
      if (progress + 10000 < maximumValue) {
        const newPosition = progress + 10000;
        await videoRef.current.setPositionAsync(newPosition);
      } else {
        const newPosition = maximumValue;
        await videoRef.current.setPositionAsync(newPosition);
      }
    }
  };

  const seekBackward = async () => {
    ControllerHandler();
    if (videoRef.current) {
      if (progress - 10000 > 0) {
        const newPosition = Math.max(progress - 10000, 0); // Seek backward by 5000 milliseconds (5 seconds), but not going below 0
        await videoRef.current.setPositionAsync(newPosition);
      } else {
        const newPosition = 0;
        await videoRef.current.setPositionAsync(newPosition);
      }
    }
  };

  const getStatus = async () => {
    const obj = await videoRef.current.getStatusAsync();

    setProgress(obj.positionMillis);
  };

  const togglePlayPause = async () => {
    ControllerHandler();
    if (videoRef.current) {
      if (isPaused) {
        await videoRef.current.playAsync();
        setProducts([]);
      } else {
        await videoRef.current.pauseAsync();
        const products = await getProducts(
          progress,
          route.params.data.ProgramCode,
        );
        setProducts(products);
      }
    }
  };

  const onPressVolume = () => {
    navigation.goBack();
  };

  React.useEffect(() => {
    if (route.params) {
      if (route.params?.isContinue) {
        const modifiedProgramCode = route.params.data.programCode.replace(
          /-\d+$/,
          '',
        );
        const baseUrl = `https://vz-2286a78a-96b.b-cdn.net`;
        const masterUrl = `${baseUrl}/${modifiedProgramCode}/playlist.m3u8`;
        setVideoUrl(masterUrl);
        console.log(masterUrl);
      } else {
        const modifiedProgramCode = route.params.data.ProgramCode.replace(
          /-\d+$/,
          '',
        );
        const baseUrl = `https://vz-2286a78a-96b.b-cdn.net`;
        const masterUrl = `${baseUrl}/${modifiedProgramCode}/playlist.m3u8`;
        setVideoUrl(masterUrl);
        console.log(masterUrl);
      }
    }
  }, []);

  React.useEffect(() => {
    fetcher(
      'https://tag2339.blob.core.windows.net/commercials/vmap-2ads.xml',
      'vmap',
    );
  }, []);

  React.useEffect(() => {
    if (visibleController) {
      PlayerRef.current.slideInUp(1000);
      ContainerRef.current.fadeIn(1000);
    }
  }, [visibleController]);

  React.useEffect(() => {
    // Set up the interval
    if (!isAdPlaying) {
      const intervalId = setInterval(() => {
        getStatus();
      }, 500); // 500ms interval

      // Clear the interval when the component is unmounted
      return () => clearInterval(intervalId);
    }
  }, [isAdPlaying]);

  // React.useEffect(() => {
  //   if (mediaUrl.length > 0) {
  //     setIsAdPlaying(true);
  //   }
  // }, [mediaUrl]);

  const getVariables = async () => {
    const test = await getProducts();
    console.log('test :>>', test);
  };

  const handleVideoError = error => {
    // Log the error to the console or display it on the screen
    console.error('Video Error:', error);
  };

  return (
    <>
      {loading && <Loader />}
      {isPaused &&
        products.map((item, index) => {
          return (
            <View
              key={index}
              style={{
                position: 'absolute',
                zIndex: 50,
                marginHorizontal: 105,
              }}>
              <ShowtagButton
                item={item}
                style={{
                  marginTop: item.y * PixelRatio.get() * 1.5,
                  marginLeft: item.x * PixelRatio.get() * 1.5,
                }}
              />
            </View>
          );
        })}
      <TouchableOpacity
        onPress={isAdPlaying ? () => null : ControllerHandler}
        activeOpacity={1}
        style={{
          flex: 1,
          position: loading ? 'absolute' : 'relative',
          top: loading ? 1000 : 0,
        }}>
        {!isAdPlaying ? (
          <ViewShot ref={screenShotRef} style={{flex: 1}}>
            <View style={{flex: 1}}>
              <Video
                style={{flex: 1, backgroundColor: 'black'}}
                resizeMode="contain"
                isMuted={isMuted}
                shouldPlay={true}
                source={{
                  uri: videoUrl,
                }} // Can be a URL or a local file.
                ref={videoRef} // Store reference
                onPlaybackStatusUpdate={e => {
                  setIsPaused(!e.isPlaying);
                }}
                onLoad={e => {
                  if (route.params?.isContinue) {
                    videoRef.current.setPositionAsync(
                      route.params.data.startTime,
                    );
                  }
                  setLoading(false);
                  setMaximumValue(e.durationMillis);
                  setTimeout(() => {
                    try {
                      PlayerRef.current.slideInUp(1000);
                      ContainerRef.current.fadeIn(1000);
                    } catch (e) {
                      console.log(e);
                    }
                  }, 200);
                  setTimeout(() => {
                    try {
                      PlayerRef.current.slideOutDown(500);
                      ContainerRef.current.fadeOut(500);
                    } catch (error) {
                      console.log(e);
                    }
                  }, 5000);
                  setTimeout(() => {
                    setVisibleController(false);
                  }, 5500);
                }}
                onError={handleVideoError}
              />
            </View>
          </ViewShot>
        ) : (
          <Video
            style={{flex: 1}}
            resizeMode="contain"
            isMuted={isMuted}
            shouldPlay={true}
            source={{}} // Can be a URL or a local file.
            onPlaybackStatusUpdate={e => {
              //if (e.didJustFinish) {
              setIsAdPlaying(false);
              //}
            }}
            onLoad={e => {
              setLoading(false);
            }}
            onError={e => {
              console.log(e);
            }}
          />
        )}
        <Animatable.View
          ref={ContainerRef}
          style={{
            position: 'absolute',
            backgroundColor: visibleController
              ? 'rgba(0,0,0,0.7)'
              : 'transparent',
            height: Dimensions.get('window').height,
            alignItems: 'center',
            justifyContent: 'flex-end',
            zIndex: 6,
          }}>
          {visibleController && (
            <Animatable.View
              ref={PlayerRef}
              style={{
                marginBottom: 35,
                marginHorizontal: 30,
              }}>
              <View style={{marginLeft: 10, top: 30}}>
                <Text
                  style={{
                    fontFamily: 'SFProDisplay-Bold',
                    fontSize: 20,
                    color: colors.white,
                  }}>
                  {route.params.data.SerieName}
                </Text>
                <Text
                  style={{
                    fontFamily: 'SFProDisplay-Regular',
                    fontSize: 16,
                    color: colors.white,
                  }}>
                  {route.params.data.ProgramName}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginHorizontal: 10,
                }}>
                <View style={{width: 20, height: 20}} />
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    onPress={seekForward}
                    style={{marginHorizontal: 5}}>
                    <Image
                      source={require('../assets/img/forward.png')}
                      style={{width: 32, height: 32}}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={togglePlayPause}
                    style={{marginHorizontal: 5}}>
                    <Image
                      source={
                        !isPaused
                          ? require('../assets/img/pause.png')
                          : require('../assets/img/playbtn.png')
                      }
                      style={{width: 32, height: 32}}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={seekBackward}
                    style={{marginHorizontal: 5}}>
                    <Image
                      source={require('../assets/img/backward.png')}
                      style={{width: 32, height: 32}}
                    />
                  </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={onPressVolume}>
                  <Image
                    source={require('../assets/img/volume.png')}
                    style={{width: 32, height: 32}}
                  />
                </TouchableOpacity>
              </View>
              <Slider
                style={{width: Dimensions.get('window').width - 50, height: 30}}
                minimumValue={0}
                maximumValue={maximumValue}
                value={progress}
                onValueChange={async value =>
                  await videoRef.current.setPositionAsync(value)
                }
                minimumTrackTintColor="#FFFFFF"
                maximumTrackTintColor="#737373"
                thumbTintColor="#FFFFFF"
                onTouchMove={() => {
                  ControllerHandler();
                }}
              />
              <Text
                style={{
                  fontFamily: 'SFProDisplay-Regular',
                  color: colors.white,
                  marginLeft: 15,
                }}>
                {convertSeconds(progress) +
                  ' / ' +
                  convertSeconds(maximumValue)}
              </Text>
            </Animatable.View>
          )}
        </Animatable.View>
      </TouchableOpacity>
    </>
  );
};
var styles = StyleSheet.create({});

export default Player;
