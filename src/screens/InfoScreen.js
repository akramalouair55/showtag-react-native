import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  StyleSheet,
} from 'react-native';
import colors from '../utils/colors/colors';
import Header from '../components/Header/Header';
import {IMGURL} from '../utils/colors/consts';
import {useNavigation} from '@react-navigation/native';

const InfoScreen = ({route}) => {
  const [data, setData] = React.useState([]);
  const [selectedItem, setSelectedItem] = React.useState({});

  const navigation = useNavigation();

  React.useEffect(() => {
    if (route.params) {
      let arr = [];
      arr = route.params.data.filter(
        item => item.EpisodeNumber !== route.params.selectedItem.EpisodeNumber,
      );
      setData([...arr]);
      setSelectedItem(route.params.selectedItem);
    }
  }, []);

  return (
    <View style={styles.main}>
      <Header title=" " />
      <ScrollView contentContainerStyle={styles.scroll}>
        {data.length > 0 && (
          <>
            <Image
              source={{
                uri: `${IMGURL}${selectedItem.ProgramImage}`.replace(
                  '.png',
                  '.webp',
                ),
              }}
              style={styles.poster}
            />
            <Text style={styles.title}>{selectedItem.SerieName}</Text>
            <Text style={styles.episode}>
              {'Episode' + selectedItem.EpisodeNumber}
            </Text>
            <Text style={styles.desc}>{selectedItem.ProgramDescription}</Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Player', {data: selectedItem});
              }}
              style={styles.btn}>
              <Image
                source={require('../assets/img/video-circle.png')}
                style={{height: 24, width: 24}}
              />
              <Text style={styles.btnText}>Play episode</Text>
            </TouchableOpacity>
            <Text style={styles.seasonNumber}>
              {'Season ' + selectedItem.SeasonNumber}
            </Text>
            {data.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={{height: 181, marginVertical: 12}}
                  onPress={() => {
                    navigation.navigate('Player', {data: item});
                  }}>
                  <Image
                    style={{height: 181, borderRadius: 8}}
                    source={{
                      uri: `${IMGURL}${item.ProgramImage}`.replace(
                        '.png',
                        '.webp',
                      ),
                    }}
                  />
                  <View
                    style={{
                      position: 'absolute',
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 181,
                      width: '100%',
                      backgroundColor: 'rgba(12, 16, 26, 0.40)',
                    }}>
                    <Image
                      style={{height: 45, width: 45, top: 20}}
                      source={require('../assets/img/circleplay.png')}
                    />
                    <View
                      style={{
                        alignSelf: 'flex-start',
                        top: 50,
                        paddingHorizontal: 20,
                      }}>
                      <Text style={styles.nb}>
                        {'Episode ' + item.EpisodeNumber}
                      </Text>
                      <Text></Text>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })}
          </>
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  scroll: {paddingHorizontal: 24, paddingTop: 10},
  poster: {height: 256, borderRadius: 16},
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 20,
    color: 'rgba(250, 232, 214, 1)',
    marginTop: 34,
  },
  episode: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 15,
    color: 'rgba(250, 232, 214, 1)',
    marginTop: 10,
  },
  desc: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    color: 'rgba(132, 132, 132, 1)',
    letterSpacing: 0.5,
    marginTop: 10,
  },
  btn: {
    height: 48,
    backgroundColor: colors.white,
    borderRadius: 8,
    flexDirection: 'row',
    marginTop: 38,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: 'black',
    marginLeft: 5,
  },
  seasonNumber: {
    color: 'rgba(250, 232, 214, 1)',
    fontSize: 16,
    fontFamily: 'SFProDisplay-Regular',
    marginTop: 20,
  },
  nb: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 12,
    color: colors.white,
  },
  pn: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 14,
    color: colors.white,
    marginBottom: 5,
  },
});

export default InfoScreen;
