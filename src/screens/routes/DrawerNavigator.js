import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import colors from '../../utils/colors/colors';
import {Image} from 'react-native';
import TabNavigator from './TabNavigator';
import {
  MoviesScreen,
  SeriesScreen,
  TrendingScreen,
  CollectionScreen,
} from '../DrawerScreens';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        drawerStyle: {backgroundColor: colors.tabBar},
        headerShown: false,
        drawerInactiveTintColor: '#FAE8D6',
        drawerActiveBackgroundColor: 'transparent',
      }}
      initialRouteName="TabNavigator">
      <Drawer.Screen
        name="TabNavigator"
        options={{title: 'Home'}}
        component={TabNavigator}
      />
      {/*     <Drawer.Screen
        name="Movies"
        options={{
          drawerLabelStyle: {marginLeft: -24},
          drawerIcon: ({color}) => {
            return (
              <Image
                source={require('../../assets/img/movies.png')}
                style={{width: 24, height: 24}}
              />
            );
          },
        }}
        component={MoviesScreen}
      />
      <Drawer.Screen
        name="Series"
        component={SeriesScreen}
        options={{
          drawerLabelStyle: {marginLeft: -24},
          drawerIcon: ({color}) => {
            return (
              <Image
                source={require('../../assets/img/series.png')}
                style={{width: 24, height: 24}}
              />
            );
          },
        }}
      />
      <Drawer.Screen
        name="Trending"
        options={{
          title: 'Trending now',
          drawerLabelStyle: {marginLeft: -24},
          drawerIcon: ({color}) => {
            return (
              <Image
                source={require('../../assets/img/trending.png')}
                style={{width: 24, height: 24}}
              />
            );
          },
        }}
        component={TrendingScreen}
      />
      <Drawer.Screen
        name="Collection"
        options={{
          title: 'Saved & Collection',
          drawerLabelStyle: {marginLeft: -24},
          drawerIcon: ({color}) => {
            return (
              <Image
                source={require('../../assets/img/collection.png')}
                style={{width: 24, height: 24}}
              />
            );
          },
        }}
        component={CollectionScreen}
      />
      */}
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
