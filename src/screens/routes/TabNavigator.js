import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeScreen, SearchScreen, SettingsScreen} from '../TabNavigatorScreens';
import {Image, Settings, View, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import colors from '../../utils/colors/colors';
import HeaderTab from '../../components/Header/HeaderTab';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarLabelStyle: {
          fontSize: 14,
          marginBottom: 10,
          fontFamily: 'SFProDisplay-Semibold',
        },
        tabBarHideOnKeyboard: true,
        headerShown: false,
        tabBarStyle: {
          backgroundColor: '#0C101A',
          height: Platform.OS == 'ios' ? 100 : 80,
        },
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          const HomeIcon = require('../../assets/img/home.png');
          const SearchIcon = require('../../assets/img/search.png');
          const SettingsIcon = require('../../assets/img/settings.png');
          const HomeIconFocused = require('../../assets/img/home-filled.png');
          const SearchIconFocused = require('../../assets/img/search-filled.png');
          const SettingsIconFocused = require('../../assets/img/settings-filled.png');

          if (route.name === 'Home') {
            iconName = focused ? HomeIconFocused : HomeIcon;
          } else if (route.name === 'Settings') {
            iconName = focused ? SettingsIconFocused : SettingsIcon;
          } else if (route.name === 'Search') {
            iconName = focused ? SearchIconFocused : SearchIcon;
          }

          // You can return any component that you like here!
          return <Image source={iconName} />;
        },
        tabBarActiveTintColor: colors.iconLabel,
        tabBarInactiveTintColor: colors.iconLabel,
      })}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Search" component={SearchScreen} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
    </Tab.Navigator>
  );
};

export default TabNavigator;
