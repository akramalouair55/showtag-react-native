import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  ChoosePlanScreen,
  ConfirmScreen,
  LoginScreen,
  SignupScreen,
  CreateProfileScreen,
  ForgotPassword,
} from '../Auth';
import {OnboardingScreen} from '../Onboarding';
import {View, Image} from 'react-native';
import TabNavigator from './TabNavigator';
import DrawerNavigator from './DrawerNavigator';
import {ChangePassword, EditUsername} from '../EditProfile';
import Player from '../Player';
import {API} from '../../utils/colors/consts';
import axios from 'react-native-axios';
import {getToken} from '../../utils/colors/utils';
import {UserContext} from '../../context/user-context';
import colors from '../../utils/colors/colors';
import * as Anim from 'react-native-animatable';
import {Toast, ALERT_TYPE} from 'react-native-alert-notification';
import InfoScreen from '../InfoScreen';
import FuturePlayer from '../FuturePlayer';

const Navigator = () => {
  const Stack = createNativeStackNavigator();
  const [route, setRoute] = React.useState('Onboarding');
  const [loading, setLoading] = React.useState(true);
  const userContext = React.useContext(UserContext);

  const isFirstTime = async () => {
    const token = await getToken();
    if (token) {
      const config = {
        method: 'get',
        url: `${API}auth/profile`,
        headers: {Authorization: `Bearer ${token}`},
      };
      axios(config)
        .then(response => {
          console.log(response.data.status);
          if (response.data.status == 200) {
            try {
              setRoute('DrawerNavigator');

              userContext.updateAvatar(response.data.user.avatar);
              userContext.updateName(response.data.user.fullname);
              userContext.updateUserId(response.data.user.id);
              userContext.updateToken(token);
              userContext.updateEmail(response.data.user.email);
            } catch (error) {
              console.log(error);
            }

            setLoading(false);
          } else {
            setRoute('Onboarding');
            setLoading(false);
          }
        })
        .catch(err => {
          console.log(err.response);
          setRoute('Onboarding');
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    isFirstTime();
  }, []);

  return (
    <>
      {loading ? (
        <View
          style={{
            flex: 1,
            backgroundColor: colors.primary,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Anim.View
            animation="pulse"
            duration={1000}
            iterationCount="infinite">
            <Image
              style={{width: 200, height: 200}}
              resizeMode="contain"
              source={require('../../assets/img/heimlogo.png')}
            />
          </Anim.View>
        </View>
      ) : (
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            animationTypeForReplace: 'pop',
            animation: 'slide_from_right',
          }}
          initialRouteName={'DrawerNavigator'}>
          <Stack.Screen name="Onboarding" component={OnboardingScreen} />
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Signup" component={SignupScreen} />
          <Stack.Screen name="ChoosePlan" component={ChoosePlanScreen} />
          <Stack.Screen name="Confirm" component={ConfirmScreen} />
          <Stack.Screen name="CreateProfile" component={CreateProfileScreen} />
          <Stack.Screen name="DrawerNavigator" component={DrawerNavigator} />
          <Stack.Screen name="EditUsername" component={EditUsername} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
          <Stack.Screen name="InfoScreen" component={InfoScreen} />
          <Stack.Screen
            name="FuturePlayer"
            component={FuturePlayer}
            options={{orientation: 'landscape'}}
          />
          <Stack.Screen
            name="Player"
            options={{orientation: 'landscape'}}
            component={Player}
          />
        </Stack.Navigator>
      )}
    </>
  );
};

export default Navigator;
