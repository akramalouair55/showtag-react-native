import React from 'react';
import {View, StyleSheet, Image} from 'react-native';

const SignupProgress = ({progress = 1}) => {
  const [array, setArray] = React.useState([]);

  React.useEffect(() => {
    if (progress == 1) {
      setArray(array1);
    } else if (progress == 2) {
      setArray(array2);
    } else {
      setArray(array3);
    }
  }, []);

  const array1 = [
    require('../../assets/img/white.png'),
    require('../../assets/img/gray.png'),
    require('../../assets/img/gray.png'),
  ];

  const array2 = [
    require('../../assets/img/green.png'),
    require('../../assets/img/white.png'),
    require('../../assets/img/gray.png'),
  ];

  const array3 = [
    require('../../assets/img/green.png'),
    require('../../assets/img/green.png'),
    require('../../assets/img/white.png'),
  ];

  return (
    <View style={styles.main}>
      <Image source={array[0]} style={styles.image} />
      <Image source={array[1]} style={styles.image} />
      <Image source={array[2]} style={styles.image} />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {flexDirection: 'row', alignItems: 'center'},
  image: {width: 42, height: 7, marginHorizontal: 3},
});

export default SignupProgress;
