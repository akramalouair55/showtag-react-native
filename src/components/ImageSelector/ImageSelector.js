import React from 'react';
import {TouchableOpacity, View, StyleSheet, Image} from 'react-native';

const ImageSelector = ({setSelectedImage, setIndex}) => {
  const array = [
    require('../../assets/img/avatar1.png'),
    require('../../assets/img/avatar2.png'),
    require('../../assets/img/avatar3.png'),
    require('../../assets/img/avatar4.png'),
    require('../../assets/img/avatar5.png'),
    require('../../assets/img/avatar6.png'),
  ];

  return (
    <View style={{flexDirection: 'row'}}>
      {array.map((item, index) => {
        return (
          <TouchableOpacity
            key={index}
            onPress={() => {
              setSelectedImage(item);
              setIndex(index + 1);
            }}>
            <Image source={item} style={styles.img} />
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  img: {width: 48, height: 48, marginHorizontal: 5},
});

export default ImageSelector;
