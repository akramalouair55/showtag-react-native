import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import colors from '../../utils/colors/colors';

const PlanCard = ({isSelected = true, setIsSelected}) => {
  const onPressFirst = () => {
    setIsSelected(true);
  };

  const onPressSecond = () => {
    setIsSelected(false);
  };

  return (
    <View>
      <View style={styles.main}>
        <Image
          source={require('../../assets/img/tag.png')}
          style={{width: 24, height: 24, marginRight: 8}}
        />
        <Text style={styles.header}>Save 20% with yearly plan</Text>
      </View>
      <TouchableOpacity
        onPress={onPressFirst}
        style={isSelected ? styles.btnCard : styles.btnCard2}>
        <View>
          <Text style={styles.title}>Basic plan</Text>
          <Text style={styles.desc}>Watch eveything with limited Ads</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.plan}>Free</Text>
          <Image
            source={
              isSelected
                ? require('../../assets/img/tick.png')
                : require('../../assets/img/add-filled.png')
            }
            style={styles.img}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={onPressSecond}
        style={isSelected ? styles.btnCard2 : styles.btnCard}>
        <View>
          <Text style={styles.title}>Ad Free </Text>
          <Text style={styles.desc}>Watch eveything with limited Ads</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.plan}>9.99</Text>
          <Image
            source={
              isSelected
                ? require('../../assets/img/add-filled.png')
                : require('../../assets/img/tick.png')
            }
            style={styles.img}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 50,
    marginBottom: 18,
  },
  btnCard: {
    backgroundColor: 'rgba(13, 51, 130, 1)',
    height: 73,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
    marginVertical: 5,
  },
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: colors.white,
  },
  desc: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    color: colors.white,
  },
  plan: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 20,
    color: colors.white,
    marginHorizontal: 5,
  },
  img: {width: 24, height: 24},
  header: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: 'rgba(251, 234, 218, 1)',
  },
  btnCard2: {
    backgroundColor: 'transparent',
    height: 73,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: 'rgba(13, 51, 130, 1)',
  },
});

export default PlanCard;
