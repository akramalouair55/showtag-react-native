import React from 'react';
import {TextInput, Image, StyleSheet, View} from 'react-native';

const Search = ({value, setValue}) => {
  const [isFocused, setIsFocused] = React.useState(false);

  return (
    <View style={isFocused ? styles.main2 : styles.main}>
      <Image
        source={require('../../assets/img/searchscreen.png')}
        style={styles.img}
      />
      <TextInput
        value={value}
        onChangeText={e => {
          setValue(e);
        }}
        style={styles.textInput}
        placeholder="Hva ser du etter"
        placeholderTextColor="rgba(255, 255, 255, 0.23)"
        onFocus={() => {
          setIsFocused(true);
        }}
        onBlur={() => {
          setIsFocused(false);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 40,
    borderWidth: 1,
    borderColor: 'rgba(224, 220, 220, 0.5)',
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
    marginHorizontal: 20,
  },
  img: {height: 24, width: 24},
  textInput: {color: 'white', flex: 1},
  main2: {
    height: 40,
    borderWidth: 1,
    borderColor: 'rgba(224, 220, 220, 1)',
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
    marginHorizontal: 20,
  },
});

export default Search;
