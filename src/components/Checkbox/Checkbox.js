import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import colors from '../../utils/colors/colors';

const CheckBox = ({isChecked = false, setIsChecked}) => {
  return (
    <TouchableOpacity
      onPress={() => {
        setIsChecked(!isChecked);
      }}
      style={styles.main}>
      {isChecked && <Image source={require('../../assets/img/checked.png')} />}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: colors.primaryGray,
    marginTop: 15,
  },
});

export default CheckBox;
