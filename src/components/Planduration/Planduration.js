import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import colors from '../../utils/colors/colors';

const Planduration = ({selection = true, setSelection}) => {
  const onPressMonthly = () => {
    setSelection(true);
  };

  const onPressYearly = () => {
    setSelection(false);
  };

  return (
    <View style={styles.main}>
      <TouchableOpacity
        onPress={onPressMonthly}
        style={selection ? styles.btn : styles.btn2}>
        <Text style={selection ? styles.focusedText : styles.text}>
          Monthly
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={onPressYearly}
        style={selection ? styles.btn2 : styles.btn}>
        <Text style={selection ? styles.text : styles.focusedText}>Yearly</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    backgroundColor: 'rgba(81, 87, 102, 0.5)',
    height: 40,
    alignItems: 'center',
    borderRadius: 100,
  },
  btn: {
    alignItems: 'center',
    backgroundColor: 'rgba(81, 87, 102, 1)',
    width: '50%',
    height: 40,
    justifyContent: 'center',
    borderRadius: 100,
  },
  focusedText: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: colors.white,
  },
  text: {
    fontFamily: 'SFProDisplay-Bold',
    color: 'rgba(148, 154, 163, 1)',
    fontSize: 16,
  },
  btn2: {width: '50%', alignItems: 'center'},
});

export default Planduration;
