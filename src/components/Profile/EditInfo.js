import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import colors from '../../utils/colors/colors';

const EditInfo = ({
  title,
  content,
  isPassword = false,
  onPress = () => null,
  disabled = false,
}) => {
  return (
    <View style={styles.main}>
      <View>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.content}>{isPassword ? '********' : content}</Text>
      </View>
      {!disabled && (
        <TouchableOpacity onPress={onPress} style={styles.img}>
          <Image
            source={require('../../assets/img/edit.png')}
            style={styles.img}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  main: {flexDirection: 'row', justifyContent: 'space-between', marginTop: 16},
  title: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 13,
    color: colors.white,
  },
  content: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: colors.white,
    marginTop: 5,
  },
  img: {width: 24, height: 24},
});

export default EditInfo;
