import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import colors from '../../utils/colors/colors';

const Input = ({
  placeholder,
  floatingText,
  value,
  setValue,
  isPassword = false,
}) => {
  return (
    <View style={styles.main}>
      <View style={styles.floatingTextView}>
        <Text style={styles.floatingText}>{floatingText}</Text>
      </View>
      <TextInput
        style={{color: colors.white}}
        secureTextEntry={isPassword}
        placeholder={placeholder}
        placeholderTextColor={colors.gray}
        value={value}
        onChangeText={e => {
          setValue(e);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 56,
    borderWidth: 1,
    borderColor: colors.primaryGray,
    borderRadius: 4,
    justifyContent: 'center',
    paddingHorizontal: 5,
    marginTop: 33,
  },
  floatingTextView: {
    backgroundColor: colors.primary,
    position: 'absolute',
    top: -10,
    left: 10,
    paddingHorizontal: 3,
    borderRadius: 5,
    alignItems: 'center',
  },
  floatingText: {
    color: colors.secondWhite,
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 12,
  },
});

export default Input;
