import React, {useRef} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import colors from '../../utils/colors/colors';
import * as Anim from 'react-native-animatable';

const SignupInput = ({
  placeholder,
  floatingText,
  Password = false,
  value,
  setValue,
  error,
}) => {
  const [isFocused, setIsFocused] = React.useState(false);
  const FloatingView = useRef(null);
  const _onFocus = () => {
    setIsFocused(true);
  };

  const _onBlur = () => {
    setIsFocused(false);
  };

  React.useEffect(() => {
    if (FloatingView != null && isFocused) {
      FloatingView.current.slideInUp(200);
    }
  }, [isFocused]);

  return (
    <View>
      <View
        style={[
          isFocused ? styles.mainFocused : styles.main,
          error && styles.errorBorder,
        ]}>
        {isFocused && (
          <Anim.View ref={FloatingView} style={styles.floatingTextView}>
            <Text style={styles.floatingText}>{floatingText}</Text>
          </Anim.View>
        )}
        <TextInput
          value={value}
          onChangeText={e => {
            setValue(e);
          }}
          style={{color: colors.white}}
          placeholder={isFocused ? '' : placeholder}
          placeholderTextColor={colors.gray}
          onBlur={_onBlur}
          onFocus={_onFocus}
          secureTextEntry={Password}
        />
      </View>
      {error && <Text style={styles.errorText}>{error}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 56,
    borderWidth: 1,
    borderColor: colors.primaryGray,
    borderRadius: 4,
    justifyContent: 'center',
    paddingHorizontal: 5,
    marginTop: 21,
  },
  mainFocused: {
    height: 56,
    borderWidth: 1,
    borderColor: colors.white,
    borderRadius: 4,
    justifyContent: 'center',
    paddingHorizontal: 5,
    marginTop: 21,
  },
  floatingTextView: {
    backgroundColor: colors.primary,
    position: 'absolute',
    top: -10,
    left: 10,
    paddingHorizontal: 3,
    borderRadius: 5,
    alignItems: 'center',
  },
  floatingText: {
    color: colors.secondWhite,
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 12,
  },
  errorBorder: {
    borderColor: 'red',
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    marginTop: 4,
  },
});

export default SignupInput;
