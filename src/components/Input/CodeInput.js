import React from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import colors from '../../utils/colors/colors';

const CodeInput = ({
  isRef,
  nextRef = null,
  previousRef = null,
  value,
  setValue,
}) => {
  return (
    <View style={value.length > 0 ? styles.main2 : styles.main}>
      <TextInput
        ref={isRef}
        value={value}
        onKeyPress={e => {
          if (e.nativeEvent.key === 'Backspace' && value.length === 0) {
            if (previousRef) {
              previousRef.current.focus();
            }
          }
        }}
        onChangeText={text => {
          setValue(text);
          if (text == value) {
            if (previousRef) {
              previousRef.current.focus();
            }
          }
          if (nextRef && text.length === 1) {
            nextRef.current.focus();
          }
        }}
        keyboardType="numeric"
        style={value.length > 0 ? styles.input2 : styles.input}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 54,
    width: 72,
    borderWidth: 1,
    borderColor: 'rgba(217, 217, 217, 1)',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  main2: {
    height: 54,
    width: 72,
    borderWidth: 1,
    borderColor: 'rgba(217, 217, 217, 1)',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(217, 217, 217, 1)',
  },
  input: {
    textAlign: 'center',
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 25,
    color: colors.white,
    width: 30,
  },
  input2: {
    textAlign: 'center',
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 25,
    color: colors.black,
  },
});

export default CodeInput;
