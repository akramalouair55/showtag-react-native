import {
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Animated,
  Easing,
} from 'react-native';
import React from 'react';
import colors from '../../utils/colors/colors';

const {width, height} = Dimensions.get('screen');

const SlideItem = ({item}) => {
  const translateYImage = new Animated.Value(40);

  Animated.timing(translateYImage, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
    easing: Easing.bounce,
  }).start();

  return (
    <View style={styles.container}>
      <Animated.Image
        source={item.img}
        resizeMode="contain"
        style={[
          styles.image,
          {
            transform: [
              {
                translateY: translateYImage,
              },
            ],
          },
        ]}
      />

      <View style={styles.contentView}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.description}</Text>
      </View>
    </View>
  );
};

export default SlideItem;

const styles = StyleSheet.create({
  container: {
    height: height * 0.55,
    width,
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: 240,
  },
  description: {
    marginTop: 10,
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: colors.gray,
    textAlign: 'center',
  },
  title: {
    color: colors.white,
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
  },
  contentView: {alignItems: 'center', paddingTop: 30, paddingHorizontal: 17},
});
