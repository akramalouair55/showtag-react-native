export default [
  {
    id: 1,
    img: require('../../assets/img/onboarding1.png'),
    title: 'Velkommen til Heim',
    description:
      'Oppdag filmer, TV-serier og eksklusivt innhold—når som helst, hvor som helst.',
  },
  {
    id: 2,
    img: require('../../assets/img/onboarding2.png'),
    title: 'Se på en hvilken som helst enhet',
    description:
      'Strøm på telefonen, nettbrettet, bærbare datamaskinen og TV-en uten å betale mer.',
  },
  {
    id: 3,
    img: require('../../assets/img/onboarding3.png'),
    title: 'Eksklusivt innhold',
    description:
      'Personlige anbefalinger for å oppdage dine nye favorittserier og filmer.',
  },
];
