import React from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import colors from '../../utils/colors/colors';
import {IMGURL} from '../../utils/colors/consts';
import {useNavigation} from '@react-navigation/native';

const Carousel = ({title, data = [], isContinue = false}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.main}>
      <Text style={styles.title}>{title}</Text>
      <ScrollView showsHorizontalScrollIndicator={false} horizontal>
        {data.map((item, index) => {
          if (title == 'Fortsett å se') {
            console.log(
              'object :>>',
              `${IMGURL}${item.programCoverImage}`.replace('.png', '.webp'),
            );
          }

          return (
            <TouchableOpacity
              onPress={() => {
                if (isContinue) {
                  navigation.navigate('Player', {data: item, isContinue: true});
                  return;
                }
                navigation.navigate('InfoScreen', {
                  data: data,
                  selectedItem: item,
                });
              }}
              key={index}
              style={{marginRight: 8, marginTop: 16}}>
              {title == 'Fortsett å se' ? (
                <Image
                  source={{
                    uri: `${IMGURL}${item.programCoverImage}`.replace(
                      '.png',
                      '.webp',
                    ),
                  }}
                  style={{height: 181, width: 255, borderRadius: 8}}
                />
              ) : (
                <Image
                  source={{
                    uri: `${IMGURL}${item.ProgramImage}`.replace(
                      '.png',
                      '.webp',
                    ),
                  }}
                  style={{height: 181, width: 255, borderRadius: 8}}
                />
              )}
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {paddingHorizontal: 16, paddingTop: 30},
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: colors.light,
  },
});

export default Carousel;
