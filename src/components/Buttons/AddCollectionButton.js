import React from 'react';
import {StyleSheet, TouchableOpacity, Image, Text} from 'react-native';

const AddCollectionButton = () => {
  return (
    <TouchableOpacity style={styles.main}>
      <Image
        source={require('../../assets/img/add.png')}
        style={styles.image}
      />
      <Text style={styles.text}>Min liste</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 40,
    borderColor: '#FAE8D6',
    backgroundColor: 'rgba(250, 232, 214, 0.13)',
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 18,
    marginVertical: 20,
  },
  image: {width: 16, height: 16, marginLeft: 8},
  text: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: '#FAE8D6',
    marginHorizontal: 8,
    marginRight: 15,
  },
});

export default AddCollectionButton;
