import React from 'react';
import colors from '../../utils/colors/colors';
import {Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

const ThirdPartyButton = ({
  title,
  onPress = () => null,
  marginHorizontal = 17,
  isApple = true,
}) => {
  const apple = require('../../assets/img/applelogo.png');
  const google = require('../../assets/img/googlelogo.png');

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{...styles.main, marginHorizontal}}>
      <Image source={isApple ? apple : google} />
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 48,
    backgroundColor: 'transparent',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.white,
    justifyContent: 'center',
    borderRadius: 8,
    marginVertical: 15,
    flexDirection: 'row',
  },
  title: {
    textAlign: 'center',
    color: colors.white,
    fontSize: 16,
    fontFamily: 'SFProDisplay-Semibold',
  },
});

export default ThirdPartyButton;
