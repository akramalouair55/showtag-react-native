import React from 'react';
import colors from '../../utils/colors/colors';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

const WhiteButton = ({
  title,
  onPress = () => null,
  marginHorizontal = 17,
  backgroundColor = colors.white,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{...styles.main, marginHorizontal, backgroundColor}}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 48,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    marginBottom: 15,
  },
  title: {
    textAlign: 'center',
    color: colors.black,
    fontSize: 16,
    fontFamily: 'SFProDisplay-Semibold',
  },
});

export default WhiteButton;
