import React from 'react';
import {StyleSheet, TouchableOpacity, Image, Text} from 'react-native';

const WatchButton = ({onPress = () => null}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.watch}>
      <Image
        source={require('../../assets/img/play.png')}
        style={styles.image}
      />
      <Text style={styles.text}>Se nå</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  watch: {
    height: 40,
    backgroundColor: '#FAE8D6',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 18,
    marginVertical: 20,
  },
  image: {width: 16, height: 16, marginLeft: 8},
  text: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: '#0C101A',
    marginHorizontal: 8,
  },
});

export default WatchButton;
