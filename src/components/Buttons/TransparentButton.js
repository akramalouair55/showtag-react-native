import React from 'react';
import colors from '../../utils/colors/colors';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

const TransparentButton = ({
  title,
  onPress = () => null,
  marginHorizontal = 17,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{...styles.main, marginHorizontal}}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 48,
    backgroundColor: 'rgba(255, 255, 255, 0.16)',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.16)',
    justifyContent: 'center',
    borderRadius: 8,
    marginBottom: 40,
  },
  title: {
    textAlign: 'center',
    color: colors.white,
    fontSize: 16,
    fontFamily: 'SFProDisplay-Semibold',
  },
});

export default TransparentButton;
