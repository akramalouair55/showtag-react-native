import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import colors from '../../utils/colors/colors';
import {useNavigation} from '@react-navigation/native';
import {BlurView} from '@react-native-community/blur';
import {UserContext} from '../../context/user-context';
import {avatars} from '../../utils/colors/consts';

const HeaderTab = ({isBlur}) => {
  const navigation = useNavigation();

  const userContext = React.useContext(UserContext);

  return (
    <View style={styles.main}>
      {isBlur && (
        <BlurView
          style={styles.absolute}
          blurType="light"
          blurAmount={10}
          overlayColor=""
          downsampleFactor={0.5}
        />
      )}
      <View style={styles.backbtn}></View>
      <Image
        source={require('../../assets/img/heimlogo.png')}
        style={{height: 26, width: 91}}
      />
      <View style={styles.backbtn}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    justifyContent: 'space-between',
    height: Platform.OS == 'ios' ? 120 : 60,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  backbtn: {
    height: 38,
    width: 38,
    alignItems: 'center',
    justifyContent: 'center',
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: Platform.OS == 'ios' ? 90 : 60,
  },
});

export default HeaderTab;
