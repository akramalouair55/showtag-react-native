import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import colors from '../../utils/colors/colors';
import {useNavigation} from '@react-navigation/native';

const Header = ({hideBack = false, title}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.main}>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        disabled={hideBack}
        style={styles.backbtn}>
        <Image
          source={hideBack ? '' : require('../../assets/img/goback.png')}
        />
      </TouchableOpacity>
      {!title ? (
        <Image
          source={require('../../assets/img/heimlogo.png')}
          style={{height: 26, width: 91}}
        />
      ) : (
        <Text style={styles.title}>{title}</Text>
      )}
      <TouchableOpacity style={styles.backbtn}></TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    justifyContent: 'space-between',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: colors.primary,
    paddingTop: Platform.OS == 'ios' ? 70 : 0,
  },
  backbtn: {
    height: 38,
    width: 38,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 18,
    color: colors.white,
  },
});

export default Header;
