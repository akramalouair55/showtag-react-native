import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {WatchButton, AddCollectionButton} from '../Buttons';
import colors from '../../utils/colors/colors';
import {useNavigation} from '@react-navigation/native';
import {IMGURL} from '../../utils/colors/consts';

const HomeCard = ({item, marginTop = null, data}) => {
  const navigation = useNavigation();

  const onPress = () => {
    navigation.navigate('Player', {data: data, selectedItem: item});
  };

  return (
    <View
      style={[
        styles.main,
        marginTop && {...styles.main, marginTop: marginTop},
      ]}>
      <View style={styles.imageView}>
        <Image
          source={{
            uri: `${IMGURL}${item.ProgramImage}`.replace('.png', '.webp'),
          }}
          style={styles.image}
        />
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'flex-end',
          flex: 1,
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
        }}>
        <Text style={styles.txt}>{item.ProgramYear}</Text>
        <Text style={styles.title}>{item.SerieName}</Text>
        <Text style={styles.txt}>{item.ProgramName}</Text>
        <View style={{flexDirection: 'row'}}>
          <WatchButton onPress={onPress} />
          <View style={{margin: 10}} />
          <AddCollectionButton />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 422,
    borderRadius: 16,
    marginHorizontal: 16,
    marginTop: 90,
  },
  imageView: {position: 'absolute', width: '100%'},
  image: {height: 422, width: '100%', borderRadius: 16},
  txt: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: colors.light,
  },
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 36,
    color: colors.light,
  },
});

export default HomeCard;
