import React from 'react';
import {View, StyleSheet, Dimensions, Animated, Text} from 'react-native';
import LottieView from 'lottie-react-native';
import colors from '../../utils/colors/colors';

const Loader = () => {
  return (
    <View
      style={{
        position: 'relative',
        flex: 1,
        backgroundColor: colors.primary,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <LottieView
        source={require('../../assets/anim/loading2.json')}
        style={{width: 300, height: 300}}
        autoPlay
        loop
      />
    </View>
  );
};

export default Loader;
