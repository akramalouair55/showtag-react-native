import React from 'react';
import {View} from 'react-native';

const Spacer = ({Space = 5}) => {
  return <View style={{marginTop: Space}} />;
};

export default Spacer;
