import React from 'react';
import {View, TouchableOpacity, StyleSheet, Image, Text} from 'react-native';
import {IMGURL} from '../../utils/colors/consts';

const ShowtagButton = ({onPress, index, style, item}) => {
  const [isPress, setIsPressed] = React.useState(false);

  return (
    <View style={[{flexDirection: 'row'}, style]}>
      <TouchableOpacity
        onPress={() => {
          setIsPressed(!isPress);
        }}
        style={[
          {
            width: 20,
            height: 20,
            backgroundColor: 'transparent',
            borderRadius: 60,
          },
        ]}>
        <Image
          source={require('../../assets/img/showtagbtn.png')}
          style={{width: 20, height: 20}}
        />
      </TouchableOpacity>
      {isPress && (
        <View
          style={[
            {
              height: 80,
              width: 300,
              backgroundColor: '#202525',
              borderRadius: 5,
              flexDirection: 'row',
              bottom: 30,
              left: 10,
              alignItems: 'center',
              paddingLeft: 15,
            },
          ]}>
          <Image
            source={{uri: `${IMGURL}${item.ProductImage}`}}
            style={{height: 56, width: 80}}
          />
          <View style={{marginLeft: 5}}>
            <Text style={styles.txt}>{`Price : ${item.Price}`}</Text>
            <Text style={styles.txt2}>{item.ProductName}</Text>
            <Text style={styles.txt3}>{item.BrandName}</Text>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  txt: {
    color: 'white',
    fontSize: 12,
    fontFamily: 'SFProDisplay-Medium',
    color: '#91D2D5',
  },
  tx2: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'SFProDisplay-Medium',
  },
  tx3: {
    color: '#C3C7C7',
    fontSize: 14,
    fontFamily: 'SFProDisplay-Medium',
  },
});

export default ShowtagButton;
