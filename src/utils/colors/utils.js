import AsyncStorage from '@react-native-async-storage/async-storage';

export const saveToken = async newToken => {
  try {
    await AsyncStorage.setItem('userToken', newToken);
  } catch (e) {
    // Saving error
    console.error('Failed to save the data to the storage', e);
  }
};

export const removeItemValue = async key => {
  try {
    await AsyncStorage.removeItem(key);
    return true;
  } catch (exception) {
    return false;
  }
};

// Function to get token
export const getToken = async () => {
  try {
    const value = await AsyncStorage.getItem('userToken');
    if (value !== null) {
      return value;
    } else {
      return null;
    }
  } catch (e) {
    // Reading error
    console.error('Failed to fetch the data from storage', e);
    return null;
  }
};

export const parseVMAP = xmlString => {
  const tagStart = '<vmap:AdTagURI';
  const cdataStart = '<![CDATA[';
  const cdataEnd = ']]>';
  const tagEnd = '</vmap:AdTagURI>';

  let extractedUri = [];

  let index = 0;
  while (index < xmlString.length) {
    // Find the start of the AdTagURI tag
    let tagStartIndex = xmlString.indexOf(tagStart, index);
    if (tagStartIndex === -1) break; // No more AdTagURI tags

    // Find the start of the CDATA section
    let cdataStartIndex = xmlString.indexOf(cdataStart, tagStartIndex);
    if (cdataStartIndex === -1) break; // Malformed XML, CDATA section not found

    // Find the end of the CDATA section
    let cdataEndIndex = xmlString.indexOf(cdataEnd, cdataStartIndex);
    if (cdataEndIndex === -1) break; // Malformed XML, CDATA section not closed

    // Extract the URL from the CDATA section
    let url = xmlString.substring(
      cdataStartIndex + cdataStart.length,
      cdataEndIndex,
    );

    extractedUri.push(url);
    // Find the end of the AdTagURI tag and update the index for the next iteration
    let tagEndIndex = xmlString.indexOf(tagEnd, cdataEndIndex);
    index =
      tagEndIndex !== -1
        ? tagEndIndex + tagEnd.length
        : cdataEndIndex + cdataEnd.length;
  }
  return extractedUri;
};

export const parseVAST = xmlString => {
  const tagStart = '<MediaFile';
  const cdataStart = '<![CDATA[';
  const cdataEnd = ']]>';
  const tagEnd = '</MediaFile>';

  let extractedVid = [];

  let index = 0;
  while (index < xmlString.length) {
    // Find the start of the MediaFile tag
    let tagStartIndex = xmlString.indexOf(tagStart, index);
    if (tagStartIndex === -1) break; // No more MediaFile tags

    // Find the start of the CDATA section
    let cdataStartIndex = xmlString.indexOf(cdataStart, tagStartIndex);
    if (cdataStartIndex === -1) break; // Malformed XML, CDATA section not found

    // Find the end of the CDATA section
    let cdataEndIndex = xmlString.indexOf(cdataEnd, cdataStartIndex);
    if (cdataEndIndex === -1) break; // Malformed XML, CDATA section not closed

    // Extract the URL from the CDATA section
    let url = xmlString.substring(
      cdataStartIndex + cdataStart.length,
      cdataEndIndex,
    );
    extractedVid.push(url);

    // Find the end of the MediaFile tag and update the index for the next iteration
    let tagEndIndex = xmlString.indexOf(tagEnd, cdataEndIndex);
    index =
      tagEndIndex !== -1
        ? tagEndIndex + tagEnd.length
        : cdataEndIndex + cdataEnd.length;
  }
  return extractedVid;
};
