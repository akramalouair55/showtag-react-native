import axios from 'react-native-axios';

export const getProducts = async (Time, ProgramCode) => {
  const currentTime = Math.round(Time / 500) * 500;

  const data = {
    Time: currentTime,
    ProgramCode: ProgramCode,
  };

  const config = {
    method: 'post',
    url: `https://tag2339flask.azurewebsites.net/api/v1/plugin/pause/product`,
    data: data,
  };

  const response = await axios(config);
  if (response.status == 200) {
    if (Array.isArray(response.data)) {
      return response.data;
    } else {
      return [];
    }
  } else {
    return [];
  }
};
