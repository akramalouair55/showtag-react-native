export const API = 'https://tagweb2339.azurewebsites.net/';
export const IMGURL = 'https://tag2339.blob.core.windows.net/images/';
export const avatars = [
  require('../../assets/img/avatar1.png'),
  require('../../assets/img/avatar2.png'),
  require('../../assets/img/avatar3.png'),
  require('../../assets/img/avatar4.png'),
  require('../../assets/img/avatar5.png'),
  require('../../assets/img/avatar6.png'),
];
