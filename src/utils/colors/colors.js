const colors = {
  primary: 'rgba(12,16,26,1)',
  gray: 'rgba(157, 157, 157, 1)',
  white: 'white',
  black: '#0C101A',
  primaryGray: 'rgba(243, 243, 243, 0.24)',
  secondWhite: '#CAC4D0',
  tabBar: '#0C101A',
  iconLabel: 'rgba(250, 232, 214, 1)',
  light: '#FAE8D6',
};

export default colors;
